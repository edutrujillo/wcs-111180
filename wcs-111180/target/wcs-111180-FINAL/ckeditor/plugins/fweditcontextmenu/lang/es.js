/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'es',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  'Cambiar activo incluido',
         EditProperties :       'Editar propiedades',
         RemoveIncludedAsset :  'Eliminar activo incluido',
         OpenLink :             'Abrir enlace',
         ChangeLinkedAsset :    'Cambiar activo enlazado',
         RemoveLinkedAsset :    'Eliminar activo enlazado',
         PleaseSelectAssetFromSearchResults : 'Seleccione un activo en los resultados de la búsqueda',
         CannotLinkOrIncludeThisAsset :       'Este activo no permite enlaces ni inclusiones.\nSeleccione un activo de uno de los tipos siguientes:\n',

         CutIncludedAsset :            'Cortar activo incluido',
         CopyIncludedAsset :           'Copiar activo incluido',
         CutLinkedAsset :              'Cortar activo enlazado',
         CopyLinkedAsset :             'Copiar activo enlazado',
         PasteIncludedAsset :          'Pegar activo incluido',
         PasteLinkedAsset :            'Pegar activo enlazado' 		

	}
});





/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwincludeasset', 'pt-br',
{
	fwincludeasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: 'Adicionar link de ativo',
	   IncludeEmbeddedLinkDlgTitle	: 'Incluir Ativo' ,
	   PleaseSelectLinkTextFrom	    : 'O texto a ser vinculado ao ativo deve ser selecionado primeiro' ,
	   PleaseSelectAssetFromTree	: 'Selecione um ativo na árvore.',
	   PleaseSelectAssetFromSearchResults : 'Selecione um ativo nos resultados da pesquisa',
	   PleaseSelectOnlyOneAsset 	: 'Só um ativo pode ser selecionado.',
	   CannotAddSelfInclude	        : 'Não é permitida a autorreferência através de Incluir.\nSelecione outro ativo.',
       CannotLinkOrIncludeThisAsset : 'Não é permitido link ou inclusão neste ativo.\nSelecione um ativo dos seguintes tipos:\n'
			    
	}
});



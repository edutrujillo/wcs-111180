/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'fr',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'Voulez-vous retirer la ressource incluse ?',
	 RemoveLinkedAssetQuestion:'Voulez-vous retirer la ressource liée ?'
	}
});

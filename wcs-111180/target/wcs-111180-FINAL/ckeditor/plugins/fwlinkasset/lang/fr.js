/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwlinkasset', 'fr',
{
	fwlinkasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: 'Ajouter un lien de ressource',
	   IncludeEmbeddedLinkDlgTitle	: 'Inclure la ressource' ,
	   PleaseSelectLinkTextFrom	    : 'Le texte à lier à la ressource doit d\'abord être sélectionné' ,
	   PleaseSelectAssetFromTree	: 'Sélectionnez une ressource dans l\'arborescence.',
	   PleaseSelectAssetFromSearchResults : 'Sélectionner une ressource à partir des résultats de recherche',
	   PleaseSelectOnlyOneAsset 	: 'Une seule ressource peut être sélectionnée.',
	   CannotAddSelfInclude	        : 'L\'auto-référence par inclusion n\'est pas autorisée.\nSélectionnez une autre ressource.',
       CannotLinkOrIncludeThisAsset : 'La liaison ou l\'inclusion de cette ressource n\'est pas autorisée.\nSélectionnez une ressource des types suivants :\n'
			    
	}
});



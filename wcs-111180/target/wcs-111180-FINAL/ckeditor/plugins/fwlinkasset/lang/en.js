﻿/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwlinkasset', 'en',
{
	fwlinkasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: 'Add asset link',
	   IncludeEmbeddedLinkDlgTitle	: 'Include Asset' ,
	   PleaseSelectLinkTextFrom	    : 'The text to link to the asset must be selected first' ,
	   PleaseSelectAssetFromTree	: 'Select an asset from the tree.',
	   PleaseSelectAssetFromSearchResults : 'Select an asset from search results',
	   PleaseSelectOnlyOneAsset 	: 'Only one asset can be selected.',
	   CannotAddSelfInclude	        : 'Self reference through Include is not allowed.\nPlease select another asset.',
       CannotLinkOrIncludeThisAsset : 'This asset is not allowed to link or include.\nPlease select asset of the following types:\n'
			    
	}
});



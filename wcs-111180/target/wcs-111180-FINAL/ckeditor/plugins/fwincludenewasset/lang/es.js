/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwincludenewasset', 'es',
{
	fwincludenewasset :
	{
	  assetCreatorIncludeDlgTitle : 'Creador de activos',
	  assetCreatorIncludeTitleDesc : 'Crear e incluir un activo nuevo',
	  assetCreatorLinkTitleDesc : 'Crear y enlazar un activo nuevo',
	  PleaseSelectLinkTextFrom	: 'Es necesario seleccionar primero el texto de enlace al activo' 
	
	
	  	    
	}
});



﻿/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwincludenewasset', 'en',
{
	fwincludenewasset :
	{
	  assetCreatorIncludeDlgTitle : 'Asset Creator',
	  assetCreatorIncludeTitleDesc : 'Create and include a new asset',
	  assetCreatorLinkTitleDesc : 'Create and link a new asset',
	  PleaseSelectLinkTextFrom	: 'The text to link to the asset must be selected first' 
	
	
	  	    
	}
});



﻿/********************************************************************************
*  Plugin :  ImagePicker

                            fwimagepicker
  Created: March 25, 2010
  By:      JAG
  Reason:  The FW Tray ToolBar  Inclide Asset Command
           to perform the Include asset command and
           functionality

  Modified:  It's possible to customize every single aspect of the editor,
  from the toolbar, to the skin, to dialogs,
  and here we customize the context menu to detect all
  FW  included and linked Assets in an instance of running ckeditor.


  The editor structure is totally plugin based,
  including many of its core features.
  Plugins live in separate files, making them easy to organize and maintain.
  CKEditor  JavaScript API makes developing plugins flexible.

   Create fatwire implementation for a ckeditor plugin to 
*  pick the images directly from the imagepicker.
*********************************************************************************/
CKEDITOR.plugins.add('fwimagepicker',   
{
    requires: ['fwimagepicker'],

    lang : ['en','fr','es','de','it','ja','ko','zh-cn','zh','pt-br'],
	
	// When Instace of the CKEditor is Created this is the Initilization  Callback 
	// for the plug-in button with the editor handle passed in 
    init:function(editor)
	{ 
	     // Lets see all Standard plus user defined ..
	     var b="fwimagepicker";
	     var c=editor.addCommand( b, {
	               // Click on Image Picker Command
                   // Ported the Image Picker
                   //

                   exec : function( ckeditor ) 
                          {							
							var env,fieldname, ckConfig = ckeditor.config, 
								cs_environment =  ckConfig.cs_environment ,assettypes,s;
							var allowedassettypes = ckConfig.allowedassettypes ;
							var ip_assettypename = ckConfig.ip_assettypename ;
							if(allowedassettypes && allowedassettypes.indexOf(ip_assettypename) === -1){
								alert("Image picker's assettype " + ip_assettypename + " is not in the allowed asset types (" + allowedassettypes + ")");
								return
							}
							if(cs_environment!='ucform'&&cs_environment!='insite')
							{

								 var ip_attributetypename =  ckConfig.ip_attributetypename ;
								 var ip_attributename = ckConfig.ip_attributename  ;
								 
								 // Get the Root CS Application Context
								 // Set the Elements Base CS Root Context
								 var csRootContext = ckConfig.csRootContext  ;

								 var pURL = csRootContext+"ContentServer?pagename=OpenMarket/Gator/AttributeTypes/IMAGEPICKERShowImages&ATTRIBUTETYPENAMEOUT=" + ckConfig.ip_attributetypename + "&ATTRIBUTENAMEOUT=" + ckConfig.ip_attributename + "&ASSETTYPENAMEOUT=" + ckConfig.ip_assettypename;

								 pURL = pURL + "&FCKName=" + encodeURIComponent(ckConfig.assetName);
								 pURL = pURL + "&FCKAssetId=" + ckConfig.assetId;
								 pURL = pURL + "&FCKAssetType=" + ckConfig.assetType;
								 pURL = pURL + "&fielddesc=" + encodeURIComponent(ckConfig.fieldDesc);
								 pURL = pURL + "&formFieldName=" + encodeURIComponent(ckConfig.fieldName);
								 pURL = pURL + "&fieldname=" + encodeURIComponent(ckConfig.fieldName);
								 pURL = pURL + "&IFCKEditor=true";
								 var windowProperties = 'scrollbars=yes,resizable=yes,width=950,height=650,top=0,left=0';
								 return window.open(pURL, "ImagePickerControl", windowProperties);
							}else{																			 
								fieldname = ((!ckConfig.fieldName)? ckeditor.name : 
								((ckConfig.editingstyle==="single")? ckConfig.fieldName : ckConfig.fieldName.substr(0,ckConfig.fieldName.lastIndexOf('_'))));
								env=(CKEDITOR.env.webkit)? "webkit" : ((CKEDITOR.env.gecko)? "gecko":"ie");
								assettypes= (!ckConfig.allowedassettypes)? "*": ckConfig.allowedassettypes;	
								//Open all image search results in the docked view
								s = SitesApp.searchController;
								s.model.set('assetType',ckConfig.ip_assettypename);								
								s.model.set('searchText',"");
								s.model.set('assetTypeParam',ckConfig.ip_assettypename);
								s.model.set('assetSubType',"");
								s.search();									
								// CKEditor must invoke the dialogmanager which is a singleton object.
								var CK = ckConfig.isMultiValued ? new fw.ui.controller.CKEditorController : new parent.fw.ui.controller.CKEditorController;
								CK.openCKDialog({
									args : {
										action : 'insert',
										item : 'asset',
										fieldName : fieldname ,
										DEPTYPE : ckConfig.DEPTYPE,
										env : env,
										isMultiVal : ckConfig.isMultiValued,
										deviceid: ckConfig.deviceid
									},
									assettypes : assettypes	
								});								
								
							}
                          }
                  });
				editor.ui.addButton("fwimagepicker",
		        {
					label:    editor.lang.fwimagepicker.title,
					command:  b,
					icon:     this.path+"imagepicker.gif"
				});	
	},
    afterInit : function( editor )
	{
		    editor.on( 'mode', function(){
				if(editor.mode === 'wysiwyg'){
					if(!editor.config.ip_assettypename)
						editor.getCommand( 'fwimagepicker' ).setState(CKEDITOR.TRISTATE_DISABLED);
					else
						editor.getCommand( 'fwimagepicker' ).setState(CKEDITOR.TRISTATE_OFF);
				}
			}, this );
	}
	
});
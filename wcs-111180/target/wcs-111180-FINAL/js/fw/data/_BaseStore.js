/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data._BaseStore"]){dojo._hasResource["fw.data._BaseStore"]=true;dojo.provide("fw.data._BaseStore");dojo.declare("fw.data._BaseStore",null,{url:"",translator:null,transport:null,transportOptions:null,receiver:null,constructor:function(_1){dojo.mixin(this,{url:_1.url,translator:_1.translator,transport:_1.transport,transportOptions:_1.transportOptions,receiver:_1.receiver});},_checkProperties:function(_2){if(!this.url){console.error("[fw.data] No URL endpoint defined for store!");return false;}else{if(!this.transport){console.error("[fw.data] Cannot fetch data without a transport!");return false;}else{if(!this.receiver){console.error("[fw.data] Cannot absorb store data without a receiver!");return false;}}}if(_2){if(!this.translator){console.error("[fw.data] Cannot properly send server requests without a translator!");return false;}}return true;},onReceiveData:function(_3){}});}
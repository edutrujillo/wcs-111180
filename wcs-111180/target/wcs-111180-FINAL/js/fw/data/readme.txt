=== Overview of ClientStore/ServerStore topology ===

The majority of code under this folder revolves around two main classes:
ServerStore and ClientStore.  The former is geared towards use with "smart"
services which are capable of fielding filtering/sorting/pagination requests
on the server side.  The latter performs these operations client-side, and thus
can be used with any service that gives back one flat response with all items.

These data stores delegate pieces of their logic to separate small
sub-components and properties, with the intent of enabling users to mix together
implementations depending upon needs dictated by a particular situation. The
selected sub-components and properties are then mixed in to the data store at
instantiation time.

The following properties and sub-components are supported by both ClientStore
and ServerStore.  (Most of these are covered in more detail later in this file.)

* url: The URL endpoint for resource this store will be fetching from.
* transport: The fw.data.transport responsible for ferrying the request.
* transportOptions: An object (map) with options to feed the transport.
* receiver: The fw.data.receiver to map the response back into store data.

The following additional properties are applicable only to ServerStore, as they
do not have any value to a client-centric data store.

* translator: fw.data.translator this store will use to translate data into
  a common format understandable by a fw.data.transport.

The rest of this document will explain most of the above components in detail,
from an implementation standpoint.  ClientStore and ServerStore already take
care of passing arguments through to these components, so the developer only
needs to take care that each component fulfills the expected contract.

=== The Transport (fw.data.transport) ===

The transport is what is responsible for ferrying a request to an endpoint on a
server.  In the case of ServerStore, the transport's sendFetchRequest method is
invoked on every data store fetch request; ClientStore only invokes it once,
when it is instantiated (subsequent fetches are then run against in-memory
results).

Contract:

fw.data.transport.MyTransport = {
  sendFetchRequest: function(
    /* string */ url,
    /* object */ params,
    /* object */ options,
    /* function? */ loadBack,
    /* function? */ errBack
  )
  {
    ...
    return /* dojo.Deferred */ dfd;
  }
}

Arguments:

* url: A url endpoint to which the transport will be sending its query.
* params: An object (map) containing key/value pairs that the transport will be
  responsible for injecting as necessary into the server request in such a way
  that the server understands.
  For ServerStore, this map is obtained as the result of passing arguments from
  a dojo.data.api.Read fetch call through a designated translator; for
  ClientStore, this argument is always an empty object.
* options: Additional options applicable to the particular transport
  implementation can be passed here.  Client/ServerStore pass the object defined
  in the transportOptions property.
* loadBack: (Optional) a function to be called upon successful completion of
  the server request.
* errBack: (Optional) a function to be called if an error occurs during the
  request, or if something goes wrong during the handling of the response.

Return value:

The sendFetchRequest function is expected to return a dojo.Deferred object; this
is basically an object with hooks to enable further registration of load/error
callbacks.

http://api.dojotoolkit.org/jsdoc/1.3/dojo.Deferred

=== The Receiver (fw.data.receiver) ===

The receiver is responsible for accepting a response from a
previously-transported request, and re-working it into a common format for
Client/ServerStore to internalize.

Note: currently there is nothing in the receiver folder; this is because the
implementation for Content Server REST services instead utilizes a sort of
'factory', so to speak, which generates valid receiver objects on the fly,
depending on which service is being invoked.  The explanation below, however,
is intended to be completely generic, and outlines what is expected of a
receiver object.

Contract:

fw.data.receiver.MyReceiver = {
  getData: function(
    /* object */ response
  )
  {
    ...
    return /* object {
      items, //array
      idAttribute, //string
      labelAttribute, //string?
      total //integer
    } */ object;
  }
}

Arguments:

* response: an object containing the response from the server, ostensibly in the
  form of a javascript object, though it could potentially be in some other
  format that getData will be responsible for parsing (though this would likely
  introduce some dependency between the transport and the receiver).

Return value:

The getData function is expected to return an object with several standard
properties defined for use by ClientStore/ServerStore:

* items: An array containing items obtained from the service response.
* idAttribute: A string identifying an attribute which contains some unique
  identifier in each item.  This does not absolutely need to be present, but
  will allow for usage of the dojo data Identity API's features (which some
  widgets also rely upon), and thus should be present whenever possible.
* labelAttribute: (Optional) A string identifying an attribute which contains
  data usable for labeling purposes in each item.  This maps to a standard
  feature of the dojo data Read API, and may be utilized by some widgets.
* total: (Required for ServerStore) Integer indicating total number of items
  /on server/, not just in this particular response. This is required for
  server-centric services in order for pagination in widgets to work optimally,
  or at all. Naturally, this would need to be supported within the service's
  logic itself as well.

=== The Translator (fw.data.translator) ===

The translator (only used by ServerStore) is responsible for taking input
arguments from a fetch request (pursuant to the dojo data Read API) and creating
a map usable by a fw.data.transport. ServerStore takes the return value from its
translator's translate call, and passes it as the params (2nd) argument to its
transport's sendFetchRequest method.

The translator should generally be interested in mapping the following
properties of a fetch request's keywordArgs:
query, queryOptions, start, count, sort.

For more information on the arguments that may be present in a dojo data Read
API fetch request, see http://docs.dojocampus.org/dojo/data/api/Read#fetch

Contract:

fw.data.translator.MyTranslator = {
  translate: function(
    /* object */ keywordArgs
  )
  {
    ...
    return /* object */ params;
  }
}

Arguments:

* keywordArgs: the keywordArgs object passed directly from a dojo data store's
  fetch request.

Return value:

The translate function is expected to return a simple key/value map which will
then be used by a transport to send arguments to a server endpoint.
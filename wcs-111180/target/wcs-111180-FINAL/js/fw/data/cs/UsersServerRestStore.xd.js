/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data.cs.UsersServerRestStore"],["require","fw.data.cs.ServerRestStore"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data.cs.UsersServerRestStore"]){_4._hasResource["fw.data.cs.UsersServerRestStore"]=true;_4.provide("fw.data.cs.UsersServerRestStore");_4.require("fw.data.cs.ServerRestStore");_4.declare("fw.data.cs.UsersServerRestStore",fw.data.cs.ServerRestStore,{_renderDate:function(_7){var _8={url:_4.config.fw_csPath+"ContentServer",content:{"date":_7,"wemUsers":true,pagename:"OpenMarket/Xcelerate/Util/ConvertDateTZByXhr"},handleAs:"json",sync:true};return _4.xhrGet(_8).then(function(_9){return _9.formattedDates;});},_receiveData:function(_a,_b){var _c=[];_4.forEach(_a.users,function(_d,i){if(_d.lastloggedin){_c.push(_d.lastloggedin);}});if(_c.length!=0){var _e;_4.when(this._renderDate(_c),function(_f){_e=_f;});var _10=0;_4.forEach(_a.users,function(_11,i){if(_11.lastloggedin){_11.lastloggedin=_e[_10];_10++;}});}this.inherited(arguments);}});}}};});
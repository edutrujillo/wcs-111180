/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.controller.MobilityPreviewController"]){dojo._hasResource["fw.ui.controller.MobilityPreviewController"]=true;dojo.provide("fw.ui.controller.MobilityPreviewController");dojo.require("fw.ui.controller.PreviewController");dojo.declare("fw.ui.controller.MobilityPreviewController",fw.ui.controller.PreviewController,{});}
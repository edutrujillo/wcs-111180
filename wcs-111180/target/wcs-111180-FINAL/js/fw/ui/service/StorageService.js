/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.service.StorageService"]){dojo._hasResource["fw.ui.service.StorageService"]=true;dojo.provide("fw.ui.service.StorageService");(function(){var _1="CONTRIBUTORUI:",_2=window.parent.WemContext,_3=_2?_2.getInstance():null,_4=function(_5){return _1+_5;};dojo.declare("fw.ui.service.StorageService",null,{setAttribute:function(_6,_7,_8){if(_3){_3.setAttribute(_4(_6),_7,_8);}},getUserPreference:function(_9){var _a;if(_3){_a=_3.getUserPreference(_4(_9));}return _a;},getAttribute:function(_b){var _c;if(_3){_c=this.getUserPreference(_b);}return _c;},removeAttribute:function(_d){if(_3){_3.removeAttribute(_4(_d));}}});}());}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dojox.grid.DataGrid"]){dojo._hasResource["fw.ui.dojox.grid.DataGrid"]=true;dojo.provide("fw.ui.dojox.grid.DataGrid");dojo.require("dojox.grid.DataGrid");dojo.declare("fw.ui.dojox.grid.DataGrid",dojox.grid.DataGrid,{postCreate:function(){this.inherited(arguments);dojo.safeMixin(this.selection,{clickSelect:function(_1,_2,_3){if(this.mode=="none"){return;}this._beginUpdate();if(this.mode!="extended"){if(this.isSelected(_1)){this.deselect(_1);}else{this.select(_1);}}else{var _4=this.selectedIndex,_5=this.getSelectedCount();if(!_2){this.deselectAll(_1);}if(_3){this.selectRange(_4,_1);}else{if(_2){this.toggleSelect(_1);}else{if(this.isSelected(_1)&&_5===1){this.deselect(_1);}else{this.addToSelection(_1);}}}}this._endUpdate();}});}});fw.ui.dojox.grid.DataGrid.markupFactory=function(_6,_7,_8,_9){return dojox.grid._Grid.markupFactory(_6,_7,_8,dojo.partial(dojox.grid.DataGrid.cell_markupFactory,_9));};}
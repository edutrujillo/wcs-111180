/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dnd.util"],["require","fw.data.util"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dnd.util"]){_4._hasResource["fw.ui.dnd.util"]=true;_4.provide("fw.ui.dnd.util");_4.require("fw.data.util");fw.ui.dnd.util={isTreeSource:function(_7){return "typeAttr" in _7&&"dataAttr" in _7;},isGridSource:function(_8){return "grid" in _8;},treeSourceCreator:function(_9,_a){var n=_4.create("div"),i=_9.item,_b=_9.tree,_c=_9.tree.model.store,_d=_c.getLabelAttributes()[0],_e={node:n,data:fw.data.util.serializeStoreItem(_c,i,["id",_b.typeAttr,"name","subtype"]),type:[_c.getValue(i,_b.typeAttr)],subtype:[_c.getValue(i,"subtype")]};return _e;},gridSourceCreator:function(_f,_10){var n=_4.create("div"),_11=_f.store,_12=_11._items[_10],_13=_11.getLabelAttributes()[0],ret={node:n,data:fw.data.util.serializeStoreItem(_11,_12,["asset","name","subtype"]),subtype:[_11.getValue(_12,"subtype")],type:[_11.getValue(_12,"asset").type]};ret.data.type=ret.data.asset.type;ret.data.id=ret.data.asset.id;return ret;},getNormalizedData:function(_14,_15){var _16=_14.getItem(_15.id).data;if(_16.item&&_16.tree){return fw.data.util.serializeStoreItem(_16.tree.model.store,_16.item);}return _16;},destroyItem:function(_17,_18){_17.delItem(_18.id);_4.destroy(_18);}};}}};});
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dnd.IFrameLogic"]){dojo._hasResource["fw.ui.dnd.IFrameLogic"]=true;dojo.provide("fw.ui.dnd.IFrameLogic");dojo.require("dojo.dnd.Manager");dojo.require("fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar");fw.ui.dnd.IFrameLogic={transferSource:function(_1){if(_1.source.grid){var m=dojo.dnd.manager();var _2=m.makeAvatar;m._dndPlugin=_1.source.dndPlugin;m.makeAvatar=function(){var _3=new fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar(m);return _3;};m.startDrag(_1.source,_1.source.dndElem.getDnDNodes(),_1.copy);m.makeAvatar=_2;}else{dojo.dnd.manager().startDrag(_1.source,_1.nodes,_1.copy);}},returnSource:function(){var _4=dojo.dnd.manager();if(_4.source){return _4;}},cancelDnd:function(){dojo.publish("/dnd/cancel");dojo.dnd.manager().stopDrag();}};}
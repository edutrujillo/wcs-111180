/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.ObjectFactory"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.ObjectFactory"]){_4._hasResource["fw.ui.ObjectFactory"]=true;_4.provide("fw.ui.ObjectFactory");(function(){var _7={managers:{},views:{},services:{}};function _8(_9){if(_9){var _a=_4.getObject(_9);if(typeof _a=="function"){return new _a();}}return null;};function _b(_c,_d){var c=_7[_d][_c];if(c){if(_4.isString(c)){c=_7[_d][_c]=_8(c);}return c;}console.error("[ObjectFactory] - no entry with key \""+_c+"\" found in section \""+_d+"\"");return null;};fw.ui.ObjectFactory={init:function(a){_7=a;},createManagerObject:function(_e){return _b(_e,"managers");},createViewObject:function(_f){return _b(_f,"views");},createServiceObject:function(_10){return _b(_10,"services");},getController:function(_11){return _b(_11,"controllers");},getManager:function(_12){return _b(_12,"managers");},getService:function(_13){return _b(_13,"services");}};})();}}};});
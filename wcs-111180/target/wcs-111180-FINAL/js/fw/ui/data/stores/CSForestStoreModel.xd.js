/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.data.stores.CSForestStoreModel"],["require","dijit.tree.ForestStoreModel"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.data.stores.CSForestStoreModel"]){_4._hasResource["fw.ui.data.stores.CSForestStoreModel"]=true;_4.provide("fw.ui.data.stores.CSForestStoreModel");_4.require("dijit.tree.ForestStoreModel");_4.declare("fw.ui.data.stores.CSForestStoreModel",_5.tree.ForestStoreModel,{getChildren:function(_7,_8,_9){var _a;if(_7.root==true){var _b="init";var id="0";var qs={oper:"init",startQpnt:0};var _c="Root";_4.mixin(_7,{loadUrl:"init",refreshKey:"Root"});}else{var _b=this.store.getValue(_7,"loadUrl");var id=this.store.getValue(_7,"id");var _d=this.store.getValue(_7,"assetType");var qs=fw.ui.app.getFetchCommand();}var _a={loadUrl:_b,id:id,oper:qs.oper,startQpnt:qs.startQpnt};if(!_b){var _e="Unable to locate loadUrl.";console.warn(_e);_8([]);return;}var _f=this;this.store.fetch({query:_4.mixin(_a,this.query),onComplete:_8,onError:function(err){_4.publish("/fw/ui/app/onerror",[err]);}});},mayHaveChildren:function(_10){return _4.some(this.childrenAttrs,function(_11){return this.store.hasAttribute(_10,_11)?this.store.getValue(_10,_11,true)==="true":false;},this);}});}}};});
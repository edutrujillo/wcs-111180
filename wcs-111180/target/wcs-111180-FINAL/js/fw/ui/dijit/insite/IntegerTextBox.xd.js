/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.insite.IntegerTextBox"],["require","dijit.form.NumberTextBox"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.insite.IntegerTextBox"]){_4._hasResource["fw.ui.dijit.insite.IntegerTextBox"]=true;_4.provide("fw.ui.dijit.insite.IntegerTextBox");_4.require("dijit.form.NumberTextBox");_4.declare("fw.ui.dijit.insite.IntegerTextBox",_5.form.NumberTextBox,{_setConstraintsAttr:function(_7){_7.places=0;this.inherited(arguments);}});}}};});
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.FileUploader"]){dojo._hasResource["fw.ui.dijit.FileUploader"]=true;dojo.provide("fw.ui.dijit.FileUploader");dojo.require("dojox.form.FileUploader");dojo.require("dijit.ProgressBar");dojo.declare("fw.ui.dijit.FileUploader",dojox.form.FileUploader,{getHiddenWidget:function(){var _1=this.inherited(arguments);if(_1&&dojo.position(_1.domNode).h>0){return null;}return _1;},postMixInProperties:function(){this.inherited(arguments);if(this.progressWidgetId&&this.progressWidgetContainer){var _2=dojo.create("div",{id:this.progressWidgetId,innerHTML:fw.util.getString("UI/UC1/JS/Uploading")},dojo.byId(this.progressWidgetContainer));this._progressBar=new dijit.ProgressBar({},_2);this._progressBar.startup();dojo.addClass(this._progressBar.domNode,"fileUploaderLoading");}},getText:function(_3){var cn=dojo.trim(_3.innerHTML);if(cn.indexOf("<")>-1&&this.uploaderType=="flash"){cn=escape(cn);}return cn;}});}
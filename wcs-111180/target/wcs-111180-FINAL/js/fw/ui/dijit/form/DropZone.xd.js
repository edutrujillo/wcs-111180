/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.form.DropZone"],["require","dijit._Widget"],["require","dijit._Templated"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.form.DropZone"]){_4._hasResource["fw.ui.dijit.form.DropZone"]=true;_4.provide("fw.ui.dijit.form.DropZone");_4.require("dijit._Widget");_4.require("dijit._Templated");_4.declare("fw.ui.dijit.form.DropZone",[_5._Widget,_5._Templated],{templateString:_4.cache("fw.ui.dijit.form","templates/DropZone.html","<div>\r\n\t<div class=\"DropOuter\">\r\n\t\t<div class=\"MainDropDiv\">\r\n\t\t\t<div class=\"DropHand\"></div>\r\n\t\t\t<div class=\"DropText\">${strDropZone}</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"),baseClass:"DropZone",strDropZone:fw.util.getString("UI/UC1/JS/DropZone")});}}};});
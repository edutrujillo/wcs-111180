/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.CommonButtonMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.CommonButtonMixin"]){_4._hasResource["fw.ui.dijit.CommonButtonMixin"]=true;_4.provide("fw.ui.dijit.CommonButtonMixin");_4.declare("fw.ui.dijit.CommonButtonMixin",[],{createButton:function(_7){var _8=_4.create("div",{"class":"inline-left"});var _9=_4.create("div",{"class":"button-left"},_8,"last");var _a=_4.create("div",{"class":"button-middle"},_8,"last");var _b=_4.create("div",{"class":"button-text",innerHTML:_7},_a,"last");var _c=_4.create("div",{"class":"button-right"},_8,"last");return _8;}});}}};});
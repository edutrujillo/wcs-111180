/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.manager.inner.DashFormManager"]){dojo._hasResource["fw.ui.manager.inner.DashFormManager"]=true;dojo.provide("fw.ui.manager.inner.DashFormManager");dojo.declare("fw.ui.manager.inner.DashFormManager",null,{DASHUI_URL:"",openForm:function(_1){},refreshDashSession:function(_2){if(_2){var _3="faces/jspx/refreshDashSession.jspx?assetid="+_2;var _4={url:_3};this.REQUEST_SERVICE.sendRequest(_4);}}});}
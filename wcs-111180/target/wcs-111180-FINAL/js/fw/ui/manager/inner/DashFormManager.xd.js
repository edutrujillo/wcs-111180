/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.manager.inner.DashFormManager"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.manager.inner.DashFormManager"]){_4._hasResource["fw.ui.manager.inner.DashFormManager"]=true;_4.provide("fw.ui.manager.inner.DashFormManager");_4.declare("fw.ui.manager.inner.DashFormManager",null,{DASHUI_URL:"",openForm:function(_7){},refreshDashSession:function(_8){if(_8){var _9="faces/jspx/refreshDashSession.jspx?assetid="+_8;var _a={url:_9};this.REQUEST_SERVICE.sendRequest(_a);}}});}}};});
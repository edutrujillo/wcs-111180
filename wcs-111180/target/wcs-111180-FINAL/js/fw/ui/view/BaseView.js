/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.BaseView"]){dojo._hasResource["fw.ui.view.BaseView"]=true;dojo.provide("fw.ui.view.BaseView");(function(){var _1=0,_2=function(_3){_1+=1;return _3+"-"+_1;};dojo.declare("fw.ui.view.BaseView",dojo.Stateful,{dirty:false,initialized:false,editable:false,constructor:function(){this.id=_2("view");},init:function(){this.inherited(arguments);},show:function(){},destroy:function(){},info:function(_4){this.message(_4,"info");},error:function(_5){this.message(_5,"error");},warn:function(_6){this.message(_6,"warn");},message:function(_7,_8){if(this.appContext){this.appContext.message(_7,_8);}else{}},feedback:function(_9,_a){var _b=this.appContext;if(_b){_b.feedback(_9,_a);}},clearMessage:function(){var _c=this.appContext;if(_c){_c.clearMessage();}},prompt:function(_d){return this.appContext.prompt(_d);},prompt:function(_e,_f){return this.appContext.prompt(_e,_f);},feedback:function(_10,_11){},clearFeedback:function(){},isEditable:function(){return this.editable;},isEditMode:function(){var _12=this.get("mode");return (_12&&_12==="edit");}});}());}
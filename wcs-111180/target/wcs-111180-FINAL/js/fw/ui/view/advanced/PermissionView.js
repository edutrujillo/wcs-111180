/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.advanced.PermissionView"]){dojo._hasResource["fw.ui.view.advanced.PermissionView"]=true;dojo.provide("fw.ui.view.advanced.PermissionView");dojo.require("fw.ui.view.AdvancedView");dojo.require("fw.ui.view.TabbedViewMixin");dojo.require("fw.ui.controller.BaseDocController");dojo.declare("fw.ui.view.advanced.PermissionView",[fw.ui.controller.BaseDocController,fw.ui.view.AdvancedView,fw.ui.view.TabbedViewMixin],{disableDocumentActions:true,getAdvancedURLParams:function(){var _1=this.model.get("asset");if(_1){return {ThisPage:"AssetSecurityDetailsFront",PostPage:"AssetSecurityDetailsPost",AssetType:_1.type,id:_1.id};}else{throw new Error(fw.util.getString("UI/UC1/JS/CannotRenderPermission"));}}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.InnerConfig"]){dojo._hasResource["fw.ui.InnerConfig"]=true;dojo.provide("fw.ui.InnerConfig");dojo.require("fw.ui.manager.inner.AssetDataManager");dojo.require("fw.ui.manager.inner.SlotManager");dojo.require("fw.ui.manager.inner.InsiteManager");dojo.require("fw.ui.manager.inner.AdvancedFormManager");dojo.require("fw.ui.manager.AssetManager");dojo.require("fw.ui.service.RequestService");dojo.require("fw.ui.service.InsiteDataService");dojo.require("fw.ui.service.AssetDataService");fw.ui.InnerConfig={"aliases":{"managers":{"assetdata":"fw.ui.manager.inner.AssetDataManager","slot":"fw.ui.manager.inner.SlotManager","insite":"fw.ui.manager.inner.InsiteManager","form":"fw.ui.manager.inner.AdvancedFormManager","asset":"fw.ui.manager.AssetManager"},"views":{},"services":{"request":"fw.ui.service.RequestService","insite":"fw.ui.service.InsiteDataService","versioning":"fw.ui.service.VersioningService","asset":"fw.ui.service.AssetDataService"}}};}
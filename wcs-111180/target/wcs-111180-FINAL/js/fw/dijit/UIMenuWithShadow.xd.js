/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UIMenuWithShadow"],["require","dijit.Menu"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UIMenuWithShadow"]){_4._hasResource["fw.dijit.UIMenuWithShadow"]=true;_4.provide("fw.dijit.UIMenuWithShadow");_4.require("dijit.Menu");_4.declare("fw.dijit.UIMenuWithShadow",_5.Menu,{templateString:"<table class=\"dijit dijitMenu dijitMenuPassive dijitReset dijitMenuTable\" waiRole=\"menu\" tabIndex=\"${tabIndex}\" dojoAttachEvent=\"onkeypress:_onKeyPress\"><tr><td class=\"dijitReset dijitTableBody\" dojoAttachPoint=\"containerNode\"></td></tr></table>"});}}};});
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UIPopupMenuItem"],["require","dijit.PopupMenuItem"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UIPopupMenuItem"]){_4._hasResource["fw.dijit.UIPopupMenuItem"]=true;_4.provide("fw.dijit.UIPopupMenuItem");_4.require("dijit.PopupMenuItem");_4.declare("fw.dijit.UIPopupMenuItem",_5.PopupMenuItem,{templateString:"<table class='popupMenuItem' cellspacing='0' cellpadding='0' style='width:100%;'><tr class='dijitReset dijitMenuItem' dojoAttachPoint='focusNode' waiRole='menuitem' tabIndex='-1' dojoAttachEvent='onmouseenter:_onHover,onmouseleave:_onUnhover,ondijitclick:_onClick'><td class='dijitReset' waiRole='presentation'><img src='${_blankGif}' alt='' class='dijitMenuItemIcon' dojoAttachPoint='iconNode'></td><td class='dijitReset dijitMenuItemLabel' colspan='2' dojoAttachPoint='containerNode'></td><td class='dijitReset dijitMenuItemAccelKey' style='display:none' dojoAttachPoint='accelKeyNode'></td><td class='dijitReset dijitMenuArrowCell' waiRole='presentation'><div dojoAttachPoint='arrowWrapper' style='visibility:hidden'><img src='${_blankGif}' alt='' class='dijitMenuExpand'><span class='dijitMenuExpandA11y'>+</span></div></td></tr></table>",action:"",alwaysEnabled:false,deferred:false,});}}};});
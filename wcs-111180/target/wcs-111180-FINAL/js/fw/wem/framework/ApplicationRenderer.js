/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.wem.framework.ApplicationRenderer"]){dojo._hasResource["fw.wem.framework.ApplicationRenderer"]=true;dojo.provide("fw.wem.framework.ApplicationRenderer");dojo.require("fw.wem.framework.LayoutRenderer");dojo.declare("fw.wem.framework.ApplicationRenderer",null,{constructor:function(_1,_2){this.application=_1;this.applicationbody=_2;},render:function(){var _3=this.application.layouttype;var _4=eval(_3);if(_4){var _5=new _4(this.application.id);_5.render(this.applicationbody);}}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["webcenter.sites.config"]){dojo._hasResource["webcenter.sites.config"]=true;dojo.provide("webcenter.sites.config");webcenter.sites.config={maxTabCount:50,"aliases":{"managers":{"asset":"fw.ui.manager.AssetManager","dialog":"fw.ui.manager.DialogManager","tree":"fw.ui.manager.TreeManager","insite":"fw.ui.manager.inner.InsiteManager","slot":"fw.ui.manager.inner.SlotManager","assetdata":"fw.ui.manager.inner.AssetDataManager"},"controllers":{"menu":"fw.ui.controller.MenuController","document":"fw.ui.controller.DocumentController","tree":"fw.ui.controller.TreeController","tab":"fw.ui.controller.TabController","search":"fw.ui.controller.SearchController","dashboard":"fw.ui.controller.DashboardController","bulk":"fw.ui.controller.BulkController","bulkasset":"fw.ui.controller.BulkAssetController","bulkproxyasset":"fw.ui.controller.BulkProxyAssetController"},"views":{"dialog":"fw.ui.view.DialogView"},"services":{"request":"fw.ui.service.RequestService","insite":"fw.ui.service.InsiteDataService","dialog":"fw.ui.service.DialogDataService","asset":"fw.ui.service.AssetDataService","approval":"fw.ui.service.ApprovalService","versioning":"fw.ui.service.VersioningService","storage":"fw.ui.service.StorageService"}}};}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <title></title>
        <meta name="google" value="notranslate">         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Include CSS to eliminate any default margins/padding and set the height of the html element and
		     the body element to 100%, because Firefox, or any Gecko based browser, interprets percentage as
			 the percentage of the height of its parent container, which has to be set explicitly.  Fix for
			 Firefox 3.6 focus border issues.  Initially, don't display flashContent div so it won't show
			 if JavaScript disabled.
		-->
        <style type="text/css" media="screen"> 
			html, body	{ height:100%; }
            body { margin:0; padding:0; overflow:auto; text-align:center; background-color: #ffffff; }
			object:focus { outline:none; }
			#flashContent { display:none; }
        </style>

        <script type="text/javascript" src="swfobject.js"></script>

        <script type="text/javascript">
            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
            var swfVersionStr = "10.0.0";
            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
            var xiSwfUrlStr = "playerProductInstall.swf";

            // Store image file name
            var savedImage ="";
            var flashvars = {};

            // Change this according to script language
            var fileExtension = ".asp"; //".aspx" , ".php", ".asp"

            // Change these variables according to your needs
            var snapshotScript = "snapshot" + fileExtension;
            var showresultScript = "showresult" + fileExtension;
            var customizerScript = "customizer" + fileExtension;
            var helpScript = "showhelp.html";
            var uploadsDir = "uploads/";
            var snapshotsDir = "snapshots/"

            // !!! Remove this in product version !!!
            flashvars.AdminID = "1";

            // Uncomment one of two
            //Start Clarkii with clarkii.png image from Clarkii's uploads directory.
            //flashvars.StartupProject = uploadsDir + "clarkii.png";

            //Start Clarkii with startup.xml project from Clarkii's projects directory.
            flashvars.StartupProject = "startup";

            flashvars.CustomizerScript = customizerScript;


            var params = {};
            params.quality = "high";
            params.bgcolor = "#ffffff";
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            var attributes = {};
            attributes.id = "clarkii4";
            attributes.name = "clarkii4";
            attributes.align = "middle";
            swfobject.embedSWF(
                "starter.swf", "flashContent",
                "100%", "100%",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);
			<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
			swfobject.createCSS("#flashContent", "display:block;text-align:left;");



            // Clarkii functions

            function onClarkiiStart(){
               // Put your code here
            }

            function showClarkiiHelp(){
               //location.href = helpScript;
               window.open(helpScript);
            }

            function saveClarkiiSnapshot(args){
                try{
                    var saveLocally = args[0]
                    var fileName = args[1];
                    var jpgQuality = args[2];
                } catch(e){
                    alert(e);
                }

                if (fileName == ""){
                    fileName = flashvars.StartupProject;
                }

                var extension = fileName.split(".").pop();
                fileName = fileName.replace(uploadsDir, "");

                // Uncoment/Comment this for random/not random file names
				var rand = 1000000 + Math.floor((9999999, 1000000 + 1) *  Math.random());
                fileName = rand + "." + extension;

                // Store file name
                savedImage = fileName;

                fileName = snapshotsDir + fileName;

                //clarkii().getAsBase64(extension, jpgQuality);
                clarkii().saveSnapshot(saveLocally, extension, jpgQuality, snapshotScript + "?fileName=" + fileName);

            }

            function onClarkiiSnapshot(data){
                window.open(showresultScript + "?image=" + snapshotsDir + savedImage);
                //location.href = "showresultScript + "?image=" + snapshotsDir + savedImage;
            }

            // Additional functions

            function save(){

                savedImage = document.getElementById("filename").value;

                clarkii().saveSnapshot(document.getElementById("savelocally").checked,
                                       document.getElementById("imagetype").value,
                                       document.getElementById("jpgquality").value,
                                       document.getElementById("scriptfile").value + "?fileName=" + snapshotsDir + savedImage);

            }

            function onSaveLocallyClick(){
                if (document.getElementById("savelocally").checked) {
                    document.getElementById("filename").disabled="disabled";
                    document.getElementById("scriptfile").disabled="disabled";
                } else {
                    document.getElementById("filename").disabled="";
                    document.getElementById("scriptfile").disabled="";
                }
            }

            function onImageTypeChange(){

                if (document.getElementById("imagetype").value == "jpg"){
                    document.getElementById("jpgquality").disabled="";
                } else {
                   document.getElementById("jpgquality").disabled="disabled";
                }
            }

            function onStartupFileChange(){

                flashvars.StartupProject = document.getElementById("startupfile").value;

                restartClarkii();

            }

            function restartClarkii(){

                swfobject.removeSWF("clarkii4");

                var newNode = document.createElement("div");
                newNode.setAttribute("id", "flashContent");
                document.body.appendChild(newNode);

                swfobject.embedSWF(
                    "starter.swf", "flashContent",
                    "100%", "100%",
                    swfVersionStr, xiSwfUrlStr,
                    flashvars, params, attributes);
            }

            function getScriptExtension() {
                var ext = window.location.pathname.split('?')[0];
                return ext.split('.').pop();
            }

	        function clarkii(){

				return swfobject.getObjectById("clarkii4");

			}


        </script>
    </head>
    <body>
        <!--
        <div align="left">
        Startup file:
        <select id="startupfile">
        <option value="uploads/clarkii.png">Image with relative path: uploads/clarkii.png</option>
        <option value="startup" selected>Startup project: startup</option>
        <option value="http://wp.clarkii.com/qm.png">Image from the same domain: http://wp.clarkii.com/qm.png</option>
        <option value="http://wp.clarkii.com/clarkii-proxy/?url=http://www.liveaquaria.com/images/categories/product/p-26101-clarkii.jpg">Image from internet via proxy: http://wp.clarkii.com/clarkii-proxy/?url=http://www.liveaquaria.com/images/categories/product/p-26101-clarkii.jpg </option>
        </select>
        <input type="button" value="Restart Clarkii" onclick="onStartupFileChange()">
        <br />

        Save locally:
        <input id="savelocally" type="checkbox" value="" onclick="onSaveLocallyClick()"></input>

        Image type:
        <select id="imagetype" onchange="onImageTypeChange()">
        <option value="jpg" selected>jpg</option>
        <option value="gif">gif</option>
        <option value="png">png</option>
        </select>

        Jpg quality:
        <select id="jpgquality">
        <option value="80" selected>80</option>
        <option value="40">40</option>
        <option value="60">60</option>
        </select>

        Script file:
        <input id="scriptfile"  type="text" value="snapshot.php" />

        File name:
        <input id="filename" type="text" value="snapshot.jpg" />

        <input type="button" value="Save" onclick="save()">
        </div>
        <hr/>

        -->

        <!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough
			 JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
			 when JavaScript is disabled.
		-->


        <div id="flashContent">
        	<p>
	        	To view this page ensure that Adobe Flash Player version
				10.0.0 or greater is installed.
			</p>
			<script type="text/javascript">
				var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://");
				document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='"
								+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" );
			</script>
        </div>	   	
   </body>
</html>

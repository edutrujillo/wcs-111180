<%@page import="com.fatwire.cache.ehcache.EhCacheObjectCache.Dependency"%>
<%@ page import="java.util.*"%>
<%@ page import="COM.FutureTense.Interfaces.IList "%>
<%@ page import="COM.FutureTense.Cache.*"%>
<%@ page import="COM.FutureTense.Cache.LinkedCache.DateOp"%>
<%@ page import="COM.FutureTense.Cache.LinkedCache.QueryOp"%>
<%@page import="com.fatwire.cache.ehcache.EhCacheObjectCache.CachedObjectWithDependencies"%>
<%@page import="net.sf.ehcache.Element"%>
<%@ page import="com.fatwire.cache.*"%>
<%@ page import="com.fatwire.cache.ehcache.*"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@page import="net.sf.ehcache.Cache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}
String authorize = (String) session.getAttribute("userAuthorized");
if (Boolean.valueOf(authorize)) {
String contextPath= request.getContextPath();

String currentPage = request.getParameter("pageNum");
currentPage = StringEscapeUtils.escapeHtml(currentPage);
currentPage = "".equals(currentPage) || currentPage == null ? "1" : currentPage;

String rowsPerPage = request.getParameter("rowsPerPage");
rowsPerPage = StringEscapeUtils.escapeHtml(rowsPerPage);
rowsPerPage = "".equals(rowsPerPage) || rowsPerPage == null ? "10" : rowsPerPage;

int pageNum = Integer.parseInt(currentPage);
int iRowsPerPage = Integer.parseInt(rowsPerPage);

int totalCount = 0;

String searchBy = request.getParameter("searchBy");
if(searchBy == null || "".equals(searchBy))
	searchBy = "Key";
String matchType = request.getParameter("matchTypeSelect");
String searchKey = request.getParameter("searchKey");
searchKey = StringEscapeUtils.escapeHtml(searchKey);

String[] searchTypeOptions =
	{ "Key", "Expired Time" };
StringBuilder srchTypeOpsSb = new StringBuilder();

Map searchTypeTextMap = new HashMap();
%>
<jsp:include page="/cachetool/serverTimeZoneInfo.jsp"/>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="searchType.optionText.key"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put("Key",(String)request.getAttribute("searchType.optionText.key"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="searchType.optionText.expiredTime"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put("Expired Time",(String)request.getAttribute("searchType.optionText.expiredTime"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.optionText.any"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put(QueryOp.ANY.name(),(String)request.getAttribute("assetCache.optionText.any"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.optionText.all"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put(QueryOp.ALL.name(),(String)request.getAttribute("assetCache.optionText.all"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="page.optionText.before"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put(DateOp.BEFORE.name(),(String)request.getAttribute("page.optionText.before"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="page.optionText.after"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put(DateOp.AFTER.name(),(String)request.getAttribute("page.optionText.after"));
%>
<!--  escaped locale strings for javascript -->
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.optionText.any"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put("escaped:"+QueryOp.ANY.name(),(String)request.getAttribute("assetCache.optionText.any"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.optionText.all"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put("escaped:"+QueryOp.ALL.name(),(String)request.getAttribute("assetCache.optionText.all"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="page.optionText.before"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put("escaped:"+DateOp.BEFORE.name(),(String)request.getAttribute("page.optionText.before"));
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="page.optionText.after"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<%
searchTypeTextMap.put("escaped:"+DateOp.AFTER.name(),(String)request.getAttribute("page.optionText.after"));

for(String op : searchTypeOptions)
{
	srchTypeOpsSb.append("<option value=\"").append(op)
		.append("\"");
	if(op.equals(searchBy))
		srchTypeOpsSb.append(" selected");
	srchTypeOpsSb.append(">").append(searchTypeTextMap.get(op)).append("</option>");
}

String[][] searchBySelectedOptions =
	{ { DateOp.BEFORE.name(), DateOp.AFTER.name() },
		{ QueryOp.ANY.name(), QueryOp.ALL.name() } };

String[] selectedOptions = new String[2];
StringBuilder matchTypeOpsSb = new StringBuilder();

//set select ops for searchBy and matchType
if(!"Key".equals(searchBy))
{
	for(int i = 0; i < selectedOptions.length; i++)
	{
		if("Expired Time".equals(searchBy))
			selectedOptions[i] = searchBySelectedOptions[0][i];
		else if("Dependency".equals(searchBy))
			selectedOptions[i] = searchBySelectedOptions[1][i];
		else
			selectedOptions[i] = "";
	}

	for(String op : selectedOptions)
	{
		matchTypeOpsSb.append("<option value=\"").append(op)
			.append("\"");
		if(op.equals(matchType))
			matchTypeOpsSb.append(" selected");
		matchTypeOpsSb.append(">").append(searchTypeTextMap.get(op))
			.append("</option>");
	}
}
	String cache = request.getParameter("cachename");
	Set<String> keySet = null;
	if(null != cache)
	{
		LinkedCache c = LinkedCacheProvider.INSTANCE.getCache(cache);

		if("Expired Time".equals(searchBy))
		{
			Date eDate = CacheUtil.getSearchedDate(searchKey);
			DateOp dateOp = DateOp.valueOf(matchType);
			keySet =
				c.findKeysByExpirationTime(eDate, dateOp, 0);
		}
		else if("Key".equals(searchBy))
		{
			String[] targets = null;
			if(searchKey == null || "".equals(searchKey))
			{
				targets = new String[1];
				targets[0] = "*";
			}
			else
				targets = searchKey.split(",");
			keySet = c.findKeys(targets, 0);
			
		}
		totalCount = keySet.size();
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="page.dateFormat"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="page.invalidSearch"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<html>
<head>
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css' rel="stylesheet" type="text/css" />
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css' rel="stylesheet" type="text/css" />
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css' rel="stylesheet" type="text/css">
<script type='text/javascript'>
function submitSearchForm(action,pageNum) {
  document.searchForm.action = './list.jsp?cachename=' + '<%=cache%>' +'&pageNum='+pageNum;
  document.searchForm.submit();
}
function checkBeforeSubmit() 
{
	var re = /^\d{1,2}\/\d{1,2}\/\d{2,4}( \d{2}:\d{2}:\d{2})?$/
	var frm = document.searchForm;
	
	if (document.getElementById("searchBy").value === "Expired Time" && !re.test(frm.searchKey.value))
	{
		alert ("<%=request.getAttribute("page.dateFormat")%>");
		return false;
	}
	var searchKeyObj = document.searchForm.searchKey;
	if(searchKeyObj.value.indexOf("<",0) == -1 
		&& searchKeyObj.value.indexOf(">",0) == -1)
	{
		document.searchForm.action = './list.jsp?cachename=' + '<%=cache%>' +'&pageNum=1';
		document.searchForm.submit();
	}
	else 
		alert("<%=request.getAttribute("page.invalidSearch")%>");
}
function onSearchTypeSelection()
{
	var selectElem = document.searchForm.searchBy;
	var searchForm = document.searchForm;
	
	if (selectElem.selectedIndex ==1 )
	{
		searchForm['matchTypeSelect'].style.visibility='visible';
		searchForm['matchTypeSelect'].options[0]=new Option("<%=searchTypeTextMap.get("escaped:"+DateOp.BEFORE.name())%>", "BEFORE", true, false)
		searchForm['matchTypeSelect'].options[1]=new Option("<%=searchTypeTextMap.get("escaped:"+DateOp.AFTER.name())%>", "AFTER", false, false)
		
	}
	else if (selectElem.selectedIndex==2)
	{
		searchForm['matchTypeSelect'].style.visibility='visible';
		searchForm['matchTypeSelect'].options[0]=new Option("<%=searchTypeTextMap.get("escaped:"+QueryOp.ANY.name())%>", "ANY", true, false)
		searchForm['matchTypeSelect'].options[1]=new Option("<%=searchTypeTextMap.get("escaped:"+QueryOp.ALL.name())%>", "ALL", false, false)
	}
	else 
	{
		searchForm['matchTypeSelect'].style.visibility='hidden';
	}
	
}
</script>
<%@ include file="./paging.jsp"%>
</head>
<body>
<h4>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="list.cache"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%=request.getAttribute("list.cache")%> : <%=StringEscapeUtils.escapeHtml(cache)%></h4>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" style="margin-top: 5px">
<tr> 
    <td></td>
    <td class="tile-dark" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	<td></td>
</tr>
<tr>
	<td class="tile-dark" VALIGN="top" WIDTH="1" nowrap="true"><br/></td>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
	
	<tr>
		<td class="tile-a" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="list.size"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="list.capacity"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b " background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.fillRatio"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.hits"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b " background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.misses"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.hitRatio"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.missesExp"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.getTime"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.puts"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.updates"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.expired"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.removed"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.evicted"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.eternalQ"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.timeToLive"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<span class="new-table-title" title="<%=request.getAttribute("linkedCache.tblTitle.timeToLive")%>">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.TTL"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.timeToLive"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<span class="new-table-title" title='<%=request.getAttribute("linkedCache.tblTitle.timeToLive")%>'>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.TTI"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
	<td><br/></td>
	<%
		
        CacheStats stat = c.getCacheStats();
        CacheRegionConfig conf = c.getConfig();
	%>
		    <td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=c.keys().size()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.getMaxElementsInMemory()%></DIV>
			</td>
			<td><br/></td>
			<% double fillRatio =  ((double)stat.getSize()/(double)(conf.getMaxElementsInMemory()));%>
			<td VALIGN="center" ALIGN="LEFT">
			<span style="padding: 0; float: left; white-space: nowrap;"><%
			if(fillRatio != 0.0) {%><%
			%><img height="12" width="<%=(int)(fillRatio*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar.png" style="padding: 0"><%}if(fillRatio < 1.0) 
			{%><img height="12"  width="<%=(int)((1.0-fillRatio)*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar_background.png" style="padding: 0"><%
			}%>&nbsp;<%=(int)(fillRatio*100)%>%</span></td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getHitCount()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getMissCount()%></DIV>
			</td>
			<% double hitRatio =  ((double)stat.getHitCount()/(double)(stat.getHitCount() + stat.getMissCount()));%>
			<td><br/></td>
			<td VALIGN="center" ALIGN="LEFT" >
			<span style="padding: 0; float: left; white-space: nowrap;"><%
			if(hitRatio != 0.0) {
			%><img height="12" width="<%=(int)(hitRatio*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar.png" style="padding: 0"><%}if(hitRatio < 1.0) 
			{%><img height="12" width="<%=(int)((1.0-hitRatio)*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar_background.png" style="padding: 0"><%}
			%>&nbsp;<%=(int)(hitRatio*100)%>%</span></td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getMissCountForExpire()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
			<%
			 double d = (int)((stat.getAverageGetTime()*1000));
			 d/=1000;
			%>
		    <DIV class="small-text-inset"><%=d%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfPuts()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfUpdates()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfExpired()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfRemoved()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfEvicted()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.isEternal()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.getTimeToLiveSeconds()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.getTimeToIdleSeconds()%></DIV>
			</td>
			<td><br/></td>
			
	</tr>
	</table>
	</td>
		<td class="tile-dark" VALIGN="top" WIDTH="1" nowrap="true"><br/></td>
		</tr>
	<tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td></tr>
	<tr><td></td><td background='<%=contextPath %>/Xcelerate/graphics/common/screen/shadow.gif'><IMG WIDTH="1" HEIGHT="5" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /><td></td></tr>
</table>
	<br/>
	<br/>
<form name="searchForm" method="post">
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<input type='hidden' id="pageNum" name='pageNum' value='1' />
<div style="float:left">
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td></td>
		<td height="1" valign="TOP" class="tile-dark"><img height="1"
			width="1"
			src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td width="1" valign="top" class="tile-dark">
			<br/>
		</td>
		<td>
		<table cellspacing="0" cellpadding="0" border="0"
			bgcolor="#ffffff" width="100%">
			<tr>
				<td colspan=4 class="tile-highlight"><img
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
					width="1" height="1"></td>
			</tr>
			<tr>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					class="tile-a">&nbsp;</td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					colspan="2" class="tile-b">&nbsp;</td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					class="tile-c">&nbsp;</td>
			</tr>
			<tr>
				<td class="tile-dark" colspan="4"><img height="1" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
			</tr>
			<tr class="form-shade">
				<td>
				<div class="form-label-inset">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="search.itemsPerPage"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</div>
				</td>
				<td><input type="text" name="rowsPerPage" id="rowsPerPage"
					class="form-inset" size=10 value='<%=rowsPerPage%>' /></td>
				<td>
				<div class="form-label-inset">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="search.searchBy"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</div>
				</td>
				<td><select name="searchBy" id="searchBy"
									class="form-inset" onchange="onSearchTypeSelection();"><%=srchTypeOpsSb.toString()%></select>
								<select name="matchTypeSelect" id="matchTypeSelect"
									class="form-inset"
									style='visibility:<%=(searchBy == null || searchBy.equals("Key"))
                    ? "hidden" : "visible"%>;'><%=matchTypeOpsSb.toString()%></select>
								</td>
			</tr>
			<tr class="form-shade">
				<td colspan="4" height="5"></td>
			</tr>
			<tr valign=middle>
				<td colspan=4><textarea class="form-inset" name="searchKey"
					id="searchKey" rows="4" cols="60"><%=(null == searchKey) ? "" : searchKey%></textarea>
				</td>
			</tr>
		</table>
		</td>
		<td width="1" valign="top" class="tile-dark"><br>
		</td>
	</tr>
	<tr>
		<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
			height="1" width="1"
			src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
	</tr>
	<tr>
		<td></td>
		<td background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'>
			<img height="5" width="1"
			src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
		<td></td>
	</tr>
	<tr>
				<td/>
				<td align=right>
					<div onclick="checkBeforeSubmit();" style="float:right">
					<jsp:include page="/cachetool/addTextButton.jsp">
						<jsp:param  name="key" value="search.button.search"/>
						<jsp:param name="encode" value="true"/>
					</jsp:include>
					</div>
				</td>
				<td/>
	</tr>
</table>
</div>
<div style="float:right">
<fieldset><legend>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="search.tableLegend"/>
	<jsp:param name="encode" value="true"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
</legend>
		<table cellspacing="3" cellpadding="0" border="0">
			<tr>
				<td class="strikeBackGround">Key=csDataSource-Content_C-*-id=1114083739888</td>
				<td class="legend-text">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="search.invalidatedPage"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</td>
			</tr>
			<tr>
				<td class="invalidateDep">csDataSourceContent_C</td>
				<td class="legend-text">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="search.updatedDependency"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</td>
			</tr>
</table>
</fieldset>
</div>
<div style="clear:both">
</div>
<table>
<tr>
		<td align="left">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCache.noItemsInCache"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<%=((String)request.getAttribute("assetCache.noItemsInCache")).replace("{0}","<b>" + Integer.toString(totalCount)) + "</b>"%>
		<div  align='right' id="searchResults" name="searchResults" ></div>
		</td>
		
</tr>
<tr><td>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
<tr> 
    <td></td>
    <td class="tile-dark" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	<td></td>
</tr>
<tr>
	<td class="tile-dark" VALIGN="top" WIDTH="1"><br/></td>
	<td>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
	<tr>
		<td colspan="11" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	</tr>
	<tr>
		<td class="tile-a" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' width="25%">
			<span class="new-table-title">
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="assetCacheDep.key"/>
				<jsp:param name="encode" value="true"/>
				<jsp:param name="render" value="true"/>
			</jsp:include>
			</span>
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="list.dependencies"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span>
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' >&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true' >
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCache.createdTime"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span>
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' >&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true' >
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="list.lastAccessedTime"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span>
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' >&nbsp;&nbsp;
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="list.expiredTime"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span>
		</td>
		<td class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="11" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	</tr>
			<%
            //ObjectCache oc = CacheProvider.getCache("linked-cache");
			CacheManager cacheMgr = CacheUtil.getCacheManager("linked-cache");
			Cache depRep = cacheMgr.getCache("dependencyRepository");
			Cache netEhCache = cacheMgr.getCache(cache);
			
			
            if(null != c)
            {
				int num = 0 ;
				int count = 0;
				int lowerBound = iRowsPerPage * (pageNum - 1);
				int upperBound = lowerBound + iRowsPerPage;
                for(String key : keySet)
                {
                	
					count++;
					if(count > lowerBound)
					{
						if(count <= upperBound)
						{
							Element e = netEhCache.getQuiet(key);
							Object linkedCacheObj = c.get(key);
							if(e!=null)
							{
							CachedObjectWithDependencies cbwd =
                                (CachedObjectWithDependencies) e
                                    .getObjectValue();
                            Date lastAccessDate =
											new Date(e.getLastAccessTime());
								Date createdTime =
											new Date(e.getCreationTime());
								boolean inv = false;
								for(Dependency d1 : cbwd.getDeps() )
								{
									String dep = d1.getKey();
									String classOfDep = "small-text-inset";
									 Element ex = depRep.getQuiet(dep);
	                                    if(!d1.equals((ex == null ? null
	                                        : ex.getObjectValue())))
	                                    {
 	                                    	inv = true;
	                                    	break;
	                                    }
								}
								if(inv)
								{%>
									<tr class="strikeBackGround"><%	
								}
								else if ((num++) % 2 !=0) 
									{ %>
								<tr class="tile-row-highlight">
								<% }
								else {
								  %>
								<tr class="tile-row-normal">
								<% } %>
										<td><br/></td>
										<td VALIGN="TOP" ALIGN="LEFT">
										<DIV class="small-text-inset"><%
										boolean isIList = ( linkedCacheObj instanceof IList ) ;
										if(isIList) {
										String encodedKey  = com.fatwire.cs.core.uri.Util.encode(key);
										%>
										<a href="./linkedCacheResult.jsp?cache=<%=cache%>&key=<%=encodedKey%>">
										<%}%>
										<%=key.replace(",",", ")%><%
										if(isIList) { %></a><%}%>
										</div></td>
										<td><br/></td>
										<td VALIGN="TOP" ALIGN="LEFT" class="padded nowrap">
										<%
										for(Dependency d1 : cbwd.getDeps() )
										{
											String dep = d1.getKey();
											String classOfDep = "small-text-inset";
											 Element ex = depRep.getQuiet(dep);
			                                    if(!d1.equals((ex == null ? null
			                                        : ex.getObjectValue())))
			                                    {
			                                    	classOfDep ="invalidateDep";
			                                    }
			                                    
										%><DIV class="<%=classOfDep%>"><%=dep%></div><%
										}
										%>
										</td>
										<td><br/></td>
										<td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset"><%=CacheUtil.getLocalizedDate(createdTime,locale)%></div></td>
										<td><br/></td>
										<td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset"><%=CacheUtil.getLocalizedDate(lastAccessDate,locale)%></div></td>
										<td><br/></td>
										<td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset"><%=CacheUtil.getLocalizedDate(new Date(e.getExpirationTime()),locale)%></div></td>
										<td><br/></td>
								</tr>
								<%
							
							}
						}
					}
				}
            }
        }
    %>
</table>
	</td>
	<td class="tile-dark" VALIGN="top" WIDTH="1"><br/></td>
	</tr>
<tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td></tr>
<tr><td></td><td background='<%=contextPath %>/Xcelerate/graphics/common/screen/shadow.gif'><IMG WIDTH="1" HEIGHT="5" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /><td></td></tr>

</table></td>
</tr>
</table>
</body>
</form>
<script language="javascript" type="text/javascript">
    var page = new Paging({    renderTop:"searchResults",
        recordCount:<%=totalCount%>,
        onSuccess:'gotoPage',
        currentPage: <%=currentPage%>,
        rowsPerPage:<%=rowsPerPage%>,
        pagePerView:7
    });
</script>
</html>
<%
}else{
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.accessError"/>
		<jsp:param name="encode" value="true"/>
		<jsp:param name="render" value="true"/>
	</jsp:include>
<%}%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@page import="com.fatwire.cache.ehcache.PageCacheProvider"%>
<%@page import="com.fatwire.cache.PageCache"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
    String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
	
	//check access.
    String cacheName = request.getParameter("cachename");
	cacheName = StringEscapeUtils.escapeHtml(cacheName);
	if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
	    cacheName = CacheUtil.SS_CACHE;
	String contextPath = request.getContextPath();
	
	String onCS = request.getParameter("onCS");
	onCS = StringEscapeUtils.escapeHtml(onCS);

    if("true".equals(session
        .getAttribute("userAuthorized")))
    {
        //check if PageCache is available
        PageCache accessor = PageCacheProvider.getCache();
        boolean pcAvail = accessor.isAvailable();
        if(!pcAvail)
        {
            accessor = PageCacheProvider.getCache(CacheUtil.SS_CACHE);
            pcAvail = accessor.isAvailable();
            if(!pcAvail)
            {
                request.getRequestDispatcher("message.jsp").forward(
                    request, response);
            }
        }
    }
    else
    {%>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="tool.accessError"/>
			<jsp:param name="render" value="true"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
    <%}
%>

<html>
<head>
<title>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="tool.title"/>
	<jsp:param name="render" value="true"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
</title>
</head>
<frameset rows="7%,*" border="1" frameborder="1">
	<frame src='<%=contextPath%>/cachetool/title.jsp'
		name="newtitle" scrolling="no">
	<frameset cols="10%,*" border="1" frameborder="1">
		<frame
			src='<%=contextPath%>/cachetool/sidebar.jsp?cachename=<%=cacheName%>&onCS=<%=onCS%>'
			name="sidebar">
		<frame
			src='<%=contextPath%>/cachetool/summary.jsp?cachename=<%=cacheName%>'
			name="content">
	</frameset>
</frameset>
</html>

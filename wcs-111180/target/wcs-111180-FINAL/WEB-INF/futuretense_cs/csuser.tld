<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>csuser</shortname>
	<info>A tag library for CatC csuser object methods.</info>
 	<tag>
		<name>argument</name>
		<tagclass>com.openmarket.framework.jsp.Argument</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Child tag which defines an arbitrary name/value pair available to the parent csuser tag.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	<tag>
		<name>authenticate</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Authenticate</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Looks up and retrieves a user from the commerce connector.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
  	</tag>
    <tag>
		<name>clear</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Clear</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Clears the parameters and data in a csuser object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   	</tag>
    <tag>
		<name>create</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Create</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Creates an empty csuser object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   	</tag>
    <tag>
		<name>createpmtacct</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Createpmtacct</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Creates a new payment account with Commere connector for the csuser object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
 		<attribute>
			<name>list</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
  	</tag>
     <tag>
		<name>deletepmtacct</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Deletepmtacct</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Deletes a payment account on Transact for the user.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
 		<attribute>
			<name>id</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
  	</tag>
    <tag>
		<name>fromstring</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Fromstring</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Creates a csuser object from a text string previously written 
            by csuser:tostring.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   	</tag>
     <tag>
		<name>getallpmtaccts</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Getallpmtaccts</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Obtains the current payment parameter selections for the csuser object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>listvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   	</tag>
    <tag>
		<name>geterrors</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Geterrors</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Obtains the errors reported the last time any csuser tags operated upon the specified csuser object.
   		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>listvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   	</tag>
    <tag>
		<name>getlegalpmtbrands</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Getlegalpmtbrands</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Obtains the current parameter selections for the csuser object.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>listvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   	</tag>
    <tag>
		<name>getparameter</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Getparameter</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Obtains the current value for a specified generic user parameter.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
           	<attribute>
			<name>field</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   </tag>
      <tag>
		<name>getparameters</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Getparameters</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Obtains the current parameter selections for the user.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>listvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
    </tag>
    <tag>
		<name>lookup</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Lookup</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Looks up and retrieves a buyer from the commerce connector.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
     </tag>
     <tag>
		<name>modify</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Modify</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
		    Modifies Buyer data on Transact.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
    </tag>
     <tag>
		<name>modifypmtacct</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Modifypmtacct</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Changes the payment account data on Transact for the specified buyer.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>list</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
           	<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   </tag>
   <tag>
		<name>register</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Register</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Registers a new buyer as a csuser object on Transact.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
        <attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   </tag>
    <tag>
		<name>setchallenges</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Setchallenges</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Set custom authenitcation challenges for the user. The list has two columns: 'name' and 'value'. The 'name' column contains a question, and the 'value' column contains the answer to the question.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
        <attribute>
			<name>list</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   </tag>
   <tag>
		<name>setparameter</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Setparameter</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Sets a single buyer parameter.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
        <attribute>
			<name>field</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
       <attribute>
			<name>value</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
    </tag>
    <tag>
		<name>setparameters</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Setparameters</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Sets the specified  buyer parameters.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
        <attribute>
			<name>list</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   </tag>
   <tag>
		<name>tostring</name>
		<tagclass>com.openmarket.gator.jsp.commerceuser.Tostring</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Writes the csuser object into a text string.
         </info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
        <attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
   </tag>
 </taglib>
 

 

/*=====
// WemContext provides methods for sharing attributes  between applications hosted in WemFramework.
// WemContext has methods for maintaining attributes and cookies.
// WemContext provides methods to access details like sitename.username,siteid populated by the framework.
// WemContext is a singleton object.The object is written in such a way that only one instance will be created per window.
// Syntax for using WemContext is as shown below 
// ***************************************************************************
//	var contextObject = WemContext.getInstance();
//  var user = contextObject.getUserName();
//  contextObject.setAttribute("mykey",myvalue) ; 
    
// ***************************************************************************	
//WemContext uses session level cookie for storing attributes,username,sitename and siteid.
// Session level cookie will be deleted when the browser is closed.
//The reason why cookie is used is objects cannot be shared across domains.

Updated: 

Replaced  client side browser storage (lawnchair) adapters used for retrieving / storing 
and updating User Attribute data preferences, with a UserPreference Data Manager  
that implements a server side both persistent and non persistent
session tracking mechanism solution.  
=====*/

var contextLocation = function() {

	// figure out the path of WemContext so that we can use it for importing
	// Cookie manager
	if (this["document"] && this["document"]["getElementsByTagName"]) {
		var scripts = document.getElementsByTagName("script");
		var contextFileLocation = /WemContext\.js(\W|$)/i;
		for ( var i = 0; i < scripts.length; i++) {
			var source = scripts[i].getAttribute("src");
			if (!source) {
				continue;
			}
			var filelocation = source.match(contextFileLocation);
			if (filelocation) {
				return source.substring(0, filelocation.index);
			}
		}
	}
};

addJavascript(contextLocation() + 'CookieManager.js', 'head');
// Load the Wem User Data Preferences Helper Manager

addJavascript(contextLocation() + 'UserPreferenceManager.js', 'head');

// load sofachair and its adaptors
addJavascript(contextLocation() + '../../js/json2.js', 'head');

// _context singleton variable; can be overwritten; if needed
var _context = null;
WemContext = {

};

WemContext.getInstance = function() {
	// returns WEMContext; creates one if it is not created
	if (!_context) {
		_context = new Context();
	}
	return _context;
	// Object
};

/**
 * This is not public method.This will be used just by the framework
 * 
 * @param contextPath
 *            The context root of CS
 */
WemContext.initialize = function(contextPath, signouturl) {
	
	var context = WemContext.getInstance();

	// Create a User Preference Utility Helper to fetch
	// and manage the set of user's data attributes
	// Note: Wemcontext Object uses UserPreferenceManager as a singleton object.
	context.sofa = new UserPreferenceManager({name : 'userpreferences', csPath : contextPath }, function() {});
	context.contextPath = contextPath; // store for future use (directToError)
	context.signouturl = signouturl;
	var contextUrl = contextPath + 'wem/fatwire/wem/ui/WemContext';
	try {
		var xhr;
		if (window.XMLHttpRequest) {
			/* Firefox, Safari, Opera... */
			xhr = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			/* Internet Explorer 6 */
			xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xhr.open("GET", contextUrl, false);
		xhr.send(null);
		if (xhr.readyState == 4 && xhr.status == 200) {
			var responseText = eval("(" + xhr.responseText + ")");
			context.setUserName(responseText.username);
			context.setUser(responseText.user);
			context.setLocale(responseText.locale);
		} else {
			alert("could not intialize wem context");
		}
	} catch (err) {
		alert("could not intialize wem context");
	}
};

// Internal implementation of WemContext is Context object
// Context uses CookieManager object to interact with Cookie
// Contstructor
function Context() {
	this.contextPath = "";
	this.signouturl = "";
	this.cookiemanager = new CookieManager();
	this.usernameAttribute = "fw_wemcontext_username";
	this.userAttribute = "fw_wemcontext_user";
	this.sitenameAttribute = "WEMUI:lastSite";
	this.siteidAttribute = "WEMUI:lastsiteid";
	this.localeAttribute = "fw_wemcontext_locale";
	this.authTicketAttribute = "_authticket_";
	this.attributekeydelimiter = "__key__";
	this.filelocation = contextLocation();
}

// Methods for Context object
Context.prototype = {

	// This method puts the key, value in the User Preference Manager storage,
	// if the persist flag is true
	// it marks it for persistence else it will be removed upon logout.
	putUserData : function(key, value, persist) {
		
		// If persist storage and track on server side 
		if (persist) {
			// Via UserPreference Manager Must Persist
			this.sofa.addUserPreference(this.getSiteName(), key, value);
		}
		// Save in local memory storage
		this.sofa.save({
			'key' : key,
			'value' : value,
			'persist' : persist
		});
	},

	// returns the value of given key, columnname from browser storage
	// if the column name is not given the value for the key will be returned
	getUserData : function(key, columnname) {
		return this.sofa.get(key);
	},

	// This method removes the data from server side 
	// storage for given user's logged in site and specific key 
	removeUserData : function(key) {
		this.sofa.removeUserPreference(this.getSiteName(),key)
	},

	// returns context path (as it was passed into WemContext.initialize)
	getContextPath : function() {
		return this.contextPath;
	},
	
	getCookiePrefix: function ( attribute ) {		
        var userinfo = this.getUser(),
        name=""; 
		if ( userinfo != null ) 
		 name = 'WEMUI:'+userinfo.substring(userinfo.indexOf('=') + 1,userinfo.indexOf(','))+':'+attribute;		
		return name ; 	
	},
	
	// set user name.Add username to browser storage.
	// @param username
	setUserName : function(username) {
		this.putUserData(this.usernameAttribute, username);
	},
	// returns user name.Retrieves username from browser storage.
	getUserName : function() {
		return this.getUserData(this.usernameAttribute);
	},
	// set user.Add user to browser storage.
	// @param username
	setUser : function(user) {
		this.putUserData(this.userAttribute, user);
	},
	// returns user.Retrieves username from browser storage.
	getUser : function() {
		return this.getUserData(this.userAttribute);
	},
	
	// set site name.Add sitename to browser storage.
	// @param sitename
	setSiteName : function(sitename) {
		// Put and persist in User's Preference Data Manager 
		// Add and persist
		this.sofa.addUserPreference(null,this.sitenameAttribute,sitename);
		this.sofa.getAllUserPreferences(sitename);
	},
	
	// returns site name.Retrieves sitename from browser storage.
	getSiteName : function() {		
		// Get from session server tracking via User Preference Manager 
		return this.getUserData(this.sitenameAttribute);
		
		//return this.getUserPreference(this.sitenameAttribute);
	},
	
	// set site id.Add site id to browser storage.
	// @param sitename
	setSiteId : function(siteid) {
		this.putUserData(this.siteidAttribute, siteid);
	},
	// returns site id.Retrieves siteid from browser storage.
	getSiteId : function() {
		return this.getUserData(this.siteidAttribute);
	},
	// set locale .Add locale to browser storage.
	// @param sitename
	setLocale : function(locale) {
		this.putUserData(this.localeAttribute, locale);
	},
	// returns locale .Retrieves locale from browser storage.
	getLocale : function() {
		return this.getUserData(this.localeAttribute);
	},

	getSignoutURL : function() {
		return this.signouturl;
	},
	// sets attribute id.Add attribute name and value to browser storage.
	// @param attributename
	// @param attributevalue
	// @param isPersist - whether or not to persists in browser storage
	setAttribute : function(attributename, attributevalue, isPersist) {
		if (isPersist) {
			this.putUserData(attributename, attributevalue, true);
		} else {
			this.putUserData(attributename, attributevalue);
		}
	},

	// returns attribute value.Retrieves attribute value from browser storage.
	// @param attributename
	getAttribute : function(attributename) {
		return this.getUserData(attributename);
	},
	
	
	// fetches from server side UserPreferences Storage 
	// value for logged in WEM user for specific user/site/key 
	// @param name  = attribute name
	getUserPreference: function(name) {
		// Via UserPreference Manager Fetch my attribute key value 
		return this.sofa.getUserPreference(this.getSiteName(), name);		 
	},
	
	// returns attribute value.Retrieves attribute value from browser storage.
	// @param attributename
	removeAttribute : function(attributename) {
		this.removeUserData(attributename);
	},

	// sets cookie
	// @param name
	// @param value
	// @param expiredays - this is number of days for which the cookie is valid
	// @param properties will be in associative array format eg: {path:'/test'}
	setCookie : function(name, value, expiredays, properties) {
		// default path to context path
		if (!properties) {
			properties = {};
		}
		if (!properties.path) {
			properties.path = this.contextPath;
		}
		this.cookiemanager.setCookie(name, value, expiredays, properties);
	},
	// returns cookie
	// @param name
	getCookie : function(name) {
		return this.cookiemanager.getCookie(name);
	},
	// removes cookie
	// @param name
	removeCookie : function(name, properties) {
		// default path to context path
		if (!properties) {
			properties = {};
		}
		if (!properties.path) {
			properties.path = this.contextPath;
		}
		this.cookiemanager.removeCookie(name, properties);		
	},
	
	// returns all the cookies
	getCookies : function() {
		return this.cookiemanager.getCookies();
	},
	directToLoginPage : function(logout) {
		if (logout) {
			var url = this.getSignoutURL()
					+ "?url="
					+ encodeURIComponent(this.removeTicket(
							window.top.location.href, 'ticket'))
			window.top.location.href = url;
		} else {
			window.top.location.href = window.top.location.href;
		}
	},
	directToErrorPage : function(logout) {
		if (logout) {

			var iframe = document.createElement("iframe");
			var self = this;
			var loadfunc = function() {
				self.directToErrorPage();
			};
			iframe.style.visibility = "hidden";
			document.body.appendChild(iframe);
			if (iframe.attachEvent) {
				iframe.attachEvent("onload", loadfunc);
			} else {
				iframe.onload = loadfunc;
			}
			iframe.src = this.getSignoutURL();
		} else {
			var href = window.top.location.href;
			// TODO is the path correct for Xdomain
			var locale = this.getLocale();
			locale = (locale && locale.length > 0) ? locale : "en_US";
			window.top.location.href = href.split('/').slice(0, 3).join('/')
					+ this.contextPath + 'html/wem/' + locale + '/error.html';
		}
	},
	directToTimeOutPage : function(logout) {
		var href = window.top.location.href;
		var locale = this.getLocale();
		locale = (locale && locale.length > 0) ? locale : "en_US";
		window.top.location.href = href.split('/').slice(0, 3).join('/')
				+ this.contextPath + 'html/wem/' + locale + '/timeout.html';

	},

	isSessionValid : function() {
		var pingUrl = this.contextPath + 'wem/fatwire/wem/ui/Ping';
		try {
			var xhr;
			if (window.XMLHttpRequest) {
				/* Firefox, Safari, Opera... */
				xhr = new XMLHttpRequest();
			} else if (window.ActiveXObject) {
				/* Internet Explorer 6 */
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xhr.open("GET", pingUrl, false);
			xhr.send(null);
			if (xhr.readyState == 4 && xhr.status == 200) {
				// Redirect to CAS login page on session timeout also returns
				// status 200.But eval will fail.
				var responseText = eval("(" + xhr.responseText + ")");
				return true;
			} else {
				// Invalid session
				return false;
			}
		} catch (err) {
			// If the request returned 302 then eval will fail and throw error.
			// Invalid session
			return false;
		}
		return false;
	},

	removeTicket : function(url, parameter) {
		if (!parameter) {
			return url;
		}
		url = url.replace(new RegExp("\\b" + parameter + "=[^&]*(&|$)", "gi"),
				"");
		// remove any leftover crud
		url = url.replace(/&$/, "");
		return url;
	},

	// sets authticket
	// @param value
	setAuthTicket : function(value) {
		this.putUserData(this.authTicketAttribute, value);
	},

	// returns authticket value
	getAuthTicket : function() {
		return this.getUserData(this.authTicketAttribute);
	} ,
	
	debug : function(message) {
		if(window.console) {
			if(window.console.debug) {
				console.debug(message);
			}
		}
	}
}

/**
 * imports javascript files
 */
function addJavascript(jsname) {
	document.write('<script type="text/javascript" src="' + jsname
			+ '"></script>')
}
// import CookieManager.js

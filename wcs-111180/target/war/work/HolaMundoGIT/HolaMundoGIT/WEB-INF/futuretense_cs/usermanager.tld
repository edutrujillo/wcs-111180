<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>usermanager</shortname>
	<info>A tag library for Content Centre usermanager methods.</info>
	<tag>
		<name>getuser</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getuser</tagclass>
		<info>
			Loads or creates a user instance with the specified user id.
		</info>
		<attribute>
			<name>user</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getuserfromname</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getuserfromname</tagclass>
		<info>
			Loads or creates a user instance based on user name.
		</info>
		<attribute>
			<name>username</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getusername</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getusername</tagclass>
		<info>
			Obtains the username for a user, given the user's id.
		</info>
		<attribute>
			<name>user</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdisplayableusername</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getdisplayableusername</tagclass>
		<info>
			Obtains the displayable name for a user, given the user's id.
		</info>
		<attribute>
			<name>user</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setuser</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Setuser</tagclass>
		<info>
			Saves a user's information.
		</info>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getusers</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getusers</tagclass>
		<info>
			Gets users with a specifed role.
		</info>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>role</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>site</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getmultisiteusers</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getmultisiteusers</tagclass>
		<info>
			Gets a list of users across sites (or all sites).
		</info>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>role</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>sites</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getuserids</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getuserids</tagclass>
		<info>
			Gets a list of user ids.
		</info>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>list</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>deleteuser</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Deleteuser</tagclass>
		<info>
			Deletes a user.
		</info>
		<attribute>
			<name>user</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>deletesite</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Deletesite</tagclass>
		<info>
			Deletes a site.
		</info>
		<attribute>
			<name>site</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>loginuser</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Loginuser</tagclass>
		<info>
			Logs in a user.
		</info>
		<attribute>
			<name>username</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>password</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>

	</tag>

	<tag>
		<name>getloginuser</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getloginuser</tagclass>
		<info>
			Obtains the logged-in user id.
		</info>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getloginusername</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getloginusername</tagclass>
		<info>
			Obtains the logged-in user name.
		</info>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getlogindisplayableusername</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Getlogindisplayableusername</tagclass>
		<info>
			Obtains the logged-in user displayable name.
		</info>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>logout</name>
		<tagclass>com.openmarket.xcelerate.jsp.usermanager.Logout</tagclass>
		<info>
			Logs out a user.
		</info>
	</tag>

</taglib>

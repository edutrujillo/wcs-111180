<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>workflowstate</shortname>
	<info>A tag library for Content Centre workflow state methods.</info>
	<tag>
		<name>scatter</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Scatter</tagclass>
		<info>
			Retrieves all fields of a loaded workflow state object and "scatters" their values into individual variables sharing a common prefix.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gather</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Gather</tagclass>
		<info>
			Retrieves name/value pairs from the environment, and sets them into a loaded workflow state object. The name/value pairs are grouped together using a prefix and a prefix separator.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Getid</tagclass>
		<info>
			Gets the id of the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getstatename</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Getstatename</tagclass>
		<info>
			Gets the state name of the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setstatename</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Setstatename</tagclass>
		<info>
			Sets the state name of the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getstatedescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Getstatedescription</tagclass>
		<info>
			Gets the description of the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setstatedescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Setstatedescription</tagclass>
		<info>
			Sets the description of the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getestimatedstatetime</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Getestimatedstatetime</tagclass>
		<info>
			Gets the estimated time of the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setestimatedstatetime</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Setestimatedstatetime</tagclass>
		<info>
			Sets the estimated time of the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>time</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Getactions</tagclass>
		<info>
			Gets the actions in the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstate.Setactions</tagclass>
		<info>
			Sets the actions in the workflow state.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

</taglib>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>currency</shortname>
	<info>A tag library for CatC currency object methods.</info>
 	<tag>
		<name>argument</name>
		<tagclass>com.openmarket.framework.jsp.Argument</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Child tag which defines an arbitrary name/value pair available to the parent currency tag.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	<tag>
		<name>create</name>
		<tagclass>com.openmarket.basic.jsp.currency.Create</tagclass>
		<bodycontent>empty</bodycontent>
		<info>
			Creates a currency object for a specific currency ISO code.
		</info>
		<attribute>
			<name>isocurrency</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>separator</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
    <tag>
        <name>getsymbol</name>
        <tagclass>com.openmarket.basic.jsp.currency.GetSymbol</tagclass>
        <bodycontent>empty</bodycontent>
        <info>
            Returns the symbol used to represent currencies of this type.
        </info>
        <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>varname</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>   
    <tag>
        <name>getcurrency</name>
        <tagclass>com.openmarket.basic.jsp.currency.GetCurrency</tagclass>
        <bodycontent>empty</bodycontent>
        <info>
            Converts a double to a displayable currency string.
        </info>
        <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
         <attribute>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>varname</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>   
     <tag>
        <name>readcurrency</name>
        <tagclass>com.openmarket.basic.jsp.currency.ReadCurrency</tagclass>
        <bodycontent>empty</bodycontent>
        <info>
            Converts a number previously rendered as a monetary string back to a decimal value.
        </info>
        <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
         <attribute>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>varname</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag> 
    <tag>
        <name>roundup</name>
        <tagclass>com.openmarket.basic.jsp.currency.RoundUp</tagclass>
        <bodycontent>empty</bodycontent>
        <info>
            Rounds a currency value upward to the next higher legal absolute currency value.   
        </info>
        <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
         <attribute>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>varname</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>   
    <tag>
        <name>rounddown</name>
        <tagclass>com.openmarket.basic.jsp.currency.RoundDown</tagclass>
        <bodycontent>empty</bodycontent>
        <info>
            Rounds a currency value downward to the next lower legal absolute currency value.   
        </info>
        <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
         <attribute>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>varname</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>   

    <tag>
        <name>round</name>
        <tagclass>com.openmarket.basic.jsp.currency.Round</tagclass>
        <bodycontent>empty</bodycontent>
        <info>
            Rounds a currency value to the nearest legal absolute currency value.
        </info>
        <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
         <attribute>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>varname</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>   
     <tag>
        <name>getisocode</name>
        <tagclass>com.openmarket.basic.jsp.currency.GetIsocode</tagclass>
        <bodycontent>empty</bodycontent>
        <info>
            Returns the ISO code for the currency instance.
        </info>
        <attribute>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>varname</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>   
</taglib>

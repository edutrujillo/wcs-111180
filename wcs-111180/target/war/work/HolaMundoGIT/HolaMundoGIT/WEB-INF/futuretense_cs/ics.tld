<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
  <tlibversion>1.0</tlibversion>
  <jspversion>1.1</jspversion>
  <shortname>ics</shortname>
  <info>A tag library for ContentServer ICS methods.</info>

  <tag>
    <name>debug</name>
    <tagclass>com.openmarket.ICS.debug</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Sets debug info for tags executed in this thread.
    </info>
    <attribute>
      <name>on</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  
  <tag>
    <name>cdata</name>
    <tagclass>com.fatwire.ics.jsp.CData</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Includes a CDATA in the ouput of a rendered XML element.
    </info>
  </tag>

  <tag>
    <name>getcookie</name>
    <tagclass>com.fatwire.ics.jsp.GetCookie</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Retrieves a cookie from the client's browser environment (potentially one set using satellite:cookie).
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>geterrno</name>
    <tagclass>com.fatwire.ics.jsp.GetErrno</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Gets the current error code as an integer.
    </info>
  </tag>

  <tag>
    <name>getvar</name>
    <tagclass>com.fatwire.ics.jsp.GetVar</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Returns the value of a Content Server variable. 
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>encoding</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>getssvar</name>
    <tagclass>com.openmarket.ICS.csSSvar</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Returns the value of a Content Sever session variable.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>resolvevariables</name>
    <tagclass>com.fatwire.ics.jsp.ResolveVariables</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Resolves multiple Content Server variables within a string.
    </info>
    <attribute>
      <name>delimited</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>hasdata</name>
    <tagclass>com.fatwire.ics.jsp.HasData</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Checks for a valid list with data.
    </info>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  
  <tag>
    <name>listget</name>
    <tagclass>com.fatwire.ics.jsp.ListGet</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Retrieves the value of the specified field in a specified list.
    </info>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>fieldname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  
  <tag>
    <name>seterrno</name>
    <tagclass>com.fatwire.ics.jsp.SetErrno</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Sets the current error code.
    </info>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>setvar</name>
    <tagclass>com.fatwire.ics.jsp.SetVar</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Sets the value of a Content Server variable.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  
  <tag>
    <name>removevar</name>
    <tagclass>com.openmarket.ICS.removevar</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Deletes a Content Server variable.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  
  <tag>
    <name>setssvar</name>
    <tagclass>com.openmarket.ICS.setSSvar</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Sets the value of a Content Server session variable.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  
  <tag>
    <name>removessvar</name>
    <tagclass>com.openmarket.ICS.removeSSvar</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Deletes a Content Server session variable.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
    
  <tag>
    <name>argument</name>
    <tagclass>com.openmarket.framework.jsp.Argument</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Child tag which defines an arbitrary name/value pair available to the parent tag.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>callelement</name>
    <tagclass>com.divine.ics.jsp.CallElement</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Processes the content of an element.
    </info>
    <attribute>
      <name>element</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
      
  <tag>
    <name>catalogmanager</name>
    <tagclass>com.divine.ics.jsp.CatalogManager</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Creates, modifies, or deletes tables and rows with argument tags.
    </info>
  </tag>
  
  <tag>
    <name>treemanager</name>
    <tagclass>com.divine.ics.jsp.TreeManager</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Creates, modifies, or deletes tree nodes with argument tags.
    </info>
  </tag>
  
  
  <tag>
    <name>sendmail</name>
    <tagclass>com.openmarket.ICS.sendmail</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Sends an SMTP mail message to one or more recipients. (Deprecated: use mail:send instead.)
    </info>
    <attribute>
      <name>to</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
	    <name>subject</name>
    	<required>true</required>
	    <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
	    <name>body</name>
	    <required>false</required>
	    <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
	    <name>replyto</name>
	    <required>false</required>
	    <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
	    <name>contenttype</name>
	    <required>false</required>
	    <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
	    <name>charset</name>
	    <required>false</required>
	    <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  <tag>
    <name>attachment</name>
    <tagclass>com.openmarket.ICS.attachment</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Includes an attachment (for use within the ics:sendmail tag).
    </info>
    <attribute>
      <name>name</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
	<name>value</name>
    	<required>true</required>
	<rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>listloop</name>
    <tagclass>com.openmarket.ICS.listloop</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Iterates over a list.
    </info>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>maxrows</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
        <name>startrow</name>
        <required>false</required>
        <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
        <name>endrow</name>
        <required>false</required>
        <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
        <name>wrapto</name>
        <required>false</required>
        <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>if</name>
    <tagclass>com.openmarket.ICS.iftag</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Processes other tags or displays text based on a logical condition. If the condition evaluates to true, the content of the ics:then tag is processed. If the condition evaluates to false, the content of the ics:else tag is processed.
    </info>
    <attribute>
      <name>condition</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>then</name>
    <tagclass>com.openmarket.ICS.thentag</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Creates a conditional then clause.
    </info>
  </tag>

  <tag>
    <name>else</name>
    <tagclass>com.openmarket.ICS.elsetag</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Creates a conditional else clause.
    </info>
  </tag>

  <tag>
    <name>selectto</name>
    <tagclass>com.fatwire.ics.jsp.SelectTo</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Executes a simple query from a table. The results are stored in a list.
    </info>
   <attribute>
      <name>table</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>what</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>where</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>orderby</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>limit</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>catalogdef</name>
    <tagclass>com.openmarket.ICS.catalogdef</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Queries a table for its column definition and stores the results in a list. 
    </info>
   <attribute>
      <name>table</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>sql</name>
    <tagclass>com.openmarket.ICS.execsql</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Executes an inline SQL statement.
    </info>
   <attribute>
      <name>sql</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>limit</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>table</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>callsql</name>
    <tagclass>com.openmarket.ICS.callsql</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Retrieves and executes a SQL query stored in the SystemSQL table.
    </info>
   <attribute>
      <name>qryname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>limit</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>clearerrno</name>
    <tagclass>com.fatwire.ics.jsp.ClearErrno</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Clears the current error state.
    </info>
	</tag>

  <tag>
    <name>copylist</name>
    <tagclass>com.openmarket.ICS.copylist</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Creates a copy of the specified list. 
    </info>
    <attribute>
      <name>from</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>to</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>disablecache</name>
    <tagclass>com.fatwire.ics.jsp.DisableCache</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Disables caching.
    </info>
    <attribute>
      <name>recursive</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>flushcatalog</name>
    <tagclass>com.openmarket.ICS.flushcatalog</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Flushes the internal memory cache for a table. Use this command to clear a table's cache after a table update using a non-Content Server mechanism.
    </info>
    <attribute>
      <name>catalog</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>flushstream</name>
    <tagclass>com.openmarket.ICS.flushstream</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Flushes a data stream.
    </info>
  </tag>

  <tag>
    <name>getproperty</name>
    <tagclass>com.fatwire.ics.jsp.GetProperty</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Gets a property value from the currently loaded Content Server property files.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>file</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>pushvars</name>
    <tagclass>com.openmarket.ICS.pushvars</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Saves a copy of all the current variables to a stack. The current variable state is unchanged.
    </info>
  </tag>
  <tag>
    <name>popvars</name>
    <tagclass>com.openmarket.ICS.popvars</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Pops the variable stack, returning the variables to their previously stacked state. If the stack is empty, ics:popvars does nothing.
    </info>
  </tag>

  <tag>
    <name>insertpage</name>
    <tagclass>com.divine.ics.jsp.InsertPage</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Inserts the results of a Content Server page evaluation into the current output stream.
    </info>
    <attribute>
      <name>pagename</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>logmsg</name>
    <tagclass>com.fatwire.ics.jsp.LogMsg</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Writes a string to a Content Server log file for debugging or system monitoring.
    </info>
    <attribute>
      <name>msg</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>name</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>severity</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>search</name>
    <tagclass>com.openmarket.ICS.search</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Searches an index based on a query.
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>searchterm</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>relevanceterm</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>queryparser</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>maxresults</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>indexexists</name>
    <tagclass>com.openmarket.ICS.indexexists</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Checks for the existence of an index.
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>dateargument</name>
    <tagclass>com.openmarket.ICS.dateargument</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Child argument tag for the ics:indexadd tag which specifies a date to be added to the search index.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>textargument</name>
    <tagclass>com.openmarket.ICS.textargument</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Child argument tag for the ics:indexadd tag which specifies a text field to be added to the search index.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>fileargument</name>
    <tagclass>com.openmarket.ICS.fileargument</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Child argument tag for the ics:indexadd tag which specifies a file to be added to the search index.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>indexadd</name>
    <tagclass>com.openmarket.ICS.indexadd</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Adds an entry to an existing search index.
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>entryname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>entrydetail</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>wordlist</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>wordlistdelimiters</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>filelist</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>date</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
 </tag>

  <tag>
    <name>indexreplace</name>
    <tagclass>com.openmarket.ICS.indexreplace</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Replaces an existing entry in a search index.
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>entryname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>entrydetail</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>wordlist</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>wordlistdelimiters</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>filelist</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>date</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
 </tag>

  <tag>
    <name>fieldargument</name>
    <tagclass>com.openmarket.ICS.fieldargument</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Child argument tag for ics:indexcreate tag to specify search index field definition for Content Server systems using the Verity search engine.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>value</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>indexcreate</name>
    <tagclass>com.openmarket.ICS.indexcreate</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Creates a search index.
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>indexremove</name>
    <tagclass>com.openmarket.ICS.indexremove</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Removes an entry from an existing search index.
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>entryname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>indexdestroy</name>
    <tagclass>com.openmarket.ICS.indexdestroy</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Deletes an existing search index. 
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>searchdatetonative</name>
    <tagclass>com.openmarket.ICS.searchdatetonative</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Converts the Java SQL-formatted date to the date format native to the search engine.
    </info>
    <attribute>
      <name>searchengine</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>charset</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>index</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>date</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>outputvariable</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>mirrorlist</name>
    <tagclass>com.openmarket.ICS.mirrorlist</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Child argument tag for the ics:mirror tag, which specifies a list to be mirrored.
    </info>
    <attribute>
      <name>listname</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>mirrortreenode</name>
    <tagclass>com.openmarket.ICS.mirrortreenode</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Child argument tag for the ics:mirror tag, which specifies a subtree to be mirrored.
    </info>
    <attribute>
      <name>tree</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>node</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>mirror</name>
    <tagclass>com.openmarket.ICS.mirror</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Mirrors one or more mirrorlists.
    </info>
    <attribute>
      <name>target</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>username</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>password</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>cgiprefix</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>targetinifile</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>port</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>useproxy</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
     <attribute>
      <name>secure</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>httpversion</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>outputvariable</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
 </tag>

  <tag>
    <name>catalogbatch</name>
    <tagclass>com.openmarket.ICS.batchtag</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Allows ics:catalogmanager calls to be used in batches. 
    </info>
  </tag>

  <tag>
    <name>getnamespace</name>
    <tagclass>com.fatwire.ics.jsp.GetNamespace</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Stream or set as a variable, the namespace prefix.  Usually required 
        for portlet development, but no-op for servlets.
    </info>
    <attribute>
      <name>output</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>encode</name>
    <tagclass>com.fatwire.ics.jsp.Encode</tagclass>
    <bodycontent>JSP</bodycontent>
    <info>
        Builds a URL string from a base and key/value pairs using the ics:argument child tag.
    </info>
    <attribute>
      <name>base</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>session</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>readevent</name>
    <tagclass>com.fatwire.ics.jsp.ReadEvent</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Reads an event record.
    </info>
    <attribute>
      <name>name</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>list</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>

  <tag>
    <name>queryevents</name>
    <tagclass>com.fatwire.ics.jsp.QueryEvents</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Performs a query for a list of registered events.
    </info>
    <attribute>
      <name>list</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>name</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>type</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>enabled</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  <tag>
    <name>literal</name>
    <tagclass>com.fatwire.ics.jsp.Literal</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Formats literals for ics:callsql and other tasks beyond those for which ics:sqlexp is designed.
    </info>
    <attribute>
      <name>column</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>string</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>table</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  <tag>
    <name>sqlexp</name>
    <tagclass>com.fatwire.ics.jsp.SqlExp</tagclass>
    <bodycontent>empty</bodycontent>
    <info>
        Builds a where clause based on the given set of parameters.
    </info>
    <attribute>
      <name>column</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>expression</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>output</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>string</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>table</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>type</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>verb</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  <tag>
    <name>ifempty</name>
    <tagclass>com.fatwire.ics.jsp.IfEmpty</tagclass>
    <tei-class>com.fatwire.ics.jsp.IfEmptyTEI</tei-class>
    <bodycontent>JSP</bodycontent>
    <attribute>
      <name>variable</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>list</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
  <tag>
    <name>ifnotempty</name>
    <tagclass>com.fatwire.ics.jsp.IfNotEmpty</tagclass>
    <tei-class>com.fatwire.ics.jsp.IfEmptyTEI</tei-class>
    <bodycontent>JSP</bodycontent>
    <attribute>
      <name>variable</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
    <attribute>
      <name>list</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
    </attribute>
  </tag>
</taglib>

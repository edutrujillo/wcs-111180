<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>ccuser</shortname>
	<info>A tag library for Content Centre CCuser methods.</info>
	<tag>
		<name>getid</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Getid</tagclass>
		<info>
			Gets the id of the user object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getname</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Getname</tagclass>
		<info>
			Gets the username from the user object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setname</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Setname</tagclass>
		<info>
			Sets the username.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdisplayablename</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Getdisplayablename</tagclass>
		<info>
			Gets displayable name.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdisplayablename</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Setdisplayablename</tagclass>
		<info>
			Sets displayable name.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getemail</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Getemail</tagclass>
		<info>
			Gets the email of the user.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setemail</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Setemail</tagclass>
		<info>
			Sets the email of the user.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getlocale</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Getlocale</tagclass>
		<info>
			Gets the locale of the user.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setlocale</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Setlocale</tagclass>
		<info>
			Sets the locale of the user.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gettimezone</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Gettimezone</tagclass>
		<info>
			Gets the timezone of the user.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>settimezone</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Settimezone</tagclass>
		<info>
			Sets the timezone of the user.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>getsiteroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Getsiteroles</tagclass>
		<info>
			Gets the site roles of the user, given a site id.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>site</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setsiteroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Setsiteroles</tagclass>
		<info>
			Sets the site roles of the user, given the site id.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>site</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>getsites</name>
		<tagclass>com.openmarket.xcelerate.jsp.ccuser.Getsites</tagclass>
		<info>
			Gets all sites for which the specified user is enabled.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
</taglib>

<script language="javascript" type="text/javascript">

var checkedCountKey = document.getElementsByName('key').length;
var checkedCountDep = document.getElementsByName('dep').length;

function checkAll(searchBy)
{
	 var x = document.getElementsByName(searchBy);
	 var element = null;
	 if(searchBy === 'key')
	 {
	
	 element = document.getElementsByName('selectAll');
	if(element[0].checked) 
	 checkedCountKey = x.length;
	else
		checkedCountKey = 0;
	 }
	 else
	 {
	 element = document.getElementsByName('selectAll_1');
	 checkedCountDep = x.length;
	 
		if(element[0].checked) 
			checkedCountDep = x.length;
		else
			checkedCountDep = 0;
	 }
	 
	 var xLength = x.length;
	 for( var i=0;i<xLength;i++)
	 {
	  var elem = x[i];
	 if(!elem.disabled) 
	  	elem.checked =  element[0].checked
	 }
}

function toggleParentOnCheck(searchBy,thisObject)
{
	var toggleBox = null;
	var element = null;
	
	if(searchBy === 'key')
	 {
		 if(thisObject.checked){
		 	checkedCountKey++;
		 }
		 else {
		 checkedCountKey--;
		 }
		 element = document.getElementsByName('key');
		 toggleBox = document.getElementsByName('selectAll');
		 if(checkedCountKey < element.length){
			 toggleBox[0].checked = false;
		 } else {
			 toggleBox[0].checked = true;
		 }
	 }
	 else
	 {
		 if(thisObject.checked) {
			 checkedCountDep++;
		 }else {
			 checkedCountDep--;
		 }
		 
		 element = document.getElementsByName('dep');
		 toggleBox = document.getElementsByName('selectAll_1');
		 
		 if(checkedCountDep < element.length){
			 toggleBox[0].checked = false;
		 } else {
			 toggleBox[0].checked = true;
		 }
		 
	 }
}

var Paging = function(parameters){

	var pagePerView = 7;
	var rowsPerPage = 5;
	var currentPage=1; 
	var recordCount = 0;
	var gotoPage= 'gotoPage';
	var renderTop = null;
	var renderBottom = null; 
	
	//constructor function
	var _Paging = function(parameters){
		
		if (parameters.pagePerView !=null){
			pagePerView = parameters.pagePerView;
		}
		
		if (parameters.rowsPerPage !=null){
			rowsPerPage = parameters.rowsPerPage;
		}
		
		if (parameters.renderTop != null){
			renderTop = parameters.renderTop;
		}
		
		if (parameters.renderBottom != null){
			renderBottom = parameters.renderBottom;
		}
		
		if (parameters.recordCount > 0){
			recordCount = parameters.recordCount; 
		}
		
		if (parameters.currentPage){
			currentPage = parameters.currentPage; 
		}
		
		if (parameters.onSuccess){
			gotoPage = parameters.onSuccess; 
		}

	}
	
	this.getCurrentPage  = function(){ return currentPage; }
	this.setCurrentPage = function(currentPage){ currentPage = currentPage };
	
	this.getRecordCount  = function(){ return recordCount; }
	this.setRecordCount = function(recordCount){ recordCount = recordCount };

	this.getRowsPerPage  = function(){ return rowsPerPage; }
	this.setRowsPerPage = function(rowsPerPage){ rowsPerPage = rowsPerPage };
	
	this.getGotoPage = function(){ return gotoPage; }
	this.setGotoPage = function(goto_page){ gotoPage = goto_page };
	
	this.getRowStart = function(){ 
		return (currentPage * rowsPerPage) - rowsPerPage + 1;
	}
	
	this.getRowEnd = function(){
		return currentPage * rowsPerPage;
	}
	
	this.getNextPage = function(){ 
		return currentPage  + 1;
	}
	
	this.getPreviousPage = function(){ 
		return currentPage  - 1;
	}

	this.getTotalPages = function(){ 
		
		var result = recordCount / rowsPerPage; 
		result = Math.ceil(result); 
		return result;
	}
	
	this.getCountStart = function(){ //page count start eg 12 in [<< <Page 12, 13, 14...> >>]
				
		var startPage = currentPage;
		startPage = parseFloat(startPage); 
		var lastPage = this.getTotalPages();
				
		//center current page in display
		var leftPageNumFromCenter = Math.floor(pagePerView / 2);
		for (var i=0; i<leftPageNumFromCenter && startPage > 1; i++)
		{
			startPage--;
		}
		
		if (lastPage - currentPage < leftPageNumFromCenter)
		{
			for(var i=0; i< (leftPageNumFromCenter - (lastPage - currentPage)) && startPage > 1; i++)
			{
				startPage--;
			}
		}
		
		return startPage ;
	}
	
	this.getCountEnd = function(){ 
		var totalPages = this.getTotalPages();
		var count_end = this.getCountStart();
		for (var i=0; i< pagePerView && count_end <= totalPages; i++)
		{
			count_end++;
		}

		return count_end;
	}
	//Output: first 1 2 3 4 5 6 7 last			
	this.getPagingStyle2 = function(){
		
		var goto_page = this.getGotoPage();
		var currentPage = this.getCurrentPage();
				
		var previous_page = Number(this.getCurrentPage()) - 1;
		var next_page = Number(this.getCurrentPage()) + 1 ; 
		
		var first_page = 1;
		var last_page = this.getTotalPages(); 

		var count_start = this.getCountStart();
		var count_end = this.getCountEnd();
		
		var paging ='';
		var link_page='';
		var link_first_page='';
		var link_prev_page='';
		var link_last_page ='';
		var link_next_page=''
		
		var link_param_others = '"' + this.getRecordCount()  + '","' + goto_page + '","' + 
								renderTop + '","' + renderBottom + '"';
		
		var params = 'renderTop:"' + renderTop + '",' +
								'renderBottom:"' + renderBottom + '",' +
								'goto_page:"' + goto_page + '",' +
								'recordCount:"' + this.getRecordCount()  + '"' ;
	
		for (var i=count_start; i<count_end;i++){
			
			if (i ==currentPage){
				paging  = paging + ' <b>' + i + '</b>';
			}else{
				link_page = 'javascript:submitSearchForm("paging","' + i + '")';
				paging  = paging + " <a href='" + link_page + "'>" + i + "</a>";
			}
		}
		
		if (last_page == 1){
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="paging.of.page"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include>
			pagingstr = "<%=request.getAttribute("paging.of.page")%>";
			paging = pagingstr.replace(/(\{0\})/, paging).replace(/(\{1\})/, last_page);
		}else{
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="paging.of.pages"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include>
			pagingstr = "<%=request.getAttribute("paging.of.pages")%>";
			paging = pagingstr.replace(/(\{0\})/, paging).replace(/(\{1\})/, last_page);
		}
		if (currentPage>1){
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="paging.first"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include>
			link_page = 'javascript:submitSearchForm("paging","' + first_page + '")';		
			link_first_page = "<a href='" + link_page + "'><%=request.getAttribute("paging.first")%></a>";			
			//add first and last page
			paging = link_first_page + ' ' +  paging ;
					
		}
		
		if (currentPage !=last_page){
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="paging.last"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include>
			link_page = 'javascript:submitSearchForm("paging","' + last_page + '")';		
			link_last_page = "<a href='" + link_page + "'><%=request.getAttribute("paging.last")%></a>";
			paging = paging + ' ' + link_last_page;

		}
		
		//render the paging (at the top)
		if (renderTop !=null && document.getElementById(renderTop)){
			document.getElementById(renderTop).innerHTML = paging;
		}

		//render the paging (at the bottom)
		var bottom_page_exist = document.getElementById(renderBottom);
		if (bottom_page_exist){	
			//alert(renderBottom);
			document.getElementById(renderBottom).innerHTML = paging;

		}else{
			//alert(document.getElementById(renderBottom) );			
		}
	}
	
	//call the constructor
	_Paging(parameters);

	if (recordCount>0){
		this.getPagingStyle2();
	}else{

		if (renderTop && document.getElementById(renderTop)){
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="paging.noDataAvailable"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include>
			document.getElementById(renderTop).innerHTML = "<%=request.getAttribute("paging.noDataAvailable")%>";
		}
		
		//render the paging (at the bottom)
		if (renderBottom){
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="paging.noDataAvailable"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include>
			document.getElementById(renderBottom).innerHTML ="<%=request.getAttribute("paging.noDataAvailable")%>";
		}
	}
	
}
</script>
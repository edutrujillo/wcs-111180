<%@page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}

%>
<%@ include file="./paging.jsp"%>
<%   
String authorize = (String) session.getAttribute(
	"userAuthorized");
if (Boolean.valueOf(authorize)) {
    String contextPath= request.getContextPath();
    String cacheName = request.getParameter("cachename");
	cacheName = StringEscapeUtils.escapeHtml(cacheName);
    if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
        cacheName = CacheUtil.SS_CACHE;
%>
<html>
<head>
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css'
	rel="stylesheet" type="text/css">
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css'
	rel="stylesheet" type="text/css">
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css'
	rel="stylesheet" type="text/css">

<script language="javascript" type="text/javascript">
function submitSearchForm(action, selectPage)
{
	var url = "<%=contextPath%>/cachetool/page.jsp?action="+action+"&cachename=<%=cacheName%>";
	var frm = document.searchForm;
	
	//set flush keys if request comes from flush
	if (action === 'flush')
		frm.flushKeys.value = getFlushKeys();
	frm.selectPage.value = selectPage;
	 /*
	  * Matches the following date patterns
	  * dd/mm/yyyy
	  * dd/mm/yy
	  * d/m/yy
	  * dd/mm/yyyy
	  */
	//enhance late
	//validate date format
	var re = /^\d{1,2}\/\d{1,2}\/\d{2,4}( \d{2}:\d{2}:\d{2})?$/
	
	if (document.getElementById("searchBy").value === "Expired Time" && !re.test(frm.searchKey.value))
	{
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="page.dateFormat"/>
			<jsp:param name="escape" value="true"/>
		</jsp:include>
		
		alert ("<%=request.getAttribute("page.dateFormat")%>");
		return false;
	}
	var searchKeyObj = document.searchForm.searchKey;
	if(searchKeyObj.value.indexOf("<",0) == -1 
			&& searchKeyObj.value.indexOf(">",0) == -1)
		{
			document.searchForm.action = url;
			document.searchForm.submit();
		}
	else{ 
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="page.invalidSearch"/>
			<jsp:param name="escape" value="true"/>
		</jsp:include>
		alert('<%=request.getAttribute("page.invalidSearch")%>');
	}
}

function submitPageForm()
{
	var url = "<%=contextPath%>/cachetool/page.jsp?cachename=<%=cacheName%>&searchBy=Dependency&searchKey=";
	var chkboxNum = document.pageForm.dep.length;
	
	var values = {}, valueStr = '';
	if (chkboxNum != undefined)
	{
	
		for (var idx = 0; idx < chkboxNum; idx++) 
		{
			if (document.pageForm.dep[idx].checked == true)
				values[document.pageForm.dep[idx].value] = true;
		}
		
		for (var i in values) {
			if (values.hasOwnProperty(i)) {
				valueStr += i + ',';
			}
		}
		
		url += valueStr.substr(0, valueStr.length - 1);
	}
	else
	{
		if(document.pageForm.dep.checked == true)
			url = url + document.pageForm.dep.value;
	}
	document.pageForm.action = url;
	document.pageForm.submit();
}


function getFlushKeys() {

	var keys = "";
	var chkboxNum = document.pageForm.key.length;
	var count = 0;
	if (chkboxNum != undefined)
	{
	for (var idx = 0; idx < chkboxNum; idx++) 
	{
		if (document.pageForm.key[idx].checked == true)
		{
			count++;
			if (count == 1)
			{
				keys = keys + document.pageForm.key[idx].value;
				
			}
			else
				keys = keys + "@~#" + document.pageForm.key[idx].value;
		}
	}
	}
	else
	{
		if(document.pageForm.key.checked == true)
			keys = keys + document.pageForm.key.value;
	}
	return keys;
}

function onSearchTypeSelection()
{
	var selectElem = document.searchForm.searchBy;
	var searchForm = document.searchForm;
	if (selectElem.selectedIndex ==1 )
	{
		searchForm['matchTypeSelect'].style.visibility='visible';
		 <jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="page.optionText.before"/>
			<jsp:param name="escape" value="true"/>
		 </jsp:include>
		searchForm['matchTypeSelect'].options[0]=new Option( "<%=request.getAttribute("page.optionText.before")%>"
															, "BEFORE", true, false)
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="page.optionText.after"/>
			<jsp:param name="escape" value="true"/>
		  </jsp:include>
		searchForm['matchTypeSelect'].options[1]=new Option( "<%=request.getAttribute("page.optionText.after")%>"
															 , "AFTER", false, false)
		
	}
	else if (selectElem.selectedIndex==2)
	{
		searchForm['matchTypeSelect'].style.visibility='visible';
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCache.optionText.any"/>
			<jsp:param name="escape" value="true"/>
		  </jsp:include>
		searchForm['matchTypeSelect'].options[0]=new Option( "<%=request.getAttribute("assetCache.optionText.any")%>"
															  , "ANY", true, false)
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCache.optionText.all"/>
			<jsp:param name="escape" value="true"/>
		  </jsp:include>
		searchForm['matchTypeSelect'].options[1]=new Option(  "<%=request.getAttribute("assetCache.optionText.all")%>"
															  , "ALL", false, false)
	}
	else 
	{
		searchForm['matchTypeSelect'].style.visibility='hidden';
	}
	
}

function pagePopup(url) {
	newwindow=window.open(url,'page','height=400,width=600');
	if (window.focus) {newwindow.focus()}
	return false;
}

</script>
</head>
<body>
<jsp:include page="/cachetool/serverTimeZoneInfo.jsp"/>
<%
    //retrieve param values from request
				String searchType = request.getParameter("searchBy") == null
						? "Key"
						: request.getParameter("searchBy");
%>
<table>
	<tr>
		<td><jsp:include page="/cachetool/search.jsp" /></td>
	<tr>
		<td>
		<%
		    if ("Dependency".equals(searchType)) {
		%>
		<jsp:include page="/cachetool/revDepPageResult.jsp?action=search" />
		<%
     	} else {
 		%>
 		<jsp:include page="/cachetool/pageResult.jsp" />
 		<%
     	}
 		%>
		</td>
	</tr>
</table>
</body>
</html>
<% }else{
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
    }
%>

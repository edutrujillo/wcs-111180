<%@page import="net.sf.ehcache.Cache"%>
<%@page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@ page import="java.util.*"%>
<%@ page import="COM.FutureTense.Cache.*"%>
<%@ page import="com.fatwire.cache.*"%>

<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}
%>
<%@ include file="./paging.jsp"%>
<%
String authorize = (String) session.getAttribute("userAuthorized");
if (Boolean.valueOf(authorize)) 
{
    String action = request.getParameter("act");
    String[] parts = request.getParameterValues("key");
    String obj = request.getParameter("keys");
    
    if(obj != null)
    {
    	Set<String> settinList  = new TreeSet<String>(Arrays.asList(obj.split(",")));
    	request.setAttribute("keys",settinList);
    }
    
    if(action != null && parts!= null && parts.length !=0)
    {   
        if("cls".equals(action))
        {
        	for(String cache : parts)
        	{
	            LinkedCache lc = LinkedCacheProvider.INSTANCE.getCache(cache);
	            if(null != lc)
	            {
	                lc.clear();
	            }
        	}
        }
        else if("inv".equals(action))
        {
        	
        	for(String cache : parts)
        	{
            	LinkedCache lc = LinkedCacheProvider.INSTANCE.getCache(cache);
	            if(null != lc)
	            {
	                lc.invalidateDependencies(Collections.singletonList(cache));
	            }
        	}
        }
        
	}

String contextPath= request.getContextPath();
if(request.getAttribute("keys") == null || "reset".equals(request.getParameter("act")) )
{
	request.setAttribute("keys",new TreeSet<String>(LinkedCacheProvider.INSTANCE.getCacheNames()));
}
%>
<html>
<head>
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css' rel="stylesheet" type="text/css" />
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css' rel="stylesheet" type="text/css" />
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css' rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<%=contextPath %>/js/prototype.js'></script>
</script>
<script type="text/javascript" src='<%=contextPath %>/js/sorttable.js'></script>
</script>
<script type="text/javascript">
function checkBeforeSubmit(action)
{	
	var elements = document.getElementsByName("key");	
	var count = 0;
	var keys = "";
	
	for(var k=0;k<elements.length;k++)
	{
		keys = keys + elements[k].value +',';
		if(elements[k].checked)
			{
				count++;
				
			}
	}
	if(count != 0 )
	{	
		var actInput = document.getElementsByName('act');
	 	actInput[0].value = action;
	 	if( action != 'fil')
	 		{
		 		var keysInput = document.getElementsByName('keys');
		 		keysInput[0].value = keys.substring(0,keys.length -1);
	 		}
	}
	else
	{
		if(action == 'fil')
		{
			var actInput = document.getElementsByName('act');
		 	actInput[0].value = 'reset';
		}
		else
		{
			return;
		}
	}
	document.forms[0].submit();
}

</script>
</head>
<body>
<form name="linkedCache" action='<%=contextPath+"/cachetool/linkedcache.jsp"%>' method="post">
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<span class="title-text">
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="linkedCache.title"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
</span>
<br/>
<br/>
<div class="small-text-inset">
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="linkedCache.description"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
</div>
<br/>
<a href="<%=contextPath%>/cachetool/linkedcache.jsp">
<jsp:include page="/cachetool/addTextButton.jsp">
	<jsp:param  name="key" value="linkedCache.button.refresh"/>
</jsp:include>
</a>
<div style="clear:both;"></div> <br/>
<input type="hidden" name="act" value=""/>
<input type="hidden" name="keys" value=""/>
<a href="#" onclick="checkBeforeSubmit('fil');">
	<jsp:include page="/cachetool/addTextButton.jsp">
		<jsp:param  name="key" value="linkedCache.button.filter"/>
	</jsp:include>
</a>
<a href="#" onclick="checkBeforeSubmit('cls');">
	<jsp:include page="/cachetool/addTextButton.jsp">
		<jsp:param  name="key" value="linkedCache.button.clear"/>
	</jsp:include>
</a>
<a href="#" onclick="checkBeforeSubmit('inv');">
<jsp:include page="/cachetool/addTextButton.jsp">
		<jsp:param  name="key" value="linkedCache.button.invalidate"/>
	</jsp:include>
</a>
<div style="clear:both;"></div>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" style="margin-top: 5px">
<tr> 
    <td></td>
    <td class="tile-dark" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	<td></td>
</tr>
<tr>
	<td class="tile-dark" VALIGN="top" WIDTH="1" nowrap="true"><br/></td>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" class="sortable">
	<thead>
	<tr>
		<td class="tile-a" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
		<th align="center" class="tile-b sorttable_nosort" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true"><input type="checkbox" onclick="checkAll('key');" id="selectAll" name="selectAll" class="checkboxLookup" /></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;</th>
		<th class="tile-b sorttable_sorted" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
			<span class="new-table-title">
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="linkedCache.tblTitle.cache"/>
				<jsp:param name="render" value="true"/>
			</jsp:include>
			</span>
			<span id="sorttable_sortfwdind">&nbsp;&#x25be;</span>
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.count"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.maxCount"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b " background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.fillRatio"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.hits"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b " background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.misses"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.hitRatio"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.missesExp"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.getTime"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.puts"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.updates"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.expired"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.removed"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.evicted"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap='true'>
		<span class="new-table-title">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.eternalQ"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.timeToLive"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<span class="new-table-title" title="<%=request.getAttribute("linkedCache.tblTitle.timeToLive")%>">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.TTL"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.timeToIdle"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<span class="new-table-title" title='<%=request.getAttribute("linkedCache.tblTitle.timeToIdle")%>'>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.tblTitle.TTI"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</span></th>
		<th class="tile-b" background='<%=contextPath %>/Xcelerate/graphics/common/screen/grad.gif' nowrap="true">&nbsp;&nbsp;
		</th>
	</tr>
	</thead>
	<tbody>
<%
 	int num = 0;

	Collection<String> caches = null;
	boolean filtered = false;
	if("fil".equals(action))
	{
		if(parts != null && parts.length != 0)
		{
			caches = new TreeSet(Arrays.asList(parts));
			request.removeAttribute("keys");
			request.setAttribute("keys",caches);
			filtered = true;
		}
	}
	
	if(!filtered)
	{
		caches = (Set<String>)request.getAttribute("keys");
	}
	String[] langCountry = locale.split("_");
	java.util.Locale locale_1 = new java.util.Locale(langCountry[0],langCountry[1]);
    for(String cacheName : caches)
    {
        LinkedCache lc = LinkedCacheProvider.INSTANCE.getCache(cacheName);        
        CacheStats stat = lc.getCacheStats();
        CacheRegionConfig conf = lc.getConfig();

        if (num % 2 !=0) 
		{ %>
	<tr class="tile-row-highlight">
	<% }
	else {
	  %>
	<tr class="tile-row-normal">
	<% } %>
			<td><br/></td>
			<td valign="center" align="left"><input type="checkbox" value="<%=cacheName%>" id="<%=cacheName+"1"%>" name="key" onclick="toggleParentOnCheck('key',this);" /></td>
        	<td><br/></td>
			 <td VALIGN="TOP" ALIGN="LEFT" sorttable_customkey="<%=cacheName%>">
		    	<DIV class="small-text-inset"><a href='<%=contextPath%>/cachetool/list.jsp?cachename=<%=cacheName%>'><%=cacheName%></a></div>
			</td>
			<td><br/></td>
		    <td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getSize()%></DIV>
		    
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.getMaxElementsInMemory()%></DIV>
			</td>
			<td><br/></td>
			<% double fillRatio =  ((double)stat.getSize()/(double)(conf.getMaxElementsInMemory()));%>
			<td VALIGN="center" ALIGN="LEFT">
			<span style="padding: 0; float: left; white-space: nowrap;"><%
			if(fillRatio != 0.0) {%><%
			%><img height="12" width="<%=(int)(fillRatio*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar.png" style="padding: 0"><%}if(fillRatio < 1.0) 
			{%><img height="12"  width="<%=(int)((1.0-fillRatio)*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar_background.png" style="padding: 0"><%
			}%>&nbsp;<%=(int)(fillRatio*100)%>%</span></td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getHitCount()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getMissCount()%></DIV>
			</td>
			<% double hitRatio =  ((double)stat.getHitCount()/(double)(stat.getHitCount() + stat.getMissCount()));%>
			<td><br/></td>
			<td VALIGN="center" ALIGN="LEFT" >
			<span style="padding: 0; float: left; white-space: nowrap;"><%
			if(hitRatio != 0.0) {
			%><img height="12" width="<%=(int)(hitRatio*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar.png" style="padding: 0"><%}if(hitRatio < 1.0) 
			{%><img height="12" width="<%=(int)((1.0-hitRatio)*100)%>" src="<%=contextPath%>/Xcelerate/graphics/common/systemtools/progressbar_background.png" style="padding: 0"><%}
			%>&nbsp;<%=(int)(hitRatio*100)%>%</span></td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getMissCountForExpire()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
			<%
			 
			 float d = (int)((stat.getAverageGetTime()*1000));
			%>
		    <DIV class="small-text-inset"><%=String.format(locale_1,"%.3f",d)%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfPuts()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfUpdates()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfExpired()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfRemoved()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=stat.getCountOfEvicted()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.isEternal()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.getTimeToLiveSeconds()%></DIV>
			</td>
			<td><br/></td>
			<td VALIGN="TOP" ALIGN="LEFT">
		    <DIV class="small-text-inset"><%=conf.getTimeToIdleSeconds()%></DIV>
			</td>
			<td><br/></td>
			</tr>
			
			<%
			num++;
			}
			%>
			</tbody>
	</table>
	</td>
	<td class="tile-dark" VALIGN="top" WIDTH="1" nowrap="true"><br/></td>
	</tr>
<tr>
	<td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /></td></tr>
<tr><td></td><td background='<%=contextPath %>/Xcelerate/graphics/common/screen/shadow.gif'><IMG WIDTH="1" HEIGHT="5" src='<%=contextPath %>/Xcelerate/graphics/common/screen/dotclear.gif' /><td></td>
</tr>
</table>
</form>
</body>
</html>
<%
} else {
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.accessError"/>
		<jsp:param name="render" value="true"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include><%
}
%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}

String authorize = (String) session.getAttribute(
	"userAuthorized");
if (Boolean.valueOf(authorize)) {
    String contextPath= request.getContextPath();
    String cacheName = request.getParameter("cachename");
    cacheName = StringEscapeUtils.escapeHtml(cacheName);
%>
<html>
<head>
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css'
	rel="stylesheet" type="text/css">
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css'
	rel="stylesheet" type="text/css">
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css'
	rel="stylesheet" type="text/css">
<%@ include file="./paging.jsp"%>
<script language="javascript" type="text/javascript">
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.invalidSearch"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
function submitSearchForm(action, selectPage)
{
	var url = '<%=contextPath%>/cachetool/urlCache.jsp?action=' + action + '&cachename=<%=cacheName%>';
	var frm = document.searchForm;
	
	//set flush keys if request comes from flush
	if (action === 'flush')
		frm.flushKeys.value = getFlushKeys();
	frm.selectPage.value = selectPage;
	var searchKeyObj = document.searchForm.searchKey;
	if(searchKeyObj.value.indexOf("<",0) == -1 
			&& searchKeyObj.value.indexOf(">",0) == -1)
	{
	document.searchForm.action = url;
	document.searchForm.submit();
	}else{ 
		alert("<%=request.getAttribute("assetCache.invalidSearch")%>");
	}
}

function submitAssetLookUpForm()
{
	var url = "<%=contextPath%>/cachetool/urlCache.jsp?cachename=<%=cacheName%>&searchBy=Dependency&searchKey=";
	var chkboxNum = document.pageForm.dep.length;
	
	var values = {}, valueStr = '';
	
	if (chkboxNum != undefined)
	{
		for (var idx = 0; idx < chkboxNum; idx++) 
		{
			if (document.pageForm.dep[idx].checked == true)
				values[document.pageForm.dep[idx].value] = true;
		}
		
		for (var i in values) {
			if (values.hasOwnProperty(i)) {
				valueStr += i + ',';
			}
		}
		url += valueStr.substr(0, valueStr.length - 1);
		
	}
	else
	{
		if(document.pageForm.dep.checked == true)
			url = url + document.pageForm.dep.value;
	}
	document.pageForm.action = url;
	document.pageForm.submit();
}


function getFlushKeys() {
	var keys = "";
	var chkboxNum = document.pageForm.key.length;
	var count = 0;
	if (chkboxNum != undefined)
	{
	for (var idx = 0; idx < chkboxNum; idx++) 
	{
		if (document.pageForm.key[idx].checked == true)
		{
			count++;
			if (count == 1)
			{
				keys = keys + document.pageForm.key[idx].value;
				
			}
			else
				keys = keys + "," + document.pageForm.key[idx].value;
		}
	}
	}
	else
	{
		if(document.pageForm.key.checked == true)
			keys = keys + document.pageForm.key.value;
	}
	return keys;
}

<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.optionText.any"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.optionText.all"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
function onSearchTypeSelection()
{
	var selectElem = document.searchForm.searchBy;
	var searchForm = document.searchForm;
	if (selectElem.selectedIndex==1)
	{
		searchForm['matchTypeSelect'].style.visibility='visible';
		searchForm['matchTypeSelect'].options[0]=new Option('<%=request.getAttribute("assetCache.optionText.any")%>', "ANY", true, false)
		searchForm['matchTypeSelect'].options[1]=new Option('<%=request.getAttribute("assetCache.optionText.all")%>', "ALL", false, false)
	}
	else 
	{
		searchForm['matchTypeSelect'].style.visibility='hidden';
	}
}

function pagePopup(url) {
	newwindow=window.open(url,'page','height=400,width=600');
	if (window.focus) {newwindow.focus()}
	return false;
}

</script>
</head>
<body>
<jsp:include page="/cachetool/serverTimeZoneInfo.jsp"/>
<%
    //retrieve param values from request
				String searchType = request.getParameter("searchBy") == null
						? "Key"
						: request.getParameter("searchBy");
%>
<table>
	<tr>
		<td>
			<jsp:include page="/cachetool/urlSearch.jsp" />
		</td>
	<tr>
		<td>
		<%
		    if ("Dependency".equals(searchType)) {
		%>
		<jsp:include page="/cachetool/urlCacheDepResult.jsp?action=search" />
		<%
     	} else {
 		%>
 		<jsp:include page="/cachetool/urlResult.jsp" />
 		<%
     	}
 		%>
		</td>
	</tr>
</table></body>
</html>
<% }else{
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
    }
%>

<%@page import="java.util.Date"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.fatwire.assetcache.AssetCacheProvider"%>
<%@page import="com.fatwire.assetcache.AssetCache"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page import="com.fatwire.cache.PageCache.DateOp"%>
<%@ page import="com.fatwire.cache.PageCache.QueryOp"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@ page import="COM.FutureTense.Util.ftMessage"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%!private static final Log log_jsp = LogFactory.getLog(ftMessage.JSP_DEBUG);%>
<%
	String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
    String authorize = (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
        String action = request.getParameter("action");
        if(action == null)
            action = "search";

        int rowsPerPage = 10;
        String searchBy = request.getParameter("searchBy");
        if(searchBy == null)
            searchBy = "Key";
        String matchType = request.getParameter("matchTypeSelect");
        String searchKey = request.getParameter("searchKey");
		searchKey = StringEscapeUtils.escapeHtml(searchKey);
        String flushKeys = request.getParameter("flushKeys");
        
        String cacheName = request.getParameter("cachename");
        if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
            cacheName = CacheUtil.SS_CACHE;
        
        try
        {
            String rowsPerPageStr = request.getParameter("rowsPerPage");
            if(rowsPerPageStr != null)
            { //frontend will validate rowsPerPage value
                rowsPerPage = Integer.parseInt(rowsPerPageStr);
                if(rowsPerPage <= 0)
                    rowsPerPage = Integer.MAX_VALUE;
                session.setAttribute("rowsPerPage", new Integer(
                    rowsPerPage));
            }
            else if(session.getAttribute("rowsPerPage") != null)
                rowsPerPage =
                    ((Integer) session.getAttribute("rowsPerPage"))
                        .intValue();
            else
            {
                session.setAttribute("rowsPerPage", rowsPerPage);
            }
        }
        catch (Exception e)
        {
            if(log_jsp.isErrorEnabled())
            {
                log_jsp
                    .error(
                        "Unexpected error while processing cachetool/dep.jsp.",
                        e);
            }
        }

        String[] searchTypeOptions =
            { "Key", "Dependency" };
        StringBuilder srchTypeOpsSb = new StringBuilder();
		Map searchTypeTextMap = new HashMap();
		%>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="searchType.optionText.key"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<%
		searchTypeTextMap.put("Key",(String)request.getAttribute("searchType.optionText.key"));
		%>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="searchType.optionText.dependency"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<%
		searchTypeTextMap.put("Dependency",(String)request.getAttribute("searchType.optionText.dependency"));
		%>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCache.optionText.any"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<%
		searchTypeTextMap.put(QueryOp.ANY.name(),(String)request.getAttribute("assetCache.optionText.any"));
		%>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCache.optionText.all"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<%
		searchTypeTextMap.put(QueryOp.ALL.name(),(String)request.getAttribute("assetCache.optionText.all"));
        for(String op : searchTypeOptions)
        {
            srchTypeOpsSb.append("<option value=\"").append(op)
                .append("\"");
            if(op.equals(searchBy))
                srchTypeOpsSb.append(" selected");
            srchTypeOpsSb.append(">").append(searchTypeTextMap.get(op)).append("</option>");
        }

        String[][] searchBySelectedOptions =
            { { QueryOp.ANY.name(), QueryOp.ALL.name() } };

        String[] selectedOptions = new String[2];
        StringBuilder matchTypeOpsSb = new StringBuilder();

        //set select ops for searchBy and matchType
        if(!"Key".equals(searchBy))
        {
            
            for(int i = 0; i < selectedOptions.length; i++)
            {
                
                if("Dependency".equals(searchBy))
                    selectedOptions[i] = searchBySelectedOptions[0][i];
                else
                    selectedOptions[i] = "";
            }
            for(String op : selectedOptions)
            {
                matchTypeOpsSb.append("<option value=\"").append(op)
                    .append("\"");
                if(op.equals(matchType))
                    matchTypeOpsSb.append(" selected");
                matchTypeOpsSb.append(">").append(searchTypeTextMap.get(op))
                    .append("</option>");
            }
        }
        int totalCount = 0;
        Set<String> keySet = null;

        //search from cache if request not from paging and set its result in session
        if(!"paging".equals(action))
        {
            //new search will visit cache to get keys
            AssetCache accessor =
                AssetCacheProvider.getCache(cacheName);
            if(!accessor.isAvailable())
            {
				%><jsp:forward page="/cachetool/message.jsp" />
				<%
    		}
            if(flushKeys != null && !"".equals(flushKeys))
            {
                accessor.removeAll(Arrays.asList(flushKeys.split(",")));
            }
            if("Key".equals(searchBy))
            {
                String[] targets = null;
                if(searchKey == null || "".equals(searchKey))
                {
                    targets = new String[1];
                    targets[0] = "*";
                }
                else
                    targets = searchKey.split(",");
                keySet = accessor.findKeys(targets, 0);
            }
            //searchBy Dependency will delegate its key search to display page since diff screen displays for ANY and ALL options  

            if(keySet != null)
            {
                totalCount = keySet.size();
                session.setAttribute("result", keySet);
                session.setAttribute("totalCount", new Integer(
                    totalCount));
            }
        }

        //set session value
        session.setAttribute("rowsPerPage", new Integer(rowsPerPage));
        String contextPath = request.getContextPath();
%>

<form name="searchForm" method="post">
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<table width="100%">
	<tr>
		<td>
		
		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td height="3px"></td>
			</tr>
			<tr>
				<td class="sub-title-text">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="assetSearch.assets"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</td>
			</tr>
			<tr>
				<td height="7px"></td>
			</tr>
			<tr>
				<td>
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td></td>
						<td height="1" valign="TOP" class="tile-dark"><img height="1"
							width="1"
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
						<td></td>
					</tr>
					<tr>
						<td width="1" valign="top" class="tile-dark"><br />
						</td>
						<td>
						<table cellspacing="0" cellpadding="0" border="0"
							bgcolor="#ffffff" width="100%">
							<tr>
								<td colspan=4 class="tile-highlight"><img
									src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
									width="1" height="1"></td>
							</tr>
							<tr>
								<td
									background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
									class="tile-a">&nbsp;</td>
								<td
									background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
									colspan="2" class="tile-b">&nbsp;</td>
								<td
									background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
									class="tile-c">&nbsp;</td>
							</tr>
							<tr>
								<td class="tile-dark" colspan="4"><img height="1" width="1"
									src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
							</tr>
							<tr class="form-shade">
								<td>
								<div class="form-label-inset">
								<jsp:include page="/cachetool/translator.jsp">
									<jsp:param name="key" value="search.itemsPerPage"/>
									<jsp:param name="render" value="true"/>
								</jsp:include>
								</div>
								</td>
								<td><input type="text" name="rowsPerPage" id="rowsPerPage"
									class="form-inset" size=10 value='<%=rowsPerPage%>' /></td>
								<td>
								<div class="form-label-inset">
								<jsp:include page="/cachetool/translator.jsp">
									<jsp:param name="key" value="search.searchBy"/>
									<jsp:param name="render" value="true"/>
								</jsp:include>
								</div>
								</td>
								<td><select name="searchBy" id="searchBy"
									class="form-inset" onchange="onSearchTypeSelection();"><%=srchTypeOpsSb.toString()%></select>
								<select name="matchTypeSelect" id="matchTypeSelect"
									class="form-inset"
									style='visibility:<%=(searchBy == null || searchBy.equals("Key"))
                    ? "hidden" : "visible"%>;'><%=matchTypeOpsSb.toString()%></select>
								</td>
							</tr>
							<tr class="form-shade">
								<td colspan="4" height="5"></td>
							</tr>
							<tr valign=middle>
								<td colspan=4><textarea class="form-inset" name="searchKey"
									id="searchKey" rows="4" cols="60"><%=(null == searchKey) ? "" : searchKey%></textarea>
								</td>
							</tr>
						</table>
						</td>
						<td width="1" valign="top" class="tile-dark"><br />
						</td>
					</tr>
					<tr>
						<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
							height="1" width="1"
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
					</tr>
					<tr>
						<td></td>
						<td background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'>
						<img height="5" width="1" src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
						<td></td>
					</tr>
				</table>
				<input type="hidden" name="flushKeys" id="flushKeys" />
				<input type="hidden" name="selectPage" id="selectPage" />
				<div onclick="submitSearchForm('search','1');" style="float:right">
					<jsp:include page="/cachetool/addTextButton.jsp">
						<jsp:param  name="key" value="search.button.search"/>
					</jsp:include>
				</div>
				</td>
			</tr>
		</table>
		
		</td>
		<td align=right>
		<fieldset align=right><legend>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="search.tableLegend"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</legend>
		<table cellspacing="3" cellpadding="0" border="0">
			<tr>
				<td class="strikeBackGround">Product_C:123:2</td>
				<td class="legend-text">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="search.invalidatedPage"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</td>
			</tr>
			<tr>
				<td class="invalidateDep">asset-123:Content_C</td>
				<td class="legend-text">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="search.updatedDependency"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</td>
			</tr>
			<tr>
				<td>asset-123:Page Superscript<sup class="supText">5</sup></td>
				<td class="legend-text">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="search.dependencyGenerationCnt"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</td>
			</tr>
		</table>
		</fieldset>
		</td>
	</tr>
</table>
</form>
<%
    }
    else
    {
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
    }
%>

/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwincludenewasset', 'it',
{
	fwincludenewasset :
	{
	  assetCreatorIncludeDlgTitle : 'Creatore asset',
	  assetCreatorIncludeTitleDesc : 'Crea e includi un nuovo asset',
	  assetCreatorLinkTitleDesc : 'Crea e collega un nuovo asset',
	  PleaseSelectLinkTextFrom	: 'Selezionare prima il testo da collegare all\'asset' 
	
	
	  	    
	}
});



/***************************************************************************************
                            fwlinkasset
  Created: March 25, 2010
  By:      JAG
  Reason:  The FW Tray ToolBar
           to perform the Link An asset command and
           functionality

  Modified:  It's possible to customize every single aspect of the editor,
  from the toolbar, to the skin, to dialogs,
  and here we customize the context menu to detect all
  FW  included and linked Assets in an instance of running ckeditor.


  The editor structure is totally plugin based,
  including many of its core features.
  Plugins live in separate files, making them easy to organize and maintain.
  CKEditor  JavaScript API makes developing plugins flexible.

  Modified: Localization

  CKEditor automatically detects the user language,
  automatically configuring its interface to be localized to it.
  This editor speaks the language your users speak.

**********************************************************************************/


CKEDITOR.plugins.add('fwlinkasset',   
  {    
    requires: ['fwlinkasset'],
	lang : ['en','fr','es','de','it','ja','ko','zh-cn','zh','pt-br'],
	
	// When Instace of the Editor is Created this is the Initilization  Callback 
	// for the plug-in button with the editor handle passed in 
    init:function(editor)
	{ 
	   var b="fwlinkasset";

	   var c=editor.addCommand( b, {
		   // Click on Link a  Asset...
		   exec : function( ckeditor )    //assetname, assetId, assetType, fieldname, fielddesc, embedtype, isFckEditor
				  {
					var ckConfig = ckeditor.config;
					var cs_environment =  ckConfig.cs_environment ;
					if(cs_environment!='ucform'&&cs_environment!='insite')
					{
					   var showSiteTree = ckConfig.showSiteTree ;

					   // Link Asset Request From Advanced UI  ?
					   if(showSiteTree==='true')
					   {
						  CKAdvancedUIAddLink.addLinkedAssetT(ckeditor) ;
						  return ;
					   }else {
							CKAdvancedUIAddLink.addLinkedAsset(ckeditor) ;
					   }
					} else {
						// Escape single quotes for linktext. Javascript special variable.
						var linkText = encodeURIComponent(CKUtilities.getCKEditorSelection(ckeditor)).replace( /'/g, '%27' ),
							dlgMgr= (ckConfig.isMultiValued)? fw.ui.ObjectFactory.createManagerObject('dialog') : parent.fw.ui.ObjectFactory.createManagerObject('dialog'),
							range,selectedNode,fieldname;
						fieldname = ((!ckConfig.fieldName)? ckeditor.name : 
								((ckConfig.editingstyle==="single")? ckConfig.fieldName : ckConfig.fieldName.substr(0,ckConfig.fieldName.lastIndexOf('_'))));
						assettypes= (!ckConfig.allowedassettypes)? "*": ckConfig.allowedassettypes;
						if(decodeURIComponent(linkText)=="_#CSEMBED_IMAGE#_"){
							//It is a Media asset. 
							if(CKEDITOR.env.ie){
								ckeditor.getSelection().lock();
							}
							range = editor.getSelection().getRanges();
							selectedNode=range[0].getCommonAncestor();
							linkText=(selectedNode.getAttribute('_fwid')!=null)?encodeURIComponent('<span id="'+ selectedNode.getAttribute('_fwid')+ '">_#CSEMBED_IMAGE#_</span>'):"";}
						// FIX for PR# 28038.
						if(!linkText || linkText=="") {
							dlgMgr.alert(editor.lang.fwlinkasset.PleaseSelectLinkTextFrom);
							return false;
						}
						env=(CKEDITOR.env.webkit)? "webkit" : ((CKEDITOR.env.gecko)? "gecko":"ie");
						// CKEditor must invoke the dialogmanager which is a singleton object.
						var CK = ckConfig.isMultiValued ? new fw.ui.controller.CKEditorController : new parent.fw.ui.controller.CKEditorController;
						CK.openCKDialog({
							args: {
								action: 'insert',
								item: 'link',	
								fieldName: fieldname,
								linkText: linkText,
								DEPTYPE : ckConfig.DEPTYPE,
								env : env,
								isMultiVal : ckConfig.isMultiValued,
								deviceid: ckConfig.deviceid
							},
							assettypes: assettypes
						});
				}
			},
		   canUndo: false    // No support for undo/redo
    	});

	
		c.modes={wysiwyg:1,source:0};
	
		c.canUndo=false;
 	    editor.ui.addButton("fwlinkasset",{
					label  : editor.lang.fwlinkasset.AddEmbeddedLinkDlgTitle,
					command: b,
					icon:    this.path+"fwlinkasset.gif"
		});
		
	

    }
});

/*************************************************************************************
                                  CKEditor Via Advanced
 Created; April 15, 2010
 Reason :   FCKNonEditable  CKEditor frameworks
 This is a plugin to make the include assets non editable.
 Also includes some utility and helper methods.
*************************************************************************************/

var CKAdvancedUIAddLink =
{
    // Called to perform Asset Include  into Advanced Select From Tree
    addLinkedAssetT : function (editor )
    {
        var ckConfig = editor.config;
		var assetname =  ckConfig.assetName ;
		var assetId = ckConfig.assetId ;
		var AssetType =  ckConfig.assetType ;
		var fieldname =  ckConfig.fieldName ;
		var fielddesc = ckConfig.fieldDesc ;
		var editingstyle = ckConfig.editingstyle ;
		// Select from Asset
        var EncodedString = parent.parent.frames["XcelWorkFrames"].frames["XcelTree"].document.TreeApplet.exportSelections()+'';
        var allowedassettypes = editor.config.allowedassettypes;
        var contextroot = editor.config.CSSitePath;
        
        if ( EncodedString!=null )
        {
         var idArray = EncodedString.split(',');
         var assetcheck = unescape(idArray[0]);
         if (assetcheck.indexOf('assettype=')!=-1 && assetcheck.indexOf('id=')!=-1)
         {
           var test = new String(EncodedString);
           var nameVal = test.split(",");
           if (nameVal.length!=2) {
               alert(editor.lang.fwlinkasset.PleaseSelectOnlyOneAsset);
               return false;
           }

           var treeid = unescape(nameVal[0]);
           var splitid = treeid.split(',');
           if (splitid.length==1)
           {
               alert(editor.lang.fwlinkasset.PleaseSelectAssetFromTree);
               return false;
           }
           var splitpair = splitid[0].split("=");
           var tn_id = splitpair[1];
           splitpair = splitid[1].split("=");
           var tn_AssetType = splitpair[1];
		  
		   //Check if the selected assettype is allowed
		   if(allowedassettypes && allowedassettypes.toUpperCase().indexOf(tn_AssetType.toUpperCase()) == -1){
				alert(editor.lang.fwlinkasset.CannotLinkOrIncludeThisAsset + allowedassettypes);
				return false;
		   }
           
           // using ckeditor
		   var selectedHTML = CKEDITOR.tools.trim(CKUtilities.getCKEditorSelection(editor));
           if (selectedHTML == "")
           {
                alert(editor.lang.fwlinkasset.PleaseSelectLinkTextFrom);
                return false;
           }

           // Craft The URL to be added
           var pURL = "../.." + contextroot + "ContentServer?pagename=OpenMarket/Xcelerate/Actions/";

           pURL = pURL + "EmbeddedLink";
		   
           // Call EmbeddedLink.xml for Asset Popup Dialog

           pURL = pURL + "&tn_id=" + tn_id;
           pURL = pURL + "&tn_AssetType=" + tn_AssetType;
           pURL = pURL + "&name=" + encodeURIComponent(assetname);
           pURL = pURL + "&AssetType=" + AssetType;
		   pURL = pURL + "&fieldname=" + encodeURIComponent(fieldname);
           pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
           pURL = pURL + "&formFieldName=" + encodeURIComponent(fieldname);
           pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
		   pURL = pURL + "&IFCKEditor=true";

           return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=500,location=no,menubar=no,resizable=yes,status=no,toolbar=no,scrollbars=yes,width=500,height=650");
        }
        else
        {
          alert(editor.lang.fwlinkasset.PleaseSelectAssetFromTree);
        }
       }
       else
       {
         alert(editor.lang.fwlinkasset.PleaseSelectAssetFromTree);
       }
    },
addLinkedAsset : function ( editor )
    {
        
	   var ckConfig = editor.config;
	   var allowedassettypes = ckConfig.allowedassettypes;
	   var assetname =  ckConfig.assetName ;
	   var assetId = ckConfig.assetId ;
	   var AssetType =  ckConfig.assetType ;
	   var fieldname =  ckConfig.fieldName ;
	   var fielddesc = ckConfig.fieldDesc ;
	   var editingstyle = ckConfig.editingstyle ;
	   var pURL = editor.config.csRootContext +  "ContentServer?pagename=OpenMarket/Xcelerate/Actions/PickAssetPopup";
	   var selectedHTML = CKEDITOR.tools.trim(CKUtilities.getCKEditorSelection(editor));
       if (selectedHTML == "")
           {
                alert(editor.lang.fwlinkasset.PleaseSelectLinkTextFrom);
                return false;
           }
		   
       var i=0, histString='', currentVal;
       if (parent.parent.parent['XcelBanner'])
       {
		 for(i=0;i<parent.parent.parent['XcelBanner'].hist.length;i++)
		 {
				currentVal = parent.parent.parent['XcelBanner'].hist[i];
				if (i==0)
					histString=currentVal.id;
				else
					histString=histString+","+currentVal.id;
		 }
       }
	   pURL = pURL + "&FCKName=" + encodeURIComponent(assetname);
	   pURL = pURL + "&cs_SelectionStyle=single";
       pURL = pURL + "&cs_CallbackSuffix=";
	   pURL = pURL + "&IFCKEditor=true";
       pURL = pURL + "&FCKAssetId=" + assetId;
	   pURL = pURL + "&FCKAssetType=" + AssetType;
	   pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
       pURL = pURL + "&cs_FieldName=" + encodeURIComponent(fieldname);
       pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
	   pURL = pURL + "&embedtype=link";
       pURL = pURL + "&cs_History=" + encodeURIComponent(histString);
	   if(allowedassettypes){
		pURL = pURL + "&cs_AllowedAssetType=" + encodeURIComponent(allowedassettypes);
	   }
	   return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=500,location=no,menubar=no,resizable=yes,scrollbars=yes,top=50,width=500,height=550");
	}
} ;
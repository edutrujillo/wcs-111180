/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwlinknewasset', 'fr',
{
	fwlinknewasset :
	{
	  assetCreatorIncludeDlgTitle : 'Créateur de ressource',
	  assetCreatorIncludeTitleDesc : 'Créer et inclure une ressource',
	  assetCreatorLinkTitleDesc : 'Créer et lier une ressource',
	  PleaseSelectLinkTextFrom	: 'Le texte à lier à la ressource doit d\'abord être sélectionné' 
	
	
	  	    
	}
});


/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwincludeasset', 'ko',
{
	fwincludeasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: '자산 링크 추가',
	   IncludeEmbeddedLinkDlgTitle	: '자산 포함' ,
	   PleaseSelectLinkTextFrom	    : '자산에 링크할 텍스트를 먼저 선택해야 합니다.' ,
	   PleaseSelectAssetFromTree	: '트리에서 자산을 선택하십시오.',
	   PleaseSelectAssetFromSearchResults : '검색 결과에서 자산 선택',
	   PleaseSelectOnlyOneAsset 	: '자산을 하나만 선택할 수 있습니다.',
	   CannotAddSelfInclude	        : 'Include를 통한 자체 참조는 허용되지 않습니다.\r\n다른 자산을 선택하십시오.',
       CannotLinkOrIncludeThisAsset : '이 자산은 링크 또는 포함이 허용되지 않습니다.\n다음 유형의 자산을 선택하십시오.\n'
			    
	}
});



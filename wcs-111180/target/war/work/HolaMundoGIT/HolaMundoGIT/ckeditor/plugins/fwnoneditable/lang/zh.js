/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'zh',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'要移除包括的資產?',
	 RemoveLinkedAssetQuestion:'要移除連結的資產?'
	}
});

/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwlinkasset', 'de',
{
	fwlinkasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: 'Assetlink hinzufügen',
	   IncludeEmbeddedLinkDlgTitle	: 'Asset einbeziehen' ,
	   PleaseSelectLinkTextFrom	    : 'Der Text zur Verknüpfung mit dem Asset muss zuerst gewählt werden' ,
	   PleaseSelectAssetFromTree	: 'Wählen Sie ein Asset aus dem Baum.',
	   PleaseSelectAssetFromSearchResults : 'Wählen Sie ein Asset aus den Suchergebnissen',
	   PleaseSelectOnlyOneAsset 	: 'Nur ein Asset kann gewählt werden.',
	   CannotAddSelfInclude	        : 'Selbstreferenz durch "Einbeziehen" ist nicht zulässig. Wählen Sie ein anderes Asset.',
       CannotLinkOrIncludeThisAsset : 'Dieses Asset kann nicht verknüpft oder einbezogen werden.\nWählen Sie Assets des folgenden Typs:\n'
			    
	}
});



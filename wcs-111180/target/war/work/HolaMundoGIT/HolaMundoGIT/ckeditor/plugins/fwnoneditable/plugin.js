/*
 * CKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2008 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * This is a plugin to make the include assets non editable.
 *
 * Written by: Deephan Mohan.
 *
 */
 
 CKEDITOR.scriptLoader.load( CKEDITOR.basePath + "plugins/fwnoneditable/htmlrenderer.js");		


 CKEDITOR.plugins.add('fwnoneditable',{    
    
    requires: ['fwnoneditable'],
	lang : ['en','fr','es','de','it','ja','ko','zh-cn','zh','pt-br'],
	
	// When Instace of the Editor is Created this is the Initilization  Callback 
	// for the plug-in button with the editor handle passed in 	
    init:function(editor){	
	var FCKNonEditable = {				
				_includeElement:'DIV',
				_includeElementForLink:'SPAN',
				_includeAttribute:'_fwid',
				_linkElement:'A',
				_linkAttribute:'_fwhref',			

				_SetListeners : function( editor){						
					editor=editor.editor;				
					if ( editor.mode != "wysiwyg" ){
						editor.textarea.on( 'keydown',function(event){
							if ( !event.data.$.ctrlKey && !event.data.$.metaKey )
							CKUtilities.setDirtyTab(editor);
						}, this );
						editor.textarea.on('paste',function(){
								CKUtilities.setDirtyTab(editor);
							},this);
						return;						
					}
					editor.on( 'saveSnapshot', function( evt )
						{
							if ( !evt.data || !evt.data.contentOnly )
								CKUtilities.setDirtyTab(editor);
						});
					editor.on( 'selectionChange', function( evt )
						{
							if ( !evt.data || !evt.data.contentOnly )
								CKUtilities.setDirtyTab(editor);
						});
					editor.document.on("keydown", FCKNonEditable._OnKeyDown,this,null,1);
					editor.document.on("click", FCKNonEditable._ClickListener,this,null,1);
					editor.document.on("keyup",function(event){
						if ( !event.data.$.ctrlKey && !event.data.$.metaKey )
						CKUtilities.setDirtyTab(editor);
					},this,null,1);
					editor.document.on("dragstart" ,function(evt) {								
						if(CKUtilities.getCustomElement(editor)){
							evt.data.preventDefault(true);evt.data.stopPropagation();
						}else{								
							var range=null, nativeSel, clonedSelection,division,htmlText;
							nativeSel = editor.getSelection().getNative();
							if(CKEDITOR.env.ie){
								range = nativeSel.createRange();
								htmlText = range.htmlText;
							} else {
								range = nativeSel.getRangeAt( 0 ) ;
								clonedSelection = range.cloneContents();							
								division = document.createElement('div');
								division.appendChild(clonedSelection);
								htmlText = division.innerHTML;
							}						
							if(htmlText.indexOf('_CSEMBEDTYPE_')!=-1){								
								evt.data.preventDefault(true);evt.data.stopPropagation();
							}
						}
					}) ;				
				},
				_OnKeyDown:function ( evt ){					
					var CKSelect = editor.document.getSelection();	
					var	selectedNode = CKUtilities.getCustomElement(editor);					
					if(CKSelect){				
						var htmlText='';					
						var div = document.createElement('div');										
						//Check if there is already something selected or not
						//if not then select the included/linked asset		
						var nativeSel = editor.getSelection().getNative();
						if(CKEDITOR.env.ie){
							var range = nativeSel.createRange();
							
							if(range && range.htmlText)
							htmlText = range.htmlText;
						} else {
							var range = nativeSel.getRangeAt( 0 ) ;
							var clonedSelection = range.cloneContents();
							div.appendChild(clonedSelection);
							htmlText = div.innerHTML;
						}
						if(htmlText.length == 0){					
						//try to select the linked/included asset						
							if(selectedNode){																	
								editor.getSelection().selectElement(selectedNode );
								FCKNonEditable._Block(evt,selectedNode);
							}						
						} else {														   		
							var type = CKSelect.getType();
							div.innerHTML = htmlText;														
							if(htmlText.indexOf('_CSEMBEDTYPE_')!=-1){
								//The div element can contain multiple levels of elements so we need to validate 
								//whether only included asset is selected or there are more elements								
								var containsOnlyEmbeddedElm = true,
									parentElm = div;
								while(true){
									if(parentElm.firstChild && parentElm.firstChild == parentElm.lastChild && parentElm.firstChild.hasChildNodes){
										parentElm = parentElm.firstChild;
										continue;
									} else {
										if(parentElm.firstChild)
											containsOnlyEmbeddedElm = false;
										break;
									}
								}						
								if(containsOnlyEmbeddedElm) {
									//Redundant code here but this needs to be checked before 
									//to prevent IE invalid pointer mismatch issue.										
									// Don't block arrow keys, pg up/down, and F1-F12
									var k =  evt.data.getKeystroke();
									if ((k > 32 && k < 41) || (k > 111 && k < 124)) {
										return;
									}														
									if(selectedNode) {
										FCKNonEditable._Block(evt,selectedNode);
									}
								} else {								
									//Since it contains other elements don't do anything.
									if (evt.data.preventDefault) {
										evt.data.preventDefault(true);
										evt.data.stopPropagation();
									} else { 
										evt.data.returnValue = false;
									}
								}
							} else {								
								//try to select the linked/included asset																
								if(selectedNode) {
									editor.getSelection().selectElement(selectedNode );
									FCKNonEditable._Block(evt,selectedNode); 			
								} else {
									return;
								}
							}
						}	
					}								
				},
				_ClickListener:function ( evt,node ){				
					//listener for mouse clicks
					return;
				},
				_Block:function ( evt,node ){									
					var k= -1,						
						evt=evt.data;								
					if (evt.getKeystroke()) {
						k = evt.getKeystroke();				
					}
					
					// Don't block arrow keys, pg up/down, and F1-F12
					if ((k > 32 && k < 41) || (k > 111 && k < 124)) {
						return;
					}				
					//Backspace and delete should have the delete functionality but with firefox
					//backspace causes the whole page to go back and breaks the ckeditor.So 
					//preventing back space along with other keys.				
					if(k==8){						
						evt.preventDefault( true );
						return;
					} else if (k==46){										
						if(node && node.getParent()){							
							var deleteEmbed = null;
							if(node.getName() == 'A'||node.getName()=='a') {
								deleteEmbed = confirm(editor.lang.fwnoneditable.RemoveLinkedAssetQuestion);
								// The original text should be replaced after the link has been removed. Replace it only when the linked asset has to be removed.
								if(deleteEmbed){
									var element = new CKEDITOR.dom.text(node.getText(),editor.document );							
									element.insertAfter(node);
								}
							} else {												
								deleteEmbed = confirm(editor.lang.fwnoneditable.RemoveIncludedAssetQuestion);						
							}
							if(deleteEmbed){							
								node.remove();
							}
							evt.preventDefault( true );
							return;
						}	
					}				
					var element = new CKEDITOR.dom.text('#TEXT#',editor.document );							
					element.insertAfter(node);			
					editor.getSelection().selectElement(element );							
					if(CKEDITOR.env.webkit){
						editor.getSelection().scrollIntoView();					
						return;
					} else {
						editor.getSelection().reset();
					}
					element.remove();				
				}			
			}							
			return FCKNonEditable;
		},		
			
		
		afterInit: function(editor){var FCKNonEditable = this.init(editor);		
		
		editor.on( 'dataReady', FCKNonEditable._SetListeners,this,null,1 ) ;	
		
		var cancel;

		var onKeyDown = function( event )
		{
		
		
			// The DOM event object is passed by the "data" property.
			event = event.data;
			if(event.$.cancelBubble)
				return;
			var keyCombination = event.getKeystroke();
			var command = this.keystrokes[ keyCombination ];
			var editor = this._.editor;

			cancel = ( editor.fire( 'key', { keyCode : keyCombination } ) === true );

			if ( !cancel )
			{
				if ( command )
				{
					var data = { from : 'keystrokeHandler' };
					cancel = ( editor.execCommand( command, data ) !== false );
				}

				if  ( !cancel )
				{
					var handler = editor.specialKeys[ keyCombination ];
					cancel = ( handler && handler( editor ) === true );

					if ( !cancel )
						cancel = !!this.blockedKeystrokes[ keyCombination ];
				}
			}

			if ( cancel )
				event.preventDefault( true );

			return !cancel;
		};
	 
		var onKeyPress = function( event )
		{
			if ( cancel )
			{
				cancel = false;
				event.data.preventDefault( true );
			}
		};

		CKEDITOR.keystrokeHandler.prototype =
		{
			
			
			attach : function( domObject )
			{
				// For most browsers, it is enough to listen to the keydown event
				// only.						
				domObject.on('keydown', FCKNonEditable._OnKeyDown,this,null,1);
				domObject.on( 'keydown', onKeyDown,this,null,100);			
				
				// Some browsers instead, don't cancel key events in the keydown, but in the
				// keypress. So we must do a longer trip in those cases.
				if ( CKEDITOR.env.opera || ( CKEDITOR.env.gecko && CKEDITOR.env.mac ) ) {				
				domObject.on('keypress', FCKNonEditable._OnKeyDown,this,null,1);
					domObject.on( 'keypress', onKeyPress, this,null,100 );
				}
			}
		};
		}
	}); 
	


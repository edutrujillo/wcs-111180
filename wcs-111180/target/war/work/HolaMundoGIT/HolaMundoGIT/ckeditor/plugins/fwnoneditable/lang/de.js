/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'de',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'Möchten Sie das einbezogene Asset entfernen?',
	 RemoveLinkedAssetQuestion:'Möchten Sie das verknüpfte Asset entfernen?'
	}
});

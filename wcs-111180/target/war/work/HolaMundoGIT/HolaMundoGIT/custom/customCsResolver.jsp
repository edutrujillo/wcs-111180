<%@ page import="java.util.Map" %>
<%@ page import="com.fatwire.auth.identity.CustomCsResolver" %>
<%@ page import="com.fatwire.security.common.EncodedParameterMap" %>
<%
	// Custom CS Resolver page
	// This JSP handles resolving a identifier to a set of
	// content server credentials. 
	String configString = request.getParameter("payload"); 

	// decode the parameters so they are available internally
	Map<String,String> configurationMap = EncodedParameterMap.decode(configString);
		
	// Invoke Mother's little helper to map from prsId to CS system user. 
	Map<String,String> credentials = CustomCsResolver.getCredentials(configurationMap);
	if ( credentials == null ) {
		%>
error=Unable to map the supplied identifier to a CS system user
		<%
		return; 
	}
	String result = EncodedParameterMap.encode(credentials);
	%>
<%=result%>
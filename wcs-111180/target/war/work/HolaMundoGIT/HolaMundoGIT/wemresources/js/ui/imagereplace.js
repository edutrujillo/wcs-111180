var elementsImageReplacement = {
	
	dataUriDetected: false,
	supportDataUri: true,
	flashVersionDetected: false,
	hasFlash: false,
	requiredFlashVersion: "9.0.0",
		
	setSwfPath : function (path) {
		this.swfPath = path;
	},
	
	hasRequiredFlashVersion : function () {
		if(!this.flashVersionDetected) {
			this.flashVersionDetected = true;
			this.hasFlash = swfobject.hasFlashPlayerVersion(this.requiredFlashVersion);
		}
		return this.hasFlash;
	},
	
	hasDataUriSupport : function () {
	    if(!this.dataUriDetected) {
	    	this.dataUriDetected = true;
		    if (navigator.appName == 'Microsoft Internet Explorer') {
				var ua = navigator.userAgent;
		        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		        if (re.exec(ua) != null) {
		            rv = parseFloat(RegExp.$1);
		            //The version was changed below from 8 to 9 so that all the IE versions till 8 can have same behavior.
		            if(rv < 9) {
		            	this.supportDataUri = false;
		            }
		        }
		    }
			if (navigator.appName == 'Netscape') {
				if(navigator.userAgent.toLowerCase().indexOf('applewebkit') != -1)
					this.supportDataUri = false;
		    }
		}
	    return this.supportDataUri;
	},
	
	display : function (data,element, width, height) {
		var replaceElement = document.getElementById(element);
			
		if(this.hasDataUriSupport()) {
			/*var image = document.createElement("img");
			image.src="data:image/png;base64," + data;
			image.width = width;
			image.height = height;
			
			replaceElement.parentNode.replaceChild(image,replaceElement);*/
			
			replaceElement.innerHTML = '<img src="data:image/png;base64,' + data + '" width="' + width + '" height="' + height + '" />';
		}
		else {
			// display flash
			if(this.hasRequiredFlashVersion()) {
				var flashvars = {
					data: urlencode(data),
					width: width,
					height: height
				};
				var params = {scale: "noscale", wmode: "transparent"};
				replaceElement.innerHTML = '<span id="' + replaceElement.id + '-temp"></span>';
				swfobject.embedSWF(this.swfPath, replaceElement.id + "-temp", width, height, this.requiredFlashVersion,null, flashvars, params);
			}
		}
	}
};



function urlencode( str ) {
    // URL-encodes string  
    // 
    // version: 904.1412
    // discuss at: http://phpjs.org/functions/urlencode
    // +   original by: Philip Peterson
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: AJ
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Brett Zamir (http://brettz9.blogspot.com)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: travc
    // +      input by: Brett Zamir (http://brettz9.blogspot.com)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Lars Fischer
    // %          note 1: info on what encoding functions to use from: http://xkr.us/articles/javascript/encode-compare/
    // *     example 1: urlencode('Kevin van Zonneveld!');
    // *     returns 1: 'Kevin+van+Zonneveld%21'
    // *     example 2: urlencode('http://kevin.vanzonneveld.net/');
    // *     returns 2: 'http%3A%2F%2Fkevin.vanzonneveld.net%2F'
    // *     example 3: urlencode('http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a');
    // *     returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a'
                             
    var histogram = {}, tmp_arr = [];
    var ret = (str+'').toString();
    
    var replacer = function(search, replace, str) {
        var tmp_arr = [];
        tmp_arr = str.split(search);
        return tmp_arr.join(replace);
    };
    
    // The histogram is identical to the one in urldecode.
    histogram["'"]   = '%27';
    histogram['(']   = '%28';
    histogram[')']   = '%29';
    histogram['*']   = '%2A';
    histogram['~']   = '%7E';
    histogram['!']   = '%21';
    histogram['%20'] = '+';
    histogram['\u00DC'] = '%DC';
    histogram['\u00FC'] = '%FC';
    histogram['\u00C4'] = '%D4';
    histogram['\u00E4'] = '%E4';
    histogram['\u00D6'] = '%D6';
    histogram['\u00F6'] = '%F6';
    histogram['\u00DF'] = '%DF';
    histogram['\u20AC'] = '%80';
    histogram['\u0081'] = '%81';
    histogram['\u201A'] = '%82';
    histogram['\u0192'] = '%83';
    histogram['\u201E'] = '%84';
    histogram['\u2026'] = '%85';
    histogram['\u2020'] = '%86';
    histogram['\u2021'] = '%87';
    histogram['\u02C6'] = '%88';
    histogram['\u2030'] = '%89';
    histogram['\u0160'] = '%8A';
    histogram['\u2039'] = '%8B';
    histogram['\u0152'] = '%8C';
    histogram['\u008D'] = '%8D';
    histogram['\u017D'] = '%8E';
    histogram['\u008F'] = '%8F';
    histogram['\u0090'] = '%90';
    histogram['\u2018'] = '%91';
    histogram['\u2019'] = '%92';
    histogram['\u201C'] = '%93';
    histogram['\u201D'] = '%94';
    histogram['\u2022'] = '%95';
    histogram['\u2013'] = '%96';
    histogram['\u2014'] = '%97';
    histogram['\u02DC'] = '%98';
    histogram['\u2122'] = '%99';
    histogram['\u0161'] = '%9A';
    histogram['\u203A'] = '%9B';
    histogram['\u0153'] = '%9C';
    histogram['\u009D'] = '%9D';
    histogram['\u017E'] = '%9E';
    histogram['\u0178'] = '%9F';
    
    // Begin with encodeURIComponent, which most resembles PHP's encoding functions
    ret = encodeURIComponent(ret);
    
    for (search in histogram) {
        replace = histogram[search];
        ret = replacer(search, replace, ret) // Custom replace. No regexing
    }
    
    // Uppercase for full PHP compatibility
    return ret.replace(/(\%([a-z0-9]{2}))/g, function(full, m1, m2) {
        return "%"+m2.toUpperCase();
    });
    
    return ret;
}


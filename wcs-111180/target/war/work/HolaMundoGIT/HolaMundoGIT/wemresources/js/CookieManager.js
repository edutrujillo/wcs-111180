/*=====
	// CookieManager is a utility class which provides methods for accessing cookies.
	// CookieManager is used by WemContext for preserving user details and attributes.
}
=====*/
// Constructor for the CookieManager

function CookieManager () {
	
}

//Methods for CookieManager class.
CookieManager.prototype = {
	// Sets cookie using document.cookie.
	// @param	name: Name of the cookie
	// @param	value:	Value for the cookie
	// @param	expiredays:	number of days after which the cookie becomes invalid
	// @param	properties will be in associative array format eg: {path:'/test'}
	setCookie : function(name,value,expiredays,/*cookieProps?*/properties)
	{
       if ( value !== 'undefined' ) {
        if(expiredays)
		{	 // convert days into milliseconds
			var expires = expiredays * 1000 * 60 * 60 * 24;
			var today = new Date();
			var expires_date = new Date( today.getTime() + (expires) );			
			// add cookie to the document. #22780 Escaping the cookie value
			cookieValue = name +"="+ escape(value) + ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" );
		}
		else
		{
			// create a cookie without speicifying the date.Will expire when the browser is closed.			
			cookieValue = name +"="+ escape(value);
		}
		
		 for(propName in properties){
			cookieValue += "; " + propName;
			var propValue = properties[propName];
			if(propValue !== true){ cookieValue += "=" + propValue; }
		}
		 document.cookie = cookieValue;
	   }
	},
	// Gets cookie value for the passed name
	// @param	name: Name of the cookie
	getCookie : function (name)
	{
		var cookies = document.cookie;
		var matches = cookies.match(new RegExp("(?:^|; )" + this.escapeString(name) + "=([^;]*)"));
		// #22780 unescaping the cookie value
		return matches ? unescape(matches[1]) : undefined; // String or undefined
	},
	// Expires the cookie by setting the expire time to some time the past
	// @param	name: Name of the cookie
	removeCookie : function (name, properties)
	{
		console.log("in remove cookie" ,name, properties);
		var propstr = '';
		for(propName in properties){
			propstr += "; " + propName;
			var propValue = properties[propName];
			if(propValue !== true){ propstr += "=" + propValue; }
		}
		document.cookie = name +"=deleted;expires=Thu, 01-Jan-1970 00:00:01 GMT" + propstr;		
	},	
	
	// Reads all the cookie and returns in array format
	getCookies : function ()
	{
		var cookies = new Object();
		var c = document.cookie;		
		var matches = c.split(new RegExp("(?:^|; )"));
		for(index in matches)
		{
			var cookie = matches[index].split("=");
			cookies[cookie[0]]=	cookie[1];
		}		
		return cookies;
	},
	// Escapes special characters
	// @value
	escapeString : function(key) {
		return key+"".replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, function(character){		
			return "\\" + character;
		}); // String
	}
} 



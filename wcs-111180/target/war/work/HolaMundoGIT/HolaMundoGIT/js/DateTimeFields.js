/*

This file declares a dijit widget for use with date/time fields.
It combines a DateTextBox and three NumberSpinners, and stores its form value
in the format specified in the implementations of the
serialize/deserialize methods of the widget instance.
(Default is YYYY-MM-DD HH:mm:ss which is suitable for Advanced UI.)

*/

dojo.require('dijit.form.DateTextBox');
dojo.require('dijit.form.NumberSpinner');

dojo.addOnLoad(function() {
	function pad(str, len, chr) {
		//work around funny keyboard event bug in widget
		if (!str || isNaN(str)) { str = '0'; }
		
		str = String(str); //convert to string if not already
		//defaults
		len = len || 2;
		chr = chr || '0';
		//prepend chr to str until length len
		while (str.length < len) {
			str = chr + str;
		}
		return str;
	}
	
	function p10(str) {
		//parseInt a string in base 10.
		return parseInt(str, 10);
	}
	
	function format2digit(value) {
		//called in the context of a NumberSpinner widget.
		//only pad if not currently focused (to prevent fighting against the user)
		return this._focused ? value : pad(value);
	}
	
	//getter/setter abstractions for Dojo 1.2 - 1.4:
	function getter (widget, attr) { return widget.attr(attr); }
	function setter (widget, attr, value, opt) {
		if (typeof opt != 'undefined') { widget.attr(attr, value, opt); }
		else { widget.attr(attr, value); }
	}
	
	var dateRx = /^\s*(\d{4})-([0-1]\d)-([0-3]\d)(?: ([0-2]\d):([0-6]\d):([0-6]\d)(?:\.\d*)?)?\s*$/;
	
	dojo.declare('fw.DateTimeFields', [dijit._Widget, dijit._Templated], {
		value: '',
		
		// useNewWindow: boolean
		//		If true, will open the Calendar in a new window.
		useNewWindow: false,
		
		templateString: '<div id="widget_${id}" class="dateTimeFields">' +
			'<input id="${id}_date" dojoAttachPoint="dateNode"> ' +
			'<input id="${id}_h" dojoAttachPoint="hourNode">:' +
			'<input id="${id}_m" dojoAttachPoint="minuteNode">:' +
			'<input id="${id}_s" dojoAttachPoint="secondNode">' +
			'<input id="${id}" type="hidden" ${!nameAttrSetting} dojoAttachPoint="hiddenNode">' +
			'<img src="' + dojo.config.fw_csPath + 'Xcelerate/graphics/common/icon/iconDeleteContent.gif"' +
			' class="dateTimeClear" dojoAttachPoint="clrNode" dojoAttachEvent="onclick:reset"></div>',
		
		attributeMap: dojo.delegate(dijit._Widget.prototype.attributeMap, {
			id: 'hiddenNode'
		}),
		
		postMixInProperties: function() {
			//just like form widgets in Dojo 1.3+ (Dojo #8660)
			var name = this.name || (this.srcNodeRef && dojo.attr(this.srcNodeRef, 'name'));
			this.nameAttrSetting = name ? 'name="' + name + '"' : '';
			this.inherited(arguments);
		},
		
		postCreate: function() {
			this.inherited(arguments);
			
			var
				dtbArgs = {
					constraints: { datePattern: 'yyyy-MM-dd'},
					name: 'date_' + this.id //add throwaway name to avoid CS XML trippage
				},
				widgets = this._widgets = [],
				commonSpinnerArgs = {
					name: 'hms_' + this.id, //add throwaway name to avoid CS XML trippage
					intermediateChanges: true,
					format: format2digit,
					editOptions: { pattern: '##' },
					lang: this.lang
				},
				img, focusTimer;
			
			if (this.useNewWindow) {
				//add calendar icon to use as trigger for opening popup
				//(this allows us to avoid a whole plethora of focus shenanigans)
				img = this.popupNode = document.createElement('img');
				img.src = dojo.config.fw_csPath + 'Xcelerate/graphics/common/icon/calendar.gif';
				img.className = 'calendarIcon';
				img.onclick = dojo.hitch(this, function() {
					this._openNewWindow();
				});
				this.domNode.insertBefore(img, this.hourNode);
				
				//clobber default open/close logic of DateTextBox with no-op
				dtbArgs._open = dtbArgs._close = function() {};
			}
			
			widgets.push(this.dateBox =
				new dijit.form.DateTextBox(dtbArgs, this.dateNode));
			widgets.push(this.hourBox = new dijit.form.NumberSpinner(
				//TODO?: 1..12 + AM/PM
				dojo.mixin({ constraints: { min: 0, max: 23 } }, commonSpinnerArgs),
				this.hourNode));
			widgets.push(this.minBox = new dijit.form.NumberSpinner(
				dojo.mixin({ constraints: { min: 0, max: 59 } }, commonSpinnerArgs),
				this.minuteNode));
			widgets.push(this.secBox = new dijit.form.NumberSpinner(
				dojo.mixin({ constraints: { min: 0, max: 59 } }, commonSpinnerArgs),
				this.secondNode));
			
			dojo.forEach(this._widgets, function(w) {
				this.connect(w, 'onChange', '_update');
				if (w.declaredClass == 'dijit.form.NumberSpinner') {
					//override wonky scroll-wheel logic on spinners.
					//this is based on the original NumberSpinner code, but
					//ensures that the value is only ever changed in increments of 1.
					w._mouseWheeled = function(evt) {
						var
							dir = ((evt.wheelDelta || -evt.detail) > 0 ? 1 : -1),
							node = (dir > 0 ? this.upArrowNode : this.downArrowNode);
						
						this._arrowPressed(node, dir, this.smallDelta);
						if (this._wheelTimer) { clearTimeout(this._wheelTimer); }
						this._wheelTimer = setTimeout(dojo.hitch(this, '_arrowReleased', node), 50);
						dojo.stopEvent(evt);
					};
				}
			}, this);
			
			if (dojo.isIE) {
				//IE doesn't like focus being called on a hidden DOM node.
				//Makes sense really, but other code in Advanced UI will try
				//to do it anyway, so nerf it:
				this.hiddenNode.focus = function() {};
			}
		},
		
		startup: function() {
			this.inherited(arguments);
			dojo.forEach(this._widgets, function(w) { w.startup(); });
			if (this.params.value) { setter(this, 'value', this.params.value); }
		},
		
		uninitialize: function() {
			this.inherited(arguments);
			dojo.forEach(this._widgets, function(w) { w.destroyRecursive(); });
		},
		
		isValid: function() {
			return dojo.every(this._widgets, function(w) { return w.isValid(); });
		},
		
		reset: function() {
			this._updating = true; //prevent unnecessary _update calls
			dojo.forEach(this._widgets, function(w) { w.reset(); });
			//update values of internal variable and hidden form field
			this.value = this.hiddenNode.value = '';
			delete this._updating;
		},
		
		serialize: function(d) {
			// summary:
			//		Serializes a date to a string.  Can be overridden for customization.
			return pad(d.getFullYear(), 4) +
				'-' + pad(d.getMonth() + 1) +
				'-' + pad(d.getDate()) +
				' ' + pad(d.getHours()) +
				':' + pad(d.getMinutes()) +
				':' + pad(d.getSeconds());
		},
		
		deserialize: function(str) {
			// summary:
			//		Deserializes a string to a date.  On failure, returns null.
			//		Can be overridden for customization.
			
			var d, m = dateRx.exec(str);
			if (!m) { return null; }
			
			d = new Date();
			d.setFullYear(p10(m[1]));
			d.setMonth(p10(m[2]) - 1);
			d.setDate(p10(m[3]));
			d.setHours(p10(m[4] || '00'));
			d.setMinutes(p10(m[5] || '00'));
			d.setSeconds(p10(m[6] || '00'));
			return d;
		},
		
		_setValueAttr: function(value) {
			var d;
			if (!this._started) { return false; } //widgets not here yet
			
			if (typeof value == 'string') {
				d = this._toDate(value);
			} else {
				//assume Date object or number coercible to date
				d = value instanceof Date ? value : new Date(value);
			}
			
			if (!d || isNaN(d.getFullYear())) { return; }
			
			this.value = d;
			//update widgets
			this._updating = true;
			setter(this.dateBox, 'value', d);
			setter(this.hourBox, 'value', d.getHours());
			setter(this.minBox, 'value', d.getMinutes());
			setter(this.secBox, 'value', d.getSeconds());
			//update hidden field
			this.hiddenNode.value = this._fromDate(d);
			delete this._updating;
		},
		
		_toDate: function(str) {
			if (!str) { return null; }
			return this.deserialize(str);
		},
		
		_fromDate: function(d) {
			if (!d || isNaN(d.getFullYear())) { return ''; }
			var r = this.serialize(d);
			return r;
		},
		
		_update: function() {
			//employ semaphore so that child widget changes invoked BY this widget
			//don't incur excessive/recursive calls
			if (this._updating) { return; }
			
			var d, str;
			
			if (this.value === '') {
				//ensure fields are initialized first for proper isValid result
				//(avoid running widget getters since they tend to revert invalid states)
				if (!this.dateBox.textbox.value) {
					setter(this.dateBox, 'value', new Date());
				}
				//value has never been set; initialize other components
				dojo.forEach([this.hourBox, this.minBox, this.secBox], function(w) {
					!w._focused && setter(w, 'value', 0);
				});
			}
			
			if (this.isValid()) {
				this._updating = true;
				
				//sync value from child widgets (honoring serialize/deserialize)
				d = new Date(getter(this.dateBox, 'value'));
				d.setHours(getter(this.hourBox, 'value'));
				d.setMinutes(getter(this.minBox, 'value'));
				d.setSeconds(getter(this.secBox, 'value'));
				this.value = d;
				this.hiddenNode.value = this._fromDate(d);
				
				delete this._updating;
			} else {
				this.value = null; //NOT '' - avoid the initialization path above
				//ensure field is picked up/reflected as invalid in our UIs
				this.hiddenNode.value = this.dateBox.textbox.value + ' ' +
					this.hourBox.textbox.value + ':' + this.minBox.textbox.value + ':' +
					this.secBox.textbox.value;
			}
		},
		
		_openNewWindow: function() {
			// summary:
			//		Called when the calendar icon is clicked (only applicable if
			//		useNewWindow is set true on this widget at instantiation).
			
			var pp = this._pickerPopup, cs = dojo.config.fw_csPath;
			if (pp && !pp.closed) { pp.focus(); return; }
			
			pp = this._pickerPopup = window.open('', 'popup_' + this.id,
				"width=240,height=270,status=no,resizable=no");
			
			if (!dojo.isIE) { this._initNewWindow(pp); }
			pp.document.write(
'<html>' +
	'<head>' +
		'<link rel="stylesheet" type="text/css" href="' + cs + 'js/dojo/resources/dojo.css">' +
		'<link rel="stylesheet" type="text/css" href="' + cs + 'js/dijit/themes/tundra/tundra.css">' +
	'</head>' +
	'<body class="tundra">' +
		'<script type="text/javascript" src="' + cs + 'js/dojo/dojo.js" ' +
			'djConfig="locale:\'' + dojo.config.locale + '\'"></script>' +
		'<script type="text/javascript">' +
			'window.onload = function() {' + //needed to prevent races in non-FF
				'dojo.require("dijit._Calendar");' +
				'dojo.addOnLoad(function() {' +
					'var c = new dijit._Calendar({' +
						'onValueSelected: function(date) {' +
							'_openerWidget.dateBox.attr("value", date);' +
							//not sure why IE doesn't just do this like everything else...
							'if (dojo.isIE) { _openerWidget._update(); }' +
							'_openerWidget._closeNewWindow();' +
						'}' +
					'});' +
					//need new Date() for IE to wrap in an instance from this frame
					'c.attr("value", new Date(_openerWidget.dateBox.attr("value")));' +
					'c.placeAt(dojo.body());' +
					'_openerWidget._opened = true;' + //set here to avoid premature closing
				'});' + //end dojo.addOnLoad
			'};' + //end window.onload
		'</script>' +
	'</body>' +
'</html>'
			);
			pp.document.close();
			if (dojo.isIE) { this._initNewWindow(pp); }
		},
		
		_initNewWindow: function(win) {
			// summary:
			//		Performs non-document-write-related window init functionality.
			//		This is split out since it works for IE in a different place
			//		than it does for others.
			win._openerWidget = this;
			win.onunload = dojo.hitch(this, function() {
				//clean up variables when this window is about to be closed
				delete this._pickerPopup;
				delete this._opened;
			});
		},
		
		_closeNewWindow: function() {
			// summary:
			//		Complement to _openNewWindow.
			
			if (this._opened && this._pickerPopup) {
				this._pickerPopup.close();
			}
		}
	});
	
	//also expose a few useful reusable functions within this namespace
	fw.DateTimeFields.util = {
		pad: pad,
		p10: p10
	};
});
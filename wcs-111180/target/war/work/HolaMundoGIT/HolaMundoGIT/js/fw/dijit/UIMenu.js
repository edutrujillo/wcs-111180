/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UIMenu"]){dojo._hasResource["fw.dijit.UIMenu"]=true;dojo.provide("fw.dijit.UIMenu");dojo.require("fw.dijit.UIMenuWithShadow");dojo.declare("fw.dijit.UIMenu",fw.dijit.UIMenuWithShadow,{unhoverTimeout:100,menuTimer:null,outItem:null,cleanOnClose:false,postCreate:function(){this.inherited(arguments);if(this.cleanOnClose){dojo.connect(this,"onOpen",this,function(){this.destroyDescendants();});}},onItemUnhover:function(_1){this.inherited(arguments);this.outItem=_1;},onItemHover:function(){if(this.parentMenu&&this.parentMenu.hidePopupTimer){clearTimeout(this.parentMenu.hidePopupTimer);this.parentMenu.hidePopupTimer=null;}clearTimeout(this.menuTimer);this.menuTimer=null;this.inherited(arguments);},onClose:function(){this.inherited(arguments);if(this.menuTimer){clearTimeout(this.menuTimer);this.menuTimer=null;}},_mouseGone:function(){this.onCancel();}});}
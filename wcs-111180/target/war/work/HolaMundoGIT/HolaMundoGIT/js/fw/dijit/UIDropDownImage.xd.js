/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UIDropDownImage"],["require","fw.dijit.UIActionImage"],["require","dijit._HasDropDown"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UIDropDownImage"]){_4._hasResource["fw.dijit.UIDropDownImage"]=true;_4.provide("fw.dijit.UIDropDownImage");_4.require("fw.dijit.UIActionImage");_4.require("dijit._HasDropDown");_4.declare("fw.dijit.UIDropDownImage",[fw.dijit.UIActionImage,_5._HasDropDown],{focus:function(){}});}}};});
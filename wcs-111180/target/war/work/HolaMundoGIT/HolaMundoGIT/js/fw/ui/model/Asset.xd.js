/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.model.Asset"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.model.Asset"]){_4._hasResource["fw.ui.model.Asset"]=true;_4.provide("fw.ui.model.Asset");_4.declare("fw.ui.model.Asset",null,{id:0,type:"",status:"",lockStatus:null,_isNewAsset:false,ccsourceid:0,preselected_dimensionid:0,preselected_dimensiontype:"Dimension",constructor:function(_7){_4.mixin(this,_7);},set:function(_8,_9){if(!_8){return;}var _a=this[_8];this[_8]=_9;this._onPropertyChange(_8,_9,_a);},get:function(_b){if(!_b){return;}return this[_b];},_onPropertyChange:function(_c,_d,_e){this.onUpdate(_c,_d,_e);},onUpdate:function(_f,_10,_11){},isNew:function(){return this._isNewAsset;},setNew:function(_12){this._isNewAsset=_12;}});}}};});
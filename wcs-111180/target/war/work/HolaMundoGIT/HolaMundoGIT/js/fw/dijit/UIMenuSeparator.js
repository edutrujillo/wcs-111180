/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UIMenuSeparator"]){dojo._hasResource["fw.dijit.UIMenuSeparator"]=true;dojo.provide("fw.dijit.UIMenuSeparator");dojo.require("dijit.MenuSeparator");dojo.declare("fw.dijit.UIMenuSeparator",dijit.MenuSeparator,{templateString:dojo.cache("fw.ui.dijit","templates/UIMenuSeparator.html","<div style=\"width: 100%; border-top: 1px solid #e8e8e8\"></div>\r\n"),});}
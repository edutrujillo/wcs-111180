/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UIComboBox"],["require","dijit.form.ComboBox"],["require","fw.dijit.UIComboBoxMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UIComboBox"]){_4._hasResource["fw.dijit.UIComboBox"]=true;_4.provide("fw.dijit.UIComboBox");_4.require("dijit.form.ComboBox");_4.require("fw.dijit.UIComboBoxMixin");_4.declare("fw.dijit.UIComboBox",[_5.form.ComboBox,fw.dijit.UIComboBoxMixin],{});}}};});
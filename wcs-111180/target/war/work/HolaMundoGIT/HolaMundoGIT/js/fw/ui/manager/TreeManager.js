/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.manager.TreeManager"]){dojo._hasResource["fw.ui.manager.TreeManager"]=true;dojo.provide("fw.ui.manager.TreeManager");(function(){function _1(){return dijit.registry.byClass("fw.dijit.UITree").filter(function(w){return (w.dndController&&w.dndController.declaredClass=="fw.ui.dnd.TreeSource");});};dojo.declare("fw.ui.manager.TreeManager",null,{updateActiveFrame:function(_2){var _3=dojo.query("iframe",_2.containerNode)[0],_4=_1(),_5=this;if(_3){_4.forEach(function(_6){_6.dndController.setTargetFrame(_3);});}else{if(_2._xhrDfd){var _7=dojo.connect(_2,"onLoad",function(){if(_2._isShown()){_5.updateActiveFrame(_2);}dojo.disconnect(_7);});}else{_4.forEach(function(_8){_8.dndController.setTargetFrame(null);});}}},enableDragTracking:function(_9){_1().forEach(function(_a){_a.dndController.transferEnabled=(_9!==false?true:false);});}});}());}
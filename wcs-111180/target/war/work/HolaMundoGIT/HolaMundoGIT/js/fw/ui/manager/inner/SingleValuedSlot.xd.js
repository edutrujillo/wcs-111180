/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.manager.inner.SingleValuedSlot"],["require","fw.ui.manager.inner.Slot"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.manager.inner.SingleValuedSlot"]){_4._hasResource["fw.ui.manager.inner.SingleValuedSlot"]=true;_4.provide("fw.ui.manager.inner.SingleValuedSlot");_4.require("fw.ui.manager.inner.Slot");_4.declare("fw.ui.manager.inner.SingleValuedSlot",fw.ui.manager.inner.Slot,{set:function(_7){this.inherited(arguments);if(_7){if(_7.id&&_7.type){this.parentAsset.set(this.parentField,{id:_7.id,type:_7.type},this.getIndex());this.parentAsset.markEdited(this.parentField);}if(_7.tname){this.setStatus("ED");}}},isContentEditable:function(){var _8=this.getParentAsset();if(_8){if(!_8.isEditable()){this.reason=fw.util.getString("UI/UC1/JS/NoAssetPermissions");return false;}return true;}return true;},clear:function(){var _9=this.parentAsset;if(_9){delete this.assetType;delete this.assetId;delete this.assetName;_9.set(this.parentField,"",this.getIndex());_9.markEdited(this.parentField);}}});}}};});
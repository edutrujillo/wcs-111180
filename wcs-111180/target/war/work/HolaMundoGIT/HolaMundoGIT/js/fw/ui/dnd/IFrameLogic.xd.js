/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dnd.IFrameLogic"],["require","dojo.dnd.Manager"],["require","fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dnd.IFrameLogic"]){_4._hasResource["fw.ui.dnd.IFrameLogic"]=true;_4.provide("fw.ui.dnd.IFrameLogic");_4.require("dojo.dnd.Manager");_4.require("fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar");fw.ui.dnd.IFrameLogic={transferSource:function(_7){if(_7.source.grid){var m=_4.dnd.manager();var _8=m.makeAvatar;m._dndPlugin=_7.source.dndPlugin;m.makeAvatar=function(){var _9=new fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar(m);return _9;};m.startDrag(_7.source,_7.source.dndElem.getDnDNodes(),_7.copy);m.makeAvatar=_8;}else{_4.dnd.manager().startDrag(_7.source,_7.nodes,_7.copy);}},returnSource:function(){var _a=_4.dnd.manager();if(_a.source){return _a;}},cancelDnd:function(){_4.publish("/dnd/cancel");_4.dnd.manager().stopDrag();}};}}};});
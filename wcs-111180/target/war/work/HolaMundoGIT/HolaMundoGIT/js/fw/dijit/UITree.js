/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UITree"]){dojo._hasResource["fw.dijit.UITree"]=true;dojo.provide("fw.dijit.UITree");dojo.require("dijit.Tree");dojo.declare("fw.dijit.UITree",dijit.Tree,{dndAccept:false,dataAttr:"name",persist:true,typeAttr:"",postCreate:function(){this.dndParams=this.dndParams.concat(["accept","dataAttr","typeAttr"]);this.inherited(arguments);this._openedItemIds={};this.persist=true;},getIconStyle:function(_1,_2){var _3=this.model,_4=_3.store,_5,_6;if(_4.isItem(_1)&&_4.hasAttribute(_1,"iconUrl")){_5=_4.getValue(_1,"iconUrl");_6={backgroundImage:"url('"+_5+"')"};return _6;}}});}
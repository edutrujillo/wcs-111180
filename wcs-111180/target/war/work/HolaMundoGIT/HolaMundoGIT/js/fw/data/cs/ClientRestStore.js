/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data.cs.ClientRestStore"]){dojo._hasResource["fw.data.cs.ClientRestStore"]=true;dojo.provide("fw.data.cs.ClientRestStore");dojo.require("fw.data.ClientStore");dojo.require("fw.data.cs._Resources");dojo.require("fw.data.util");dojo.declare("fw.data.cs.ClientRestStore",fw.data.ClientStore,{resource:"",params:null,constructor:function(_1){var _2=fw.data.cs._Resources.resolveStoreArgs(_1);dojo.mixin(this,_2);if(!this.transportOptions){this.transportOptions={};}this.transportOptions.sync=true;},onReceiveData:function(_3){this.inherited(arguments);this.comparatorMap={};if(_3.idAttribute){this.comparatorMap[_3.idAttribute]=fw.data.util.caseInsensitiveComparator;}if(_3.labelAttribute){this.comparatorMap[_3.labelAttribute]=fw.data.util.caseInsensitiveComparator;}}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.TooltipDialog"]){dojo._hasResource["fw.ui.dijit.TooltipDialog"]=true;dojo.provide("fw.ui.dijit.TooltipDialog");dojo.require("dijit.TooltipDialog");dojo.declare("fw.ui.dijit.TooltipDialog",dijit.TooltipDialog,{templateString:dojo.cache("fw.ui.dijit","templates/TooltipDialog.html","<div role=\"presentation\" tabIndex=\"-1\">\r\n\t<div class=\"dijitTooltipContainer\" role=\"presentation\">\r\n\t\t${!_roundedCornerTemplateString}\r\n\t\t<div class =\"dijitTooltipContents dijitTooltipFocusNode\" dojoAttachPoint=\"containerNode\" role=\"dialog\"></div>\r\n\t</div>\r\n\t<div class=\"dijitTooltipConnector\" role=\"presentation\"></div>\r\n</div>\r\n"),_roundedCornerTemplateString:dojo.cache("fw.ui.dijit","templates/RoundedCorners.html","<div class=\"background\"></div>\r\n<div class=\"topLeft corner\"></div>\r\n<div class=\"top horizontal\"><div></div></div>\r\n<div class=\"topRight corner\"></div>\r\n<div class=\"left vertical\"><div></div></div>\r\n<div class=\"right vertical\"><div></div></div>\r\n<div class=\"bottomLeft corner\"></div>\r\n<div class=\"bottom horizontal\"><div></div></div>\r\n<div class=\"bottomRight corner\"></div>\r\n"),baseClass:"fwTooltipDialog"});}
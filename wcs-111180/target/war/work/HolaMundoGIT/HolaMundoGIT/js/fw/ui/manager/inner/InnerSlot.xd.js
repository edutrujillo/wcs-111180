/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.manager.inner.InnerSlot"],["require","fw.ui.manager.inner.Slot"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.manager.inner.InnerSlot"]){_4._hasResource["fw.ui.manager.inner.InnerSlot"]=true;_4.provide("fw.ui.manager.inner.InnerSlot");_4.require("fw.ui.manager.inner.Slot");_4.declare("fw.ui.manager.inner.InnerSlot",fw.ui.manager.inner.Slot,{set:function(_7){this.inherited(arguments);var _8;if(_7){if(_7.id&&_7.type){_8=this.parentAsset;if(_8){_8.set(this.parentField,{id:_7.id,type:_7.type},this.getIndex());_8.markEdited(this.parentField);}}if(_7.tname){this.setStatus("ED");}}},isContentEditable:function(){var _9=this.getParentAsset();if(_9){if(!_9.isEditable()){this.reason=fw.util.getString("UI/UC1/JS/NoAssetPermissions");return false;}return true;}return true;},clear:function(){var _a=this.parentAsset;if(_a){delete this.assetType;delete this.assetId;delete this.assetName;_a.set(this.parentField,"",this.getIndex());_a.markEdited(this.parentField);}}});}}};});
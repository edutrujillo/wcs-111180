/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.slot.MultiSlotContainer"],["require","dijit._Widget"],["require","dijit._CssStateMixin"],["require","fw.ui.slot.ActivatorMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.slot.MultiSlotContainer"]){_4._hasResource["fw.ui.slot.MultiSlotContainer"]=true;_4.provide("fw.ui.slot.MultiSlotContainer");_4.require("dijit._Widget");_4.require("dijit._CssStateMixin");_4.require("fw.ui.slot.ActivatorMixin");_4.declare("fw.ui.slot.MultiSlotContainer",[_5._Widget,_5._CssStateMixin,fw.ui.slot.ActivatorMixin],{slotArgs:null,overlayTitle:"",baseClass:"fwMultiSlot",buttons:[],constructor:function(_7){this.slotArgs={};},postCreate:function(){this.inherited(arguments);this._addActivator();}});}}};});
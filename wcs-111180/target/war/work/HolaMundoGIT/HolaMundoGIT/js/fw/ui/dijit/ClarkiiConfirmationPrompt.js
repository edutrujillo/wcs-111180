/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.ClarkiiConfirmationPrompt"]){dojo._hasResource["fw.ui.dijit.ClarkiiConfirmationPrompt"]=true;dojo.provide("fw.ui.dijit.ClarkiiConfirmationPrompt");dojo.require("fw.ui.dijit.ConfirmationPrompt");dojo.declare("fw.ui.dijit.ClarkiiConfirmationPrompt",fw.ui.dijit.ConfirmationPrompt,{insertImage:"",_slsInsert:fw.util.getString("UI/Forms/InsertImage"),templateString:dojo.cache("fw.ui.dijit","templates/ClarkiiConfirmationPrompt.html","<div>\r\n <div class=\"clarkiiprompt\" dojoAttachPoint=\"containerNode\"></div>\r\n <div class=\"buttons innerBox\">\r\n  ${!_roundedCornerTemplateString}\r\n  <div dojoType=\"fw.ui.dijit.Button\" buttonStyle=\"grey\" dojoAttachEvent=\"onClick:onConfirm\">${_slsYes}</div>\r\n  <div dojoType=\"fw.ui.dijit.Button\" buttonStyle=\"grey\" dojoAttachEvent=\"onClick:onDecline\">${_slsNo}</div>\r\n  <div dojoType=\"fw.ui.dijit.Button\" buttonStyle=\"grey\" dojoAttachEvent=\"onClick:onInsert\">${_slsInsert}</div>\r\n </div>\r\n</div>\r\n"),onInsert:function(){}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.view.SimpleAssetView"],["require","fw.ui.view.SimpleView"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.view.SimpleAssetView"]){_4._hasResource["fw.ui.view.SimpleAssetView"]=true;_4.provide("fw.ui.view.SimpleAssetView");_4.require("fw.ui.view.SimpleView");_4.declare("fw.ui.view.SimpleAssetView",fw.ui.view.SimpleView,{getElementArgs:function(_7){var _8=this.get("model"),_9;_7=_7||{};if(_8){_9=_8.get("asset");_7.id=_9.id;_7.type=_9.type;_7.assetTypeParam=_9.type;}return _7;}});}}};});
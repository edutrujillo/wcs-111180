/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.form.Form"],["require","dijit.form.Form"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.form.Form"]){_4._hasResource["fw.ui.dijit.form.Form"]=true;_4.provide("fw.ui.dijit.form.Form");_4.require("dijit.form.Form");_4.declare("fw.ui.dijit.form._FormMixin",null,{_getValueAttr:function(){var _7={};_4.forEach(this.getDescendants(),function(_8){var _9=_8.name;if(!_9||_8.disabled){return;}var _a=_8.get("value");if(typeof _8.checked=="boolean"){if(/Radio/.test(_8.declaredClass)){if(_a!==false){_4.setObject(_9,_a,_7);}else{_a=_4.getObject(_9,false,_7);if(_a===undefined){_4.setObject(_9,null,_7);}}}else{var _b=_4.getObject(_9,false,_7);if(!_b){_b=[];_4.setObject(_9,_b,_7);}if(_a!==false){_b.push(_a);}}}else{var _c=_4.getObject(_9,false,_7);if(typeof _c!="undefined"){if(_4.isArray(_c)){if(_4.isArray(_a)){for(var x=0;x<_a.length;x++){_c.push(_a[x]);}}else{_c.push(_a);}}else{_4.setObject(_9,[_c,_a],_7);}}else{_4.setObject(_9,_a,_7);}}});return _7;}});_4.declare("fw.ui.dijit.form.Form",[_5.form.Form,fw.ui.dijit.form._FormMixin],{});}}};});
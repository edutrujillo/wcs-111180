/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.view.MobilityPreview"],["require","fw.ui.view.Preview"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.view.MobilityPreview"]){_4._hasResource["fw.ui.view.MobilityPreview"]=true;_4.provide("fw.ui.view.MobilityPreview");_4.require("fw.ui.view.Preview");_4.declare("fw.ui.view.MobilityPreview",[fw.ui.view.Preview],{init:function(){this.inherited(arguments);this.deviceImageWidget&&this.deviceImageWidget.showViewLabel();}});}}};});
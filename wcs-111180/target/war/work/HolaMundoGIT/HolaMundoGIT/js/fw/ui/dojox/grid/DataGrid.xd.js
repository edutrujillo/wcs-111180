/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dojox.grid.DataGrid"],["require","dojox.grid.DataGrid"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dojox.grid.DataGrid"]){_4._hasResource["fw.ui.dojox.grid.DataGrid"]=true;_4.provide("fw.ui.dojox.grid.DataGrid");_4.require("dojox.grid.DataGrid");_4.declare("fw.ui.dojox.grid.DataGrid",_6.grid.DataGrid,{postCreate:function(){this.inherited(arguments);_4.safeMixin(this.selection,{clickSelect:function(_7,_8,_9){if(this.mode=="none"){return;}this._beginUpdate();if(this.mode!="extended"){if(this.isSelected(_7)){this.deselect(_7);}else{this.select(_7);}}else{var _a=this.selectedIndex,_b=this.getSelectedCount();if(!_8){this.deselectAll(_7);}if(_9){this.selectRange(_a,_7);}else{if(_8){this.toggleSelect(_7);}else{if(this.isSelected(_7)&&_b===1){this.deselect(_7);}else{this.addToSelection(_7);}}}}this._endUpdate();}});}});fw.ui.dojox.grid.DataGrid.markupFactory=function(_c,_d,_e,_f){return _6.grid._Grid.markupFactory(_c,_d,_e,_4.partial(_6.grid.DataGrid.cell_markupFactory,_f));};}}};});
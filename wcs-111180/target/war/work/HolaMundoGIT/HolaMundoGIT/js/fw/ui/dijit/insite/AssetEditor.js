/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.insite.AssetEditor"]){dojo._hasResource["fw.ui.dijit.insite.AssetEditor"]=true;dojo.provide("fw.ui.dijit.insite.AssetEditor");dojo.require("dijit._Widget");dojo.require("fw.ui.dijit.TooltipBar");dojo.declare("fw.ui.dijit.insite.AssetEditor",dijit._Widget,{widgetArgs:null,editorParams:null,value:null,postMixInProperties:function(){this.inherited(arguments);var _1=this.widgetArgs;_1.dialogArgs={attrType:"textField",assetId:_1.assetId,fieldName:_1.fieldName,editor:this.editor,editorParams:this.editorParams,title:_1.fieldName,dialogType:"MultiValuedAssetDialog",isBinary:false,onSave:function(_2){}};},startup:function(){this.inherited(arguments);var _3=new fw.ui.dijit.TooltipBar({connectId:[this.id],buttonList:["reorder"],args:this.widgetArgs.dialogArgs});this.tooltip=_3;},isValid:function(){return true;}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.controller.DashboardController"],["require","fw.ui.controller.BaseController"],["require","fw.ui.view.DashboardView"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.controller.DashboardController"]){_4._hasResource["fw.ui.controller.DashboardController"]=true;_4.provide("fw.ui.controller.DashboardController");_4.require("fw.ui.controller.BaseController");_4.require("fw.ui.view.DashboardView");_4.declare("fw.ui.controller.DashboardController",fw.ui.controller.BaseController,{init:function(_7){var _8=this;if(!this.initialized){this.initialized=true;this.appContext=_7;if(!this.view){this.view=new fw.ui.view.DashboardView({appContext:this.appContext});this.view.controller=this;this.view.viewType="dashboard";this.view.set("title","");_4.when(this.view.show({closable:false}),function(){_8.view.toolbar.onClick=_4.hitch(_8,_8.onClick);});}}},onClick:function(_9,_a,_b){SitesApp.event(_9,_a,_b);},handleEvent:function(_c){if(_c.name==="refresh"){return this.refresh();}},refresh:function(){var _d=this,_e=new _4.Deferred();_4.when(this.view.refresh(),function(){_d.view.toolbar.onClick=_4.hitch(_d,_d.onClick);_e.resolve();},function(e){_e.reject(e);});return _e;}});}}};});
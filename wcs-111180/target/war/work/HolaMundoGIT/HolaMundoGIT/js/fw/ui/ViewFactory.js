/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.ViewFactory"]){dojo._hasResource["fw.ui.ViewFactory"]=true;dojo.provide("fw.ui.ViewFactory");(function(){var _1=[];fw.ui.ViewFactory={buildView:function(_2,_3){var _4,_5,_6,_7,_8,_9,_a,_b;_3=_3||{};_5=SitesApp.getConfig("views")[_2];if(!_5){var e=new Error("Missing configuration for view type \""+_2+"\" in the \"views\" section");e.title="Invalid configuration";throw e;}if(typeof _5==="string"){_6=_5;}else{if(typeof _5==="object"){_6=_5.viewClass;_7=_5.controllerClass||_6;_a=_5.isSingleton;_b=_5.viewParams;}}if(_6){if(_a&&_1[_6]){_4=_1[_6];}else{_8=dojo.getObject(_6);if(typeof _8==="function"){dojo.mixin(_3,{appContext:SitesApp});if(_b){dojo.mixin(_3,_b);}_4=new _8(_3);_4.set("viewType",_2);if(_7&&_7!==_6){_9=dojo.getObject(_7);if(typeof _9==="function"){_4.set("controller",new _9(dojo.mixin(_3,{view:_4,appContext:SitesApp})));}else{var c=fw.ui.ObjectFactory.getController(_7);if(c){_4.set("controller",c);c.set("appContext",SitesApp);c.set("view",_4);}else{throw new Error("Unable to find constructor for controller: "+_7);}}}else{_4.set("controller",_4);_4.set("view",_4);}if(_a){_1[_6]=_4;}}else{throw new Error("Unable to find constructor for "+_6);}}}else{throw new Error("Unable to find configuration for view type: \""+_2+"\"");}return _4;}};}());}
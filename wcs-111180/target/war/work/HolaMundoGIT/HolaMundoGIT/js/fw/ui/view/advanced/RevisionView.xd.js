/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.view.advanced.RevisionView"],["require","fw.ui.view.AdvancedView"],["require","fw.ui.view.TabbedViewMixin"],["require","fw.ui.controller.BaseDocController"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.view.advanced.RevisionView"]){_4._hasResource["fw.ui.view.advanced.RevisionView"]=true;_4.provide("fw.ui.view.advanced.RevisionView");_4.require("fw.ui.view.AdvancedView");_4.require("fw.ui.view.TabbedViewMixin");_4.require("fw.ui.controller.BaseDocController");(function(){_4.declare("fw.ui.view.advanced.RevisionView",[fw.ui.controller.BaseDocController,fw.ui.view.AdvancedView,fw.ui.view.TabbedViewMixin],{getAdvancedURLParams:function(){var _7=this.model.get("asset");if(_7&&this.params&&this.params.rev){return {ThisPage:"RevisionDetailsFront",AssetType:_7.type,id:_7.id,rev:this.params.rev};}else{throw new Error(fw.util.getString("UI/UC1/JS/CannotRenderRevision"));}},init:function(){this.inherited(arguments);var _8=this.params&&this.params.rev,_9="",_a;if(_8){_9=fw.util.getString("dvin/UI/AssetMgt/Version")+" "+_8;}this.toolbar.set("version",_9);}});})();}}};});
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dnd.util"]){dojo._hasResource["fw.ui.dnd.util"]=true;dojo.provide("fw.ui.dnd.util");dojo.require("fw.data.util");fw.ui.dnd.util={isTreeSource:function(_1){return "typeAttr" in _1&&"dataAttr" in _1;},isGridSource:function(_2){return "grid" in _2;},treeSourceCreator:function(_3,_4){var n=dojo.create("div"),i=_3.item,_5=_3.tree,_6=_3.tree.model.store,_7=_6.getLabelAttributes()[0],_8={node:n,data:fw.data.util.serializeStoreItem(_6,i,["id",_5.typeAttr,"name","subtype"]),type:[_6.getValue(i,_5.typeAttr)],subtype:[_6.getValue(i,"subtype")]};return _8;},gridSourceCreator:function(_9,_a){var n=dojo.create("div"),_b=_9.store,_c=_b._items[_a],_d=_b.getLabelAttributes()[0],_e={node:n,data:fw.data.util.serializeStoreItem(_b,_c,["asset","name","subtype"]),subtype:[_b.getValue(_c,"subtype")],type:[_b.getValue(_c,"asset").type]};_e.data.type=_e.data.asset.type;_e.data.id=_e.data.asset.id;return _e;},getNormalizedData:function(_f,_10){var _11=_f.getItem(_10.id).data;if(_11.item&&_11.tree){return fw.data.util.serializeStoreItem(_11.tree.model.store,_11.item);}return _11;},destroyItem:function(_12,_13){_12.delItem(_13.id);dojo.destroy(_13);}};}
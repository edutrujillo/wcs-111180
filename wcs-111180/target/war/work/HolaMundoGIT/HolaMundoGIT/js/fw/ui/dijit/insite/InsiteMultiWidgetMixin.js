/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.insite.InsiteMultiWidgetMixin"]){dojo._hasResource["fw.ui.dijit.insite.InsiteMultiWidgetMixin"]=true;dojo.provide("fw.ui.dijit.insite.InsiteMultiWidgetMixin");dojo.require("fw.ui.ObjectFactory");dojo.require("fw.ui.dijit.insite.InsiteWidgetMixin");dojo.declare("fw.ui.dijit.insite.InsiteMultiWidgetMixin",fw.ui.dijit.insite.InsiteWidgetMixin,{init:function(_1){var _2=this.widgetArgs,_3=this.get("value");this._asset=_1.asset;this.assetManager=fw.ui.ObjectFactory.createManagerObject("asset");this._asset.set(_2.fieldName,_3||[]);this.set("disabled",true);}});}
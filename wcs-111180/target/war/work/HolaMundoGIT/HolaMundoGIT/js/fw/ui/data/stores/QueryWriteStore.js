/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.data.stores.QueryWriteStore"]){dojo._hasResource["fw.ui.data.stores.QueryWriteStore"]=true;dojo.provide("fw.ui.data.stores.QueryWriteStore");dojo.require("dojox.data.QueryReadStore");dojo.declare("fw.ui.data.stores.QueryWriteStore",dojox.data.QueryReadStore,{constructor:function(_1){this._features["dojo.data.api.Write"]=true;this._features["dojo.data.api.Notification"]=true;},referenceIntegrity:true,_assert:function(_2){if(!_2){throw new Error("assertion failed in ItemFileWriteStore");}},_getIdentifierAttribute:function(){var _3=this.getFeatures()["dojo.data.api.Identity"];return _3;},newItem:function(_4,_5){},setValues:function(_6,_7,_8){this._assertIsItem(_6);if(!dojo.isString(_7)){throw new Error(this._className+".getValue(): Invalid attribute, string expected!");}this._assert(typeof _8!=="undefined");var _9=this._getIdentifierAttribute();if(_7==_9){throw new Error("ItemFileWriteStore does not have support for changing the value of an item's identifier.");}return true;},});}
	/*
		CUSTOM CONFIGURATION FILE FOR CLARKII IMAGEEDITOR		
		
		*	User can provide their own customized toolbar with selected menu options from the superlist (clarkii_menu). 		
		*	By default, there are two modes, FORMS and INSITE. 
		*	Each mode provides options for user to either enable or disable menus in clarkii editor.
		*	Menus to be enabled or disabled must be provided as a list.
			
			*	The user can obtain the menu names from the superlist (i.e keys in the property list refer to the menu names).
			*	Only one list can be populated (i.e enabled or disabled).
			*	Clarkii editor will use the default menu (i.e default_menu), if no list is provided the user.
			*	Under a given mode, clarkii will use the enabled list when both the lists provided. 
			*	User can defined their own toolbar in the following format:
				var FOO_BAR = {
					enabled: [],
					disabled: []
				};
				The new toolbar name must be passed in the presentation object. 
	*/
	
	// List of default menu options for clarkii editor. 
	var clarkii_menu = 
	{
		File: "idfile",
		New:"idNewBtn",
		Open:"idOpenBtn",
		Save:"idSaveBtn",
		Local_Save: "idSaveLocallyBtn",
		Restart: "idRestartBtn",
		Wordpress_Save: "idWPSaveBtn",
		Edit:"idedit",
		Undo:"idUndoBtn",
		Redo:"idRedoBtn",
		Cut:"idCutBtn",
		Copy:"idCopyBtn",
		Paste:"idPasteBtn",
		Delete:"idDeleteBtn",
		View:"idview",
		Grid:"idGridSld",
		Snap_to_grid:"idSnapChb",
		Show_Toolbox:"idShowToolboxBtn",
		Modify:"idmodify",
		Rotate:"idRotateSld",
		Opasity:"idOpasitySld",
		Brightness:"idBrightnessSld",
		Contrast:"idContrastSld",
		Grayscale:"idGrayscaleSld",
		Hue:"idHueSld",
		Saturation:"idSaturationSld",
		Mirror:"idMirrorCb",
		Flip:"idFlipCb",
		Crop:"idCropBtn",
		Resample:"idResampleBtn",
		Reset_modifications:"idResetModificationsBtn",
		Filter:"idfilter",
		Emboss:"idEmbossSld",
		Blur:"idBlurSld",
		Sharpen:"idSharpenSld",
		Negative:"idNegativeSld",
		Mosaic:"idMosaicSld",
		Pencil_sketch:"idPencilSketchSld",
		Red_Tint:"idRedTintSld",
		Green_Tint:"idGreenTintSld",
		Blue_Tint:"idBlueTintSld",
		Sepia_Tint:"idSepiaTintSld",
		Red_Eye_Remover:"idRedEyeRemover",
		Lens_Flare:"idLensFlare",
		Flood_Fill:"idFloodFill",
		Reset_filters:"idResetFiltersBtn",
		Insert:"idinsert", 
		Image:"idImageBtn",
		Rectangle:"idRectangleBtn",
		Ellipse:"idEllipseBtn",
		Line:"idLineBtn",
		Curve:"idCurveBtn",
		Text:"idTextBtn",
		Text_Alon_Path:"idTextAlonPathBtn",
		Drawing:"idDrawingBtn",
		Pages:"idpages",
		Layers:"idlayers",
		Properties:"idproperties",
		Help:"idhelp",
		Full_screen:"idFullScreen",
		Workbench_Menu:"idMenuWorkbench",					
		Undo:"idUndoBtn",
		Redo:"idRedoBtn",
		Show_Toolbox:"idShowToolboxBtn",		
		Remove_all_cursors:"idArrowBtn",
		Hand_pan:"idHandBtn",
		Zoomer:"idZoomer",
		Original_Size:"idSizeOriginalBtn",
		Fit_To_Canvas:"idFitToCanvasBtn",					
		Left_panel:"idLeftPanel",
		Right_panel:"idRightPanel"
	}
	
	// Default toolbar for clarkii editor.
	var default_menu = ['File','New','Open','Modify','Rotate','Crop','Resample','Full_screen'];								
	
	// Customized toolbar for Insite mode. 
	var CS_INSITE = {
		enabled: ['File','New','Open','Modify','Rotate','Crop','Resample','Full_screen'],
		disabled: []
	};
	
	// Customized toolbar for forms.
	var CS_FORM = {
		enabled: ['File','New','Open','Modify','Rotate','Crop','Resample','Full_screen'],
		disabled: []
	};
		

/*
This file contains patches to core dojo functionality, as required by our UIs.

Note that due to the nature of some of these fixes, this file should be included
AFTER all dojo.requires are performed.

TODO: consider working this into the dojo build; for now i'm keeping it
separate to prevent potential of issues if it were to be shrinksafe'd.
--KGF
*/

(function(){
var d = dojo; //make d alias inside patched functions work

if (dojo.isIE) {
	//=== PR 21255-related patches ===
	//prevent infinite loops in IE if dojo.destroy fails.
	
	//dependency for dojo.destroy, exists inside closure in dojo/html.js
	var _destroyContainer = null;
	d.addOnWindowUnload(function(){
		_destroyContainer = null; //prevent IE leak
	});
	
	//Modified version of dojo.destroy:
	//Updates the code to 1.4 to reduce errors in IE, and adds boolean returns
	//as indication that an error occurred, since they are otherwise squelched.
	dojo._destroyElement = dojo.destroy = function(node) {
		node = d.byId(node);
		try{
			var doc = node.ownerDocument;
			// cannot use _destroyContainer.ownerDocument since this can throw an exception on IE
			if(!_destroyContainer || _destroyDoc != doc){
				_destroyContainer = doc.createElement("div");
				_destroyDoc = doc;
			}
			_destroyContainer.appendChild(node.parentNode ? node.parentNode.removeChild(node) : node);
			// NOTE: see http://trac.dojotoolkit.org/ticket/2931. This may be a bug and not a feature
			_destroyContainer.innerHTML = "";
			return true;
		}catch(e){
			/* squelch */
			return false;
		}
	};
	
	//Modified version of IE-specific dojo.empty:
	//Checks return value of modified dojo.destroy and bails out of loop
	//(at the possible expense of some memory leakage / leftover nodes)
	dojo.empty = function(node) {
		node = d.byId(node);
		for(var c; c = node.lastChild;){ // intentional assignment
			if (!d.destroy(c)) break;
		}
	};
	
	//wipe out function added to unload handlers for "memory leak fixing"
	//possible TODO: replace it with one that tries/catches to avoid
	//exceptions being thrown when it's run in iframes.
	//(Original dojo bug regarding this handler: #3531)
	dijit._destroyAll = function() {};
	//=== end PR 21255 fixes ===
	
}
})();

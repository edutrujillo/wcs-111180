<%@page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
	String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
	
	String cacheName = request.getParameter("cachename");
	cacheName = StringEscapeUtils.escapeHtml(cacheName);
	if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
	    cacheName = CacheUtil.SS_CACHE;
    Boolean cscacheChecked = cacheName.equals(CacheUtil.CS_CACHE);
    Boolean sscacheChecked = cacheName.equals(CacheUtil.SS_CACHE);
    String onCS = request.getParameter("onCS");
	onCS = StringEscapeUtils.escapeHtml(onCS);
    boolean isOnCS = Boolean.parseBoolean(onCS);
%>
<html>
<head>
<LINK
	href='<%=request.getContextPath()%>/Xcelerate/data/css/<%=session.getAttribute("locale")%>/cacheTool.css'
	rel="stylesheet" type="text/css">
</head>

<body>
<ul class="menu-text">
	<li><a class="no-underline"
		href="./summary.jsp?cachename=<%=cacheName%>" target=content>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="sidebar.summary"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</a></li>
	<li><a class="no-underline"
		href="./page.jsp?cachename=<%=cacheName%>" target=content>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="sidebar.page"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</a></li>
	<li><a class="no-underline"
		href="./dep.jsp?cachename=<%=cacheName%>" target=content>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="sidebar.dependency"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</a></li>
	<% if(isOnCS) {%>
	<li><a class="no-underline"
		href="./assetCache.jsp?cachename=<%=cacheName%>" target=content>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="sidebar.assetCache"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</a></li>
	<% }%>
	<li><a class="no-underline"
		href="./urlCache.jsp?cachename=<%=cacheName%>" target=content>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="sidebar.urlCache"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</a></li>
	<li><a class="no-underline"
		href="./clusterInfo.jsp?cachename=<%=cacheName%>" target=content>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="sidebar.clusterInfo"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</a></li>
</ul>
<br />
<form action="tool.jsp" target="_top">
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<input type="hidden"
	name="onCS" value='<%=onCS%>' /> <input
	type="radio" name="cachename" value='<%=CacheUtil.CS_CACHE %>'
	<%=isOnCS ? cscacheChecked ? "checked" : "" : "disabled"%> />
    <jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="sidebar.csCache"/>
		<jsp:param name="render" value="true"/>
	</jsp:include><br />
<input type="radio" name="cachename" value='<%=CacheUtil.SS_CACHE %>'
	<%=sscacheChecked ? "checked" : ""%> />
	 <jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="sidebar.ssCache"/>
		<jsp:param name="render" value="true"/>
	</jsp:include><br />
<input type="submit" value="Choose cache"
	<%=isOnCS ? "" : "disabled"%> />
</form>
</body>
</html>
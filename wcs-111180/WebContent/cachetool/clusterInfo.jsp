<%@page import="java.util.ArrayList"%>
<%@page import="net.sf.ehcache.distribution.CachePeer"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.Arrays"%>
<%@page import="net.sf.ehcache.distribution.CacheManagerPeerProvider"%>
<%@page import="net.sf.ehcache.Ehcache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>

<%
   
    String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
	String authorize =
        (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
        String contextPath = request.getContextPath();
%>
<html>
<head>
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css'
	rel="stylesheet" type="text/css" />
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css'
	rel="stylesheet" type="text/css"/ >
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css'
	rel="stylesheet" type="text/css"/ >
</head>
<body>
<%
    String[] rowColors =
            { "tile-row-normal", "tile-row-highlight" };
        String cacheName = request.getParameter("cachename");
        String[] ar = { CacheUtil.CS_CACHE,CacheUtil.SS_CACHE,CacheUtil.LINKED_CACHE,CacheUtil.CAS_CACHE};
        if(!Arrays.asList(ar).contains(cacheName))
        	cacheName = CacheUtil.SS_CACHE;
        
        CacheManager cacheMgr = CacheUtil.getCacheManager(cacheName);
        
        if(null == cacheMgr)
        {%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="assetCacheDep.noCacheManager"/>
				<jsp:param name="render" value="true"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
			return;
        }

        Ehcache cache = cacheMgr.getCache("notifier");
        CacheManagerPeerProvider provider =
            cacheMgr.getCacheManagerPeerProvider("RMI");
        List<CachePeer> remotePeerList =
            provider.listRemoteCachePeers(cache);
        List<CachePeer> accessibleRemotePeerlist = new ArrayList<CachePeer>();
        List<String> errorMachines = new ArrayList<String>();
        for(CachePeer peer : remotePeerList)
        {
            try 
            {
	            peer.getUrl();
	            accessibleRemotePeerlist.add(peer);
            }
            catch(Exception e) {
                //if we are not able to reach the peer
                // we do not add it.
                errorMachines.add(peer.toString());
            }
        }
        if(accessibleRemotePeerlist.size() == 0)
        {
%>
<table>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="clusterInfo.noRemoteCluster"/>
			<jsp:param name="render" value="true"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		</td>
	</tr>
</table>
<%
        }else{
%>
<table border="0" bgcolor="#ffffff">
	<tr>
		<td height="3px"></td>
	</tr>
	<tr>
		<td class="sub-title-text">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="clusterInfo.clusterInfo"/>
			<jsp:param name="render" value="true"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		</td>
	</tr>
	<tr>
		<td height="7px"></td>
	</tr>
	<tr>
		<td class="padded">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="clusterInfo.noOfRemoteClusterPeer"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		<%=((String)request.getAttribute("clusterInfo.noOfRemoteClusterPeer")).replace("{0}","<b>"+Integer.toString(accessibleRemotePeerlist.size())+"</b>")%>
		</td>
	</tr>
	<tr>
		<td>

		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td></td>
				<td height="1" valign="TOP" class="tile-dark"><img height="1"
					width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
			<tr>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
				<td>
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td colspan="10" class="tile-highlight"><img
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
							width="1" height="1"></td>
					</tr>
					<tr>
						<td class="tile-a"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="clusterInfo.tblTitle.name"/>
							<jsp:param name="encode" value="true"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="clusterInfo.tblTitle.url"/>
							<jsp:param name="encode" value="true"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="clusterInfo.tblTitle.port"/>
							<jsp:param name="encode" value="true"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="clusterInfo.tblTitle.guid"/>
							<jsp:param name="encode" value="true"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-c"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
					</tr>
					<%
					    int i = 0;
					        for(CachePeer peer : accessibleRemotePeerlist)
					        {
					            String urlbase = peer.getUrlBase();
					            String name = peer.getName();
					            String guid = peer.getGuid();
					%>
					<tr class='<%=rowColors[i++ % 2]%>'>
						<td valign="TOP"><br />
						</td>
						<td valign="TOP">
						<div class="small-text-inset"><%=name%></div>
						</td>
						<td><br />
						</td>
						<td valign="TOP">
						<div class="small-text-inset"><%=urlbase.substring(urlbase.indexOf("//") + 2,
                        urlbase.indexOf(":"))%></div>
						</td>
						<td><br />
						</td>
						<td valign="TOP">
						<div class="small-text-inset"><%=urlbase.substring(urlbase.indexOf(":") + 1)%></div>
						</td>
						<td><br />
						</td>
						<td><br />
						</td>
						<td valign="TOP">
						<div class="small-text-inset"><%=guid%></div>
						</td>
						<td><br />
						</td>
					</tr>
					<%}%>
					
				</table>
				</td>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
			</tr>
			<tr>
				<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
					height="1" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
			</tr>
			<tr>
				<td></td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'><img
					height="5" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%
if(!errorMachines.isEmpty()) {%>
<br/><br/>
<h3>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="clusterInfo.errorMachines"/>
	<jsp:param name="encode" value="true"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
</h3>
<%
for(String machine : errorMachines)
    out.println(machine +"\n\n" );
}
%>
</body>
</html>
<%}
    }
    else
    {
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="encode" value="true"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
    }
%>
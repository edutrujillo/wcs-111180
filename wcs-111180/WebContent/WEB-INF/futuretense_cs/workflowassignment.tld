<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>workflowassignment</shortname>
	<info>A tag library for Content Centre Workflow Assignment methods.</info>

	<tag>
		<name>getid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getid</tagclass>
		<info>
			Gets the id of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getassigndate</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getassigndate</tagclass>
		<info>
			Gets the date when the workflow assignment was assigned. 
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getassignedobject</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getassignedobject</tagclass>
		<info>
			Gets the workflow asset object of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getassigneduserid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getassigneduserid</tagclass>
		<info>
			Gets the id of user who has been assigned to this workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

    <tag>
		<name>getassigneduserrole</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getassigneduserrole</tagclass>
		<info>
			Gets the role of user who has been assigned to this workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

    <tag>
		<name>getassigneruserid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getassigneruserid</tagclass>
		<info>
			Gets the id of user who assigned this workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getassignmentcomment</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getassignmentcomment</tagclass>
		<info>
			Gets the comment of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdeadline</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getdeadline</tagclass>
		<info>
			Gets the deadline of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdisposition</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getdisposition</tagclass>
		<info>
			Gets the disposition of the assignment and classifies it as cancelled, completed, delegated, etc.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdispositioncomment</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getdispositioncomment</tagclass>
		<info>
			Gets the disposition comment of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdispositiondate</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getdispositiondate</tagclass>
		<info>
			Gets the disposition date of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdispositionuserid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getdispositionuserid</tagclass>
		<info>
			Gets the id of the user who resolved the workflow assignment. 
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getgroupedcomment</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getgroupedcomment</tagclass>
		<info>
			Gets the group comment of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getgroupeddeadline</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getgroupeddeadline</tagclass>
		<info>
			Gets the group deadline of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getgroupid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getgroupid</tagclass>
		<info>
			Gets the group id of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getgroupedworkflowstepid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getgroupedworkflowstepid</tagclass>
		<info>
			Gets the step id of the group for the workflow assignment. 
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getqueuedworkflowstepid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getqueuedworkflowstepid</tagclass>
		<info>
			Get the step id of the queued workflow step for the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getstateid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getstateid</tagclass>
		<info>
			Gets the state id of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getstatus</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getstatus</tagclass>
		<info>
			Gets the status of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>


	<tag>
		<name>getprocessstatus</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getprocessstatus</tagclass>
		<info>
			Gets the process status of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getprocessdeadline</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getprocessdeadline</tagclass>
		<info>
			Gets the process deadline of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getprocess</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowassignment.Getprocess</tagclass>
		<info>
			Gets the workflow process of the workflow assignment.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

</taglib>

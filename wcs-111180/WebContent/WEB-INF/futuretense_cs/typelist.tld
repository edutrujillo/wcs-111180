<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>typelist</shortname>
	<info>A tag library for Content Centre typelist methods.</info>
	<tag>
		<name>scatter</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Scatter</tagclass>
		<info>
			Retrieves all fields of a loaded typelist object and "scatters" their values into individual variables sharing a common prefix.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gather</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Gather</tagclass>
		<info>
			Retrieves name/value pairs from the environment, and sets them into a loaded typelist object. The name/value pairs are grouped together using a prefix and a prefix separator.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>clearassettypes</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Clearassettypes</tagclass>
		<info>
			Clears the type list of all asset types.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getallassettypes</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Getallassettypes</tagclass>
		<info>
			Gets all asset types.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>addassettype</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Addassettype</tagclass>
		<info>
			Adds an asset type to the typelist object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>deleteassettype</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Deleteassettype</tagclass>
		<info>
			Deletes an asset type from the typelist object.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>hastype</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Hastype</tagclass>
		<info>
			Checks whether the typelist object contains the specified type.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>isall</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Isall</tagclass>
		<info>
			Verifies whether the type list is set to be "any" type.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setall</name>
		<tagclass>com.openmarket.xcelerate.jsp.typelist.Setall</tagclass>
		<info>
			Sets the type list to indicate "any" type.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>


</taglib>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>sitelist</shortname>
	<info>A tag library for Content Centre Sitelist methods.</info>
	<tag>
		<name>scatter</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Scatter</tagclass>
		<info>
			Retrieves all fields of a loaded object and "scatters" their values into individual variables sharing a common prefix.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gather</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Gather</tagclass>
		<info>
			Retrieves name/value pairs from the environment, and sets them into a loaded object. The name/value pairs are grouped together using a prefix and a prefix separator.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>clear</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Clear</tagclass>
		<info>
			Clears the site list.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getall</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Getall</tagclass>
		<info>
			Gets the list of sites as a comma-separated string.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>add</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Add</tagclass>
		<info>
			Adds a site to the list.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>pubid</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>delete</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Delete</tagclass>
		<info>
			Deletes a site from the list.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>pubid</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>hassite</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Hassite</tagclass>
		<info>
			Verifies whether a site is present in site list.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>pubid</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>isall</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Isall</tagclass>
		<info>
			Verifies whether the site list is set to be "any" site.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setall</name>
		<tagclass>com.openmarket.xcelerate.jsp.sitelist.Setall</tagclass>
		<info>
			Sets the site list to indicate "any" site.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

</taglib>

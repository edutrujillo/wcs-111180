<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>workflowgroup</shortname>
	<info>A tag library for Content Centre Workflow Group methods.</info>
	<tag>
		<name>scatter</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Scatter</tagclass>
		<info>
			Retrieves all fields of a loaded workflow group object and "scatters" their values into individual variables sharing a common prefix.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gather</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Gather</tagclass>
		<info>
			Retrieves name/value pairs from the environment, and sets them into a loaded workflow group object. The name/value pairs are grouped together using a prefix and a prefix separator.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getid</tagclass>
		<info>
			Gets the id of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getname</tagclass>
		<info>
			Gets the name of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setname</tagclass>
		<info>
			Sets the name of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getdescription</tagclass>
		<info>
			Gets the description of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setdescription</tagclass>
		<info>
			Sets the description of workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getworkflowprocessid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getworkflowprocessid</tagclass>
		<info>
			Gets the workflow process id for the specified workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setworkflowprocessid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setworkflowprocessid</tagclass>
		<info>
			Sets the workflow process id for the specified workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getabsdeadline</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getabsdeadline</tagclass>
		<info>
			Gets the absolute deadline of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setabsdeadline</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setabsdeadline</tagclass>
		<info>
			Sets the absolute deadline of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>absdeadline</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>geteditroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Geteditroles</tagclass>
		<info>
			Gets the roles that can edit or delete a group for the specified workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>seteditroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Seteditroles</tagclass>
		<info>
			Sets the roles that can edit or delete a group for the specified workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdeleteroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getdeleteroles</tagclass>
		<info>
			Gets the roles that can add or remove a group from the specified workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdeleteroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setdeleteroles</tagclass>
		<info>
			Sets the roles that can add or remove a group from the specified workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getsites</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getsites</tagclass>
		<info>
			Gets the sites for the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setsites</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setsites</tagclass>
		<info>
			Sets the sites for the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getgroupactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getgroupactions</tagclass>
		<info>
			Gets the group deadlock actions of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setgroupactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setgroupactions</tagclass>
		<info>
			Sets the group actions of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getparticipants</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Getparticipants</tagclass>
		<info>
			Gets the participants of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setparticipants</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowgroup.Setparticipants</tagclass>
		<info>
			Sets the participants of the workflow group.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

</taglib>

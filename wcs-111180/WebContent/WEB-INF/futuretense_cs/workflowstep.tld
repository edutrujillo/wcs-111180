<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>workflowstep</shortname>
	<info>A tag library for Content Centre workflow step methods.</info>
	<tag>
		<name>scatter</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Scatter</tagclass>
		<info>
			Retrieves all fields of a loaded workflow step object and "scatters" their values into individual variables sharing a common prefix.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gather</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Gather</tagclass>
		<info>
			Retrieves name/value pairs from the environment, and sets them into a loaded workflow step object. The name/value pairs are grouped together using a prefix and a prefix separator.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getid</tagclass>
		<info>
			Gets the id of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getname</tagclass>
		<info>
			Gets the name of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setname</tagclass>
		<info>
			Sets the name of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdeadlinetype</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getdeadlinetype</tagclass>
		<info>
			Gets the deadline type of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdeadlinetype</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setdeadlinetype</tagclass>
		<info>
			Sets the deadline type for the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>type</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getstartstate</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getstartstate</tagclass>
		<info>
			Gets the starting state of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setstartstate</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setstartstate</tagclass>
		<info>
			Sets the starting state of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getendstate</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getendstate</tagclass>
		<info>
			Gets the ending state of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setendstate</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setendstate</tagclass>
		<info>
			Sets the ending state of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getoverrideflag</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getoverrideflag</tagclass>
		<info>
			Gets the override flag of the workflow step. Flag is for voting.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setoverrideflag</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setoverrideflag</tagclass>
		<info>
			Sets the override flag of the workflow step. Flag is for voting.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>flag</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getgrouplockflag</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getgrouplockflag</tagclass>
		<info>
			Gets the group lock flag of the workflow step. Flag is for workflow group synchronization.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setgrouplockflag</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setgrouplockflag</tagclass>
		<info>
			Sets the group lock flag of the workflow step. Flag is for workflow group synchronization.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>flag</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getsteptype</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getsteptype</tagclass>
		<info>
			Gets the type of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setsteptype</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setsteptype</tagclass>
		<info>
			Sets the type of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>type</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getconditions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getconditions</tagclass>
		<info>
			Gets the conditions of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setconditions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setconditions</tagclass>
		<info>
			Sets the conditions of the workflow step. 
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getcompletionactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getcompletionactions</tagclass>
		<info>
			Gets the completion actions of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setcompletionactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setcompletionactions</tagclass>
		<info>
			Sets the completion actions of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getauthorizationroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getauthorizationroles</tagclass>
		<info>
			Gets the authorization roles of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setauthorizationroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setauthorizationroles</tagclass>
		<info>
			Sets the authorization roles of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getnotifyroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getnotifyroles</tagclass>
		<info>
			Gets the roles that are notified when the workflow step is taken.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setnotifyroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setnotifyroles</tagclass>
		<info>
			Sets the roles that are notified when the workflow step is taken.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdeadlockactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Getdeadlockactions</tagclass>
		<info>
			Gets the deadlock actions of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdeadlockactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowstep.Setdeadlockactions</tagclass>
		<info>
			Sets the deadlock actions of the workflow step.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

</taglib>

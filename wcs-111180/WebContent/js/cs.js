var MAIN_SUBVIEW = "contentPaneMainBodySubView:";  //MAIN PANEL SUBVIEW
var LEFT_NAV_SUBVIEW = "leftNav:";  //LEFT TREES  PANEL SUBVIEW
var ASSET_TREE_PULLDOWN_MENU = "AssetDropDownMenu" ;
var ASSET_TREE_CASCADED_PULLDOWN_MENU = "CascadedMenu" ;

var NO_MENU_DISPLAY_REPLY = "000000:noshow@9999999" ;
var waitMessage = "Please wait...";

var CLSID_OIE        = "clsid:EC59EE59-B27D-4581-9E93-6AB6CB88E970";  //OIE Object ID
var PREVIEW_SATELLITE = "&SATELLITE=true&pagename=OpenMarket%2FXcelerate%2FUIFramework%2FShowPreviewFrames&AssetType="  ; 

var isIE = document.all;

var  assetNEW ;
var  crntMenuOn ;

var  notMapped = 0 ;
var  casBottom = "0px";
var  casTop = "0px";
var  isClientUrlSet = false;
var  isImageEmpty = false;
var imageObjects=[];
var oieObjects = [];
var count=0;
var oiePrefix="oie_";
var editorType = "oie3";
var isSubmited = false;
var submitButtonId;
// global constants used for clarkii customization
var buttons="New,Open,Save,SaveLocally,Restart,WPSave,Undo,Redo,Cut,Copy,Paste,Delete,Crop,ResetModifications,ResetFilters,Image,Rectangle,Ellipse,Line,Curve,Text,TextAlonPath,Drawing,ToggleLeftPanel,ShowToolbox,Arrow,Hand,SizeOriginal,FitToCanvas,ToggleRightPanel,";
var sliders="Grid,Rotate,Opasity,Brightness,Contrast,Grayscale,Hue,Saturation,Emboss,Blur,Sharpen,Negative,Mosaic,PencilSketch,RedTint,GreenTint,BlueTint,SepiaTint,";
var checkboxes="Mirror,Flip,Snap,";
var others="MenuMain,file,edit,view,modify,filter,insert,pages,layers,properties,help,FullScreen,MenuWorkbench,Zoomer,LeftPanel,RightPanel,SnapChb,";
	

/****************************************************************************************
 * Created : June 06, 2010
 * Reason:   If Search results Popup has been mapped and siaplayed it
             Resizes serachpopup to wrap the serach results table
             Utility to  display and shpw a Div Container, Menu, etc
             If we did not do this,div will not show the dropdown
             elements from the main page when dragged over.
             This problem is just in Firefox 

  Modified: June 03, 2010
  Reason:   Set popupsearch flag to true preventing the refresh popup function
            to unmap and/or hide the popup
 ***************************************************************************************/
function resizeSearchPopup()
{
	var div = document.getElementById("searchpopupdiv");
	if(div && div.style.display == 'block')
	{
		var table = div.childNodes[0];
		if(table)
		{
			var tableHeight = table.offsetHeight;
   		    var tableWidth = table.offsetWidth;
            // Set Max for Scroll Bar
            var popupOffsetHeight = 400;

            if(isIE)
			{
		        popupOffsetHeight = 388;
			}
		   	if(tableHeight > popupOffsetHeight)
			{
				div.style.height="400px";
				if(isIE)
				{
					tableWidth = tableWidth + 15;
				}
				div.style.width= tableWidth + 16+"px";
				div.style.overflow ="auto";

			} else
			{
				if(isIE)
				{
					tableHeight = tableHeight + 14;
					tableWidth = tableWidth + 12;
				}
			    div.style.height= tableHeight +"px";
				div.style.width= tableWidth +"px";
				div.style.overflow ="hidden";

			}
			var theiFrame= document.getElementById('myiFrame');
			theiFrame.style.height = div.offsetHeight + 2;
		    theiFrame.style.width = div.offsetWidth + 2;

            showsearchpopup = "true";
        }
	}
}

/*****************************************************************************************
 *  This method is called by FCKeditorRender
 *  to create FCKEditor in dash UI on click of the
    attribute text box

    Modified: Oct 16, 2009
    The last parameter contains the FCKEditor Asset Pick From Tree PopUp Attributes
    Note:  The FCKEditorRendere Crafts the following Attributes/Properties
           List of arguments dilimited by the '%' sign... the index ordering is as follows
           As of Now We are expecting Four Attributes
    // FckArg(0)  showAssetButtonId Simulate a Submit Request to the server _adfspu(..);
    // FckArg(1)  linkAssetButtonId link Button Id To set the Button source
    // FckArg(2)  Source to the link Asset Image
    // FckArg(3)  Source to the Include Asset Image

 ***********************************************************************************/
function createWebEditor( FCKid,
                          basepath,
                          configPath,
                          height,
                          width,
                          toolBarSet,
                          assetType,
                          showSiteTree,
                          enableAddLink,
                          isDash,
                          assetName,
                          assetId,
                          attribDesc ,
                          locale,
                          rendererURL,
                          CSSitePath, allowedAssetTypes, scriptsAllowed, assettypename, attributename, attributetypename,
                          FckPopUpArgs)
{
    var oFCKeditor = new FCKeditor(FCKid) ;

    //console.log("<in> cs.js::createWebEditor Legacy.... () configPath:"+configPath+", FCKInstance["+FCKid+"]")   ;

    oFCKeditor.Config['CustomConfigurationsPath'] = configPath;
    oFCKeditor.Config['assetType'] = assetType;
    oFCKeditor.Config['AutoDetectLanguage'] = "false";
    oFCKeditor.Config['DefaultLanguage'] = locale;
    oFCKeditor.Config['assetId'] = assetId;
	oFCKeditor.Config['fieldName'] = FCKid;
	oFCKeditor.Config['fieldDesc'] = attribDesc;
	oFCKeditor.Config['showSiteTree'] = showSiteTree;
	oFCKeditor.Config['enableEmbeddedLinks'] = enableAddLink;
	oFCKeditor.Config['isDash'] = isDash;
	oFCKeditor.Config['assetName'] = assetName;
	oFCKeditor.Config['CSSitePath'] = CSSitePath;
	oFCKeditor.Config['urlFCKEditorRenderer'] = rendererURL;

    var params = FckPopUpArgs.split("%");
    //  Preview URL  Template  comes Encoded in two parts.
    if ( params.length == 5 )
    {
        oFCKeditor.Config['showAssetButtonId'] = params[0];
        oFCKeditor.Config['linkAssetButtonId'] = params[1];
        oFCKeditor.Config['linkButtonSrc'] = params[2];
        oFCKeditor.Config['includeButtnSrc'] = params[3];
    }

    if(allowedAssetTypes && allowedAssetTypes.length > 0){
		oFCKeditor.Config['allowedassettypes'] = allowedAssetTypes;
	}
	if(scriptsAllowed && scriptsAllowed.length > 0){
		oFCKeditor.Config['script'] = scriptsAllowed;
	}
	if((assettypename && assettypename.length > 0) && (attributename && attributename.length > 0) && (attributetypename && attributetypename.length > 0)){
		oFCKeditor.Config['ip_assettypename'] = assettypename;
		oFCKeditor.Config['ip_attributename'] = attributename;
		oFCKeditor.Config['ip_attributetypename'] = attributetypename;
	}
    oFCKeditor.BasePath = basepath;

    if( toolBarSet != null )
    {
        oFCKeditor.ToolbarSet = toolBarSet;
    }

    if( height != null )
    {
        oFCKeditor.Height = height+100;
    }

    if( width != null )
    {
        oFCKeditor.Width = width;
    }

    oFCKeditor.ReplaceTextarea();

    //console.log(" Created Instance new FCKeditor = oFCKeditor ["+oFCKeditor.Height+","+oFCKeditor.Width+"]")   ;

}














/**********************************************************************
  Function: bulkEditCkNVLClickCB
  Reason:   CheckBox NULL Control for Bulk Edit Asset Template
**********************************************************************/
function bulkEditCkNVLIconClickCB ( ckboxUI, labelUI, ckimageUI,
                                onlabelText, offLabelText,
                                onImageSrc,offImageSrc )
{
    var checkBoxComp = document.getElementById(MAIN_SUBVIEW+ckboxUI);
    var labelUIComp = document.getElementById(MAIN_SUBVIEW+labelUI);
    var imageUIComp = document.getElementById(MAIN_SUBVIEW+ckimageUI);

    if ( checkBoxComp.checked == true )
    {
       imageUIComp.src = onImageSrc ;
       labelUIComp.innerHTML = onlabelText   ;
    }
    else
    {
       imageUIComp.src = offImageSrc ; 
       labelUIComp.innerHTML =  offLabelText      ;
    }
}

/**********************************************************************
  Function: bulkEditCkNVLClickCB
  Reason:   CheckBox NULL Control for Bulk Edit Asset Template
**********************************************************************/
function bulkEditCkNVLTextClickCB ( ckboxUI, labelUI,
                                    onlabelText, offLabelText )
{
    var checkBoxComp = document.getElementById(MAIN_SUBVIEW+ckboxUI);
    var labelUIComp = document.getElementById(MAIN_SUBVIEW+labelUI);

    if ( checkBoxComp.checked == true )
    {
       labelUIComp.innerHTML = onlabelText   ;
    }
    else
    {
       labelUIComp.innerHTML =  offLabelText      ;
    }
}



function setPositionForDragAndDropFCK()
{
	sourceX=631;
	sourceY=190;
}


//  On Success, process the reply string received from
//  the server containg the asset permission
//  Reply Message Payload has been formated as follows
//  10100:startItemName(0)@StartIemId(0):startItemName(1)@StartIemId(1):....startItemName(n)@StartIemId(n)
//   Where n = Number of StartMenu Items found for this Asset..
function getFCKPopUpReplyStatus(req)
{
    var tout = req.responseText ;

    var params = tout.split("@");
    //  Preview URL  Template  comes Encoded in two parts.
    if ( params.length == 3 )
    {
        var popupid = params[0] ;
        var searchlink =   params[1];
        var searchfield =  params[2] ;
        // Preview Asset with asset URL
        showSearchPopUp(popupid,searchlink,searchfield);
    }
}


var fckEditorembedtype ;

// FCKEditor_IncludeOrLinkAsset  that handles add asset link/include asset actions in  fckeditor
// PopUp has created a Show Assets Pick From Tree / Table
// Need to do the following
// 1)  Populate the CoreTable ( UISearchResults )  Filtered AssetType   ( Go To Server )
// 2)  That will reload
// 3)  PPR will display the table
// 4)  Set the Button To The Type of Link
// 5)  FCKCB
// 6)  Set The Command Button:   create link asset  path: src= ../../images/en_us/linkasset.gif
// 6)  Set The Command Button:   include asset  path: src= ../../images/en_us/include.gif
//function FCKEditor_IncludeOrLinkAsset (assetname, assetId, assetType, fieldname, fielddesc, embedtype, isFckEditor)
function FCKEditor_IncludeOrLinkAsset ( assetType )
{
     var  embedtype = fckEditorembedtype ; 

     //alert("<in> cs.js::FCKEditor_IncludeOrLinkAsset pURL:"+embedtype)  ;
    //return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=700,location=no,menubar=no,resizable=yes,toolbar=no,top=50,width=250,height=" + height);

     return true ; 
}



var linkAssetButtonId ;
var linkImageSrc ;
var cannotIncludeSelfMsg  ;
var cannotLinkOrIncludeThisAsset ;
var FCKselectedHTML  ;


/*************************************************************************************************
                           showAssetsToFCKEditor
 Reason:  User has Clicked on FCK Toolbar Action Command Button:
          The Two possible commands that will now launch and dsiplay the
          Pick From Tree Search and Display Asset:
          FCKTollbar Buttons to launcjh PopUp include:
                    1)   Add Asset Link
                    2)   Include Asset

  Modified: Oct 16, 2009
    The last parameter contains the FCKEditor Asset Pick From Tree PopUp Attributes
    Note:  The FCKEditorRendere Crafts the following Attributes/Properties
           List of arguments dilimited by the '%' sign... the index ordering is as follows
           As of Now We are expecting Four Attributes

           FckArg(0)  showAssetButtonId Simulate a Submit Request to the server _adfspu(..);
           FckArg(1)  linkAssetButtonId link Button Id To set the Button source
           FckArg(2)  Source Image to the Operation Tpyefor Show and select Asset Asset Image
                                         proc_a
****************************************************************************************************/

function showAssetsToFCKEditor( showAssetButtonId,
                                linkAssetButtonIdFCK,
                                linkImageSrcFCK,
                                cannotIncludeSelf,
                                CannotLinkOrIncludeThisAsset,
                                selectedHTML )
{
        // Perserve the parameters will be used in display PopUp Utility functions
        linkAssetButtonId = linkAssetButtonIdFCK   ;
        linkImageSrc = linkImageSrcFCK ;
        cannotIncludeSelfMsg  = cannotIncludeSelf ;
        cannotLinkOrIncludeThisAsset = CannotLinkOrIncludeThisAsset ;

        FCKselectedHTML = selectedHTML ;

        // get the Link ASSET Button component id
        var linkButton = document.getElementById(linkAssetButtonId);
        // Lets set the PopUp Selected Asset to Correct Source image
        if ( null != linkButton )
        {
           linkButton.src = linkImageSrc ;
        }
        //alert("showAssetsToFCKEditor@"+showAssetButtonId) ;
    
        _adfspu('mainForm', 1, 0, showAssetButtonId);
}


/*************************************************************************************************
                           showAssetsToFCKEditor
 Reason:  User has Clicked on FCK Toolbar Action Command Button:
          The Two possible commands that will now launch and dsiplay the
          Pick From Tree Search and Display Asset:
          FCKTollbar Buttons to launcjh PopUp include:
                    1)   Add Asset Link
                    2)   Include Asset

  Modified: Oct 16, 2009
    The last parameter contains the FCKEditor Asset Pick From Tree PopUp Attributes
    Note:  The FCKEditorRendere Crafts the following Attributes/Properties
           List of arguments dilimited by the '%' sign... the index ordering is as follows
           As of Now We are expecting Four Attributes

           FckArg(0)  showAssetButtonId Simulate a Submit Request to the server _adfspu(..);
           FckArg(1)  linkAssetButtonId link Button Id To set the Button source
           FckArg(2)  Source Image to the Operation Tpyefor Show and select Asset Asset Image

****************************************************************************************************/


/***************************************************************************************************
                                      addFCKEmbeddedAssetLink
 *  Created: Oct 20, 2009
    Reason:  User has a selected Asset and has clicked on Link Asset
             From Pick from Tree PopUp Window.

            Use the Passed in  Parameters:
                1)     assetname            - The name of the Asset update / create
                2)     fieldname            - The Attribute Editor Component ID
                3)     fielddesc            - The Attribute Editor Field Description
                4)     tn_allowedassettypes - The Allowable Asset Types.. List ??
                5)     tn_assetId           - The id of the selected asset
                6)     tn_assetType         - The asset type  of the selected asset
                7)     assetType            - The Asset Type of the asset being updated / edited

   CRAFT THE URL from the following Passed In Info to launch select template Window
   and include the asset into the FCKeditor.


   Modified:  Call AddInclusionCK.xml  CKEditor Object to use not FCKeditor
******************************************************************************************************/
function  addFCKEmbeddedAssetLink ( assetname, fieldname, fielddesc, tn_allowedassettypes,
                                    tn_assetId, tn_assetType, assetType, assetId, useCKEditor )
{
       // var oFCKEditor = FCKeditorAPI.GetInstance(fieldname);
       // Restore Get the selected  Save text Element ...
       // oFCKEditor.Selection.GetSelection();

       var embedtype = fckEditorembedtype ;

       var ckeditor =  CKEDITOR.instances[fieldname];
       //console.log("<in> cs.js::addFCKEmbeddedAssetLink() GetInstance["+fieldname+"], embedtype@"+embedtype+",useCKEditor:"+useCKEditor+",ckeditor.modeType:"+ckeditor.config.modeType  );

       if (tn_assetId == assetId && tn_assetType == assetType && embedtype !='link')
	   {
		    //alert(cannotIncludeSelfMsg);
		    return false;
	   }

       if(tn_allowedassettypes && tn_allowedassettypes.toUpperCase().indexOf(tn_assetType.toUpperCase()) == -1)
       {
            //alert(cannotIncludeSelfMsg + tn_allowedassettypes);
            return false;
       }

       // Valid Asset ?  Craft The Url .. 
       var pURL = "../../ContentServer?pagename=OpenMarket/Xcelerate/Actions/";
       if (embedtype=="link")
       {
             pURL = pURL + "EmbeddedLink";
             height = 285;
       }
       else
       {
            // Now check is for Only CKEditor Object () 
            pURL = pURL + "AddInclusion";

            height = 250;
       }
       pURL = pURL + "&tn_id=" + tn_assetId;
       pURL = pURL + "&tn_AssetType=" + tn_assetType;

       pURL = pURL + "&name=" + encodeURIComponent(assetname);
       pURL = pURL + "&AssetType=" + assetType;
       pURL = pURL + "&fieldname=" + encodeURIComponent(fieldname);
       pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
       pURL = pURL + "&formFieldName=" + encodeURIComponent(fieldname);

       //  Need to Change / Edit Included or Linked Asset Element
       if ( ckeditor.config.modeType == "edit")
       {
           pURL = pURL + "&modeType=edit";
           ckeditor.config.modeType = "" ;
       }
       //console.log("<in> cs.js::addFCKEmbeddedAssetLink() URL@"+pURL );
       return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=700,location=no,menubar=no,resizable=yes,toolbar=no,top=50,width=250,height=" + height);
}




function  showFCKAssetPickFromTree ( popid )
{
    //alert("<in> cs.js::showFCKAssetPickFromTree() popid:"+popid ) ;
}





/********************************************************************************
 * Javascript method which updates the state of
   the arbitrary Pin Asset Table
 ********************************************************************************/
function setAssetPinState( pinSlotId )
{
    var pinUi =  pinSlotId.split(":") ;
    var pinComp = document.getElementById(SEARCH_SUBVIEW + pinUi[0]);

    if ( pinComp != null  )
    {
      //  pinComp.src = "http://jgranato-330:8080/cs/images/nopin.gif"   ;
      var srcfile =  pinComp.src.split("/") ;
      // alert("setAssetPinState getElementById slot:"+pinComp.id+", src:"+srcfile[srcfile.length-1])   ;
      if ( srcfile[srcfile.length-1] == "pin.gif")
      {
         pinComp.src = "http://jgranato-330:8080/cs/images/"+pinUi[1] ;
      }

      else
        if ( srcfile[srcfile.length-1] == "nopin.gif" ||
                srcfile[srcfile.length-1] == "nopinB.png" )
         pinComp.src = "http://jgranato-330:8080/cs/images/pin.gif" ;
    }
    else
      //alert("setAssetPinState non id slot:"+pinSlotId)   ;
    return true;
}





// Call the CS Element that will craft the Insite Preview Asset URL
// Both  Advanced and Dash use the same element to query the Preview Asset URL 
function requestInsiteAssetURL ( relURL )
{
   var url = location.protocol + "//" + location.host + relURL;
   new Ajax.Request(url, {
                asynchronous:true,
                onSuccess: function(transport)
                {
                    focusInsiteWindow(transport.responseText);
                }
   });
}



function showSelectFilterValue( filterId )
{
     //alert("showSelectFilterValue: "+filterId)      ;
     var filter = 	dijit.byId(filterId);

     if ( filter != null )
     {
         //value = filter.attr('value') ;
         //alert("showSelectFilterValue:  value "+filter.value )      ;

     }
}




function setDashWindowPreferences()
{
	//name window for use in opening different UI's, dash, advanced, insite..etc. Need reference to self
    window.top.name=DASH_SCREEN;
    dashboardWin=window.top;
	if( window.top.opener != null && window.top.opener.cloneWindowReferences ){
    //this page was opened up form another..need to set window references for window buttons after this page
    // has loaded so call it now!
    window.top.opener.cloneWindowReferences(); 
    }
}                       


function populateTree()
	  {
 		document.getElementById("leftNav:treeValues").value = treeObj.getNodeOrders();
		document.getElementById("leftNav:draggedOnes").value = document.mainForm.__draggedFields__.value;
		document.getElementById("leftNav:intialSitePlanState").value = document.mainForm.__initialState__.value;
	  }


popupSearchField = "";
popupSearchLink = "";

/**
 * Checks whether the user is in the search box and keyed in enter button
 * and does search
 */
function _submitOnEnter( a0 )
{
    if( a0 )
    {
        var a3;
        if( a0.srcElement == undefined )
            a3 = a0.target;
        else
            a3 = a0.srcElement;
        if( !a3 )return true;
         if( a0.keyCode == 13 )
           {
            if( 'header:searchField' == a3.id)
        	{
               _adfspu('mainForm', 1, 0, 'header:searchCommandLink');
                return false;
            } 
            
            else if(popupSearchField ==  a3.id)
            {
              	 _adfspu('mainForm', 1, 0, popupSearchLink);
                return false;
            }
            
        }
    }
}



/******************************************************************************************
Modified:   May 17, 2010 
Reason:      Fixed PR#22262  DashUI When you attempt to resize an image that has been inserted 
                   into an attribute that uses FCKEditor and save it via Dash with IE7 it crashes IE7. 
	         When editing the same image via the Advanced UI and saving it, 
	         it saves successfully.
Reason;      This function is included here to set the focus to the attribute which 
                   has FCKeditor as its editor in dash UI. This is done after a user clicks on the 
	       (attribute)text box and after FCKeditor completes loading.
	This callback function is called only from Dash UI (after the editor is loaded) and not from
	advanced UI because FCKeditorAPI object is not available at the top level of the window i.e.
	'window.top.FCKeditorAPI' is undefined in advanced UI.  
****************************************************************************************/
function FCKeditor_OnComplete( editorInstance )
{
	editorInstance.Focus();    
	
	// Fires when the FCKeditor instance object loses the input focus. 
    editorInstance.Events.AttachEvent('OnBlur', FCKeditor_OnBlur);
}

/*********************************************************************************
I'd almost given up when I discovered that FCKeditor has a JavaScript API,
 making interaction with it possible once the editor is loaded and running.

It was then a simple matter of adding a few more lines of 
JavaScript to the head of the document to get FCKeditor to change it's background colour
 on focus in the same way as the other form elements.

*********************************************************************************/

function FCKeditor_OnFocus(editorInstance)
{
  editorInstance.EditorDocument.body.style.cssText += 'background-color:#ffd;';
}
  
 
/***********************************************************************************************
*					         	FCKeditor_OnBlur

  Modified:   May 17, 2010 
  Reason:      Fixed PR#22262  DashUI When you attempt to resize an image that has been inserted 
                     into an attribute that uses FCKEditor and save it via Dash with IE7 it crashes IE7. 
		When editing the same image via the Advanced UI and saving it, 
		it saves successfully.

 FCKEditorDocument = ( object ) - the DOM Document object for the editing area.
 
Properties:   

Name = ( string ) - the instance name. 
Status = ( integer ) - the editor status (loading status). 

EditorDocument = ( object ) - the DOM Document object for the editing area. 
EditorWindow = ( object ) - the DOM Window object for the editing area. 

 ***********************************************************************************************/ 
function FCKeditor_OnBlur(editorInstance)
{
  editorInstance.EditorDocument.body.style.cssText += 'background-color:#ffd;';
  
  // in order to get the element that is selected, first check the type of the selection
  // Doesn't seem to be working in Fx when the selection is only one character - 
  // This is due to some bug in the 'GetParentElement' method
  //   NONE : 0    -  selection is empty
  //  TEXT   : 1,    -  selection contains text (may also contains CONTROL objects)
  //	CONTROL : 2     - only one element is selected (such as img, table etc)
  
  if(editorInstance.Selection.GetType() == 'Control')
  {
     var _window =  editorInstance.EditorWindow  ;    // the DOM Document object for the FCKeditor editing area.  
     var _document = editorInstance.EditorDocument ;  // the DOM Window object for the FCKeditor editing area.   
     
	 try{
		if(_window["getSelection"]){ 		
		    _window.getSelection().removeAllRanges();		  			
		}else if(_document.selection){

			if(_document.selection.empty){
			  _document.selection.empty();
		    }else if(_document.selection.clear){
			 _document.selection.clear();
			}
		}
		return true;
	}catch(e){
		alert(" Error on Blur Focus" ) ; //dojo.debug(e);
		return false;
	}
	
  }
  else if(editorInstance.Selection.GetType() == 'Text')
  {
       return true ;
  }  
}


/**
 * Used to validate that text was selected for FCK
 */
function validateWebEditor( sFormFieldName )
{
    var oEditor;
    // using FCKEditor
    if( FCKeditorAPI == null )
    {
        oEditor = null;
    }
    else
    {
        oEditor = FCKeditorAPI.GetInstance(sFormFieldName);
    }


    if( oEditor == null )
    {
        //user hasn't created fckeditor and hasn't selected text..reset value just to be sure
        document.getElementById("contentPaneMainBodySubView:webeditorSelectedValue").value = '';
    }
    else
    {
        var textrange;

        if( isIE )
        {
            textrange = oEditor.EditorDocument.selection.createRange().text;
        }
        else
        {
            textrange = oEditor.EditorWindow.getSelection();
        }

        //Set selected value for server side validation to use
        document.getElementById("contentPaneMainBodySubView:webeditorSelectedValue").value = textrange.text;
    }
    return true;

}




function updateFckEditorLegacy()
{
    //console.log( "<in> cs.js::updateFckEditorLegacy() Legacy FCKEditor  ");
    if( typeof FCKeditorAPI == "object" )
    {
		for ( var name in FCKeditorAPI.Instances )
		{
            var oEditor = FCKeditorAPI.Instances[ name ]
			oEditor.UpdateLinkedField();
            //console.log( " <setData> FCKEditor.UpdateLinkedField name@"+name);
        }
	}
}




// Work Smarte not Harder 
function updateFckEditor()
{
    // Update FCKeditor
    updateFckEditorLegacy() ;
    // Update CKEditor 
    updateCKEditorX() ;
}









/**
 * Javascript method which retrieves the selected text and sets it into a hidden field
 */ 
function getSelectedTextFromTextArea(txtAreaElementId) {
	var textAreaComponent = window.document.getElementById(txtAreaElementId);

    //Check if the browser is IE
	if( document.selection && document.selection.createRange )
	{
        //Browser is IE, use the selection.createRange to get the selected text
		var textRange = document.selection.createRange();
		var selectedText = textRange.text;
    }
	else
	{	
		//Browser is non-IE, get the selected text using selectionStart and selectionEnd
		var selStart = textAreaComponent.selectionStart;
		var selEnd = textAreaComponent.selectionEnd;
		var selectedText = (textAreaComponent.value).substring(selStart, selEnd);
	}
	
	//Set the selected text value into the hidden field (defined in ainspect.jspx)
	document.getElementById("contentPaneMainBodySubView:webeditorSelectedValue").value = selectedText;
	return true;
}


// Used by dash scroller to update variable and submit form
function updateScroller( startMenuId )
{
    document.getElementById("selectedStartMenu").value = startMenuId;
    document.getElementById("scrollerEvent").value = "true";
    document.forms[0].submit();
}


function swap( currentImage, newImageName )
{
    if( document.images )
    {
        currentImage.src = newImageName;
    }
}


/* Function for checking user */
var httpObj;

function createRequestObject()
{

    var req;

    if( window.XMLHttpRequest )
    {
        /* Firefox, Safari, Opera... */
        req = new XMLHttpRequest();
    }
    else if( window.ActiveXObject )
    {
        /* Internet Explorer 5+ */
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else
    {
        /* There is an error creating the object,
         * just as an old browser is being used. */
        //alert('Problem creating the XMLHttpRequest object');
    }

    return req;
}

function checkUser()
{
	// This needs to be refactored later since this method probably is not required any more.
    /*try
    {
        if( currentUserSite.charAt(0) == '' );
    }
    catch ( e )
    {
        //alert("'currentUserSite' is not defined before calling checkUser function. Format should be username:pubid");
        return;
    }

    try
    {
        if( csPath.charAt(0) == '' );
    }
    catch ( e )
    {
        //alert("'csPath' is not defined before including cs.js file.");
        return;
    }

    sendRequest(csPath + "?pagename=OpenMarket/Xcelerate/Util/getCurrentUser&ran=" + Math.random());
	*/
	return;
}

function sendRequest( url )
{
    try
    {
        /* Make the XMLHttpRequest object */
        httpObj = createRequestObject();
        /* Open PHP script for requests */
        httpObj.open('get', url);
        httpObj.onreadystatechange = handleResponse;
        httpObj.send(null);
    }
    catch ( e )
    {
    }
}




function handleResponse()
{
    try
    {
        if( httpObj.readyState == 4 && httpObj.status == 200 )
        {
            /* Text returned FROM the PHP script */
            var response = httpObj.responseText;

            if(response.indexOf("_NOGOOD_") > -1)
                return;

            if( response != currentUserSite )
            {
                var sHref = window.location.href;
                if( sHref.indexOf("dash.jspx") > -1 )
                {
                    // Dash
                    if( sHref.indexOf('#') > -1 )
                        sHref = sHref.substring(0, sHref.indexOf('#'));
                    if( sHref.indexOf('?') < 0 )
                        sHref += '?';
                    if( sHref.indexOf("CS_UI_CONTEXT") < 0 )
                    {
                        if( sHref.charAt(sHref.length - 1) == '?' )
                            sHref += "CS_UI_CONTEXT=CS_ADVANCE_UI";
                        else
                            sHref += "&CS_UI_CONTEXT=CS_ADVANCE_UI";
                    }
                    window.location.href = sHref;
                }
                else
                {
                    // Not dash
                    window.location.href = removeParam(sHref, "pubid");
                }
            }
        }
    }
    catch ( e )
    {
    }
}

function removeParam( href, paramname )
{
    var index = href.indexOf('?') + 1;
    var ret = href.substring(0, index);
    var temphref = href.substring(index);
    var params = temphref.split("&");

    var first = true;
    var tempName;
    for( var i = 0, l = params.length; i < l; i++ )
    {
        tempName = params[i].split("=")[0];
        if( paramname != tempName )
        {
            if( first )
            {
                ret += params[i];
                first = false;
            }
            else
            {
                ret += '&' + params[i];
            }
        }
    }

    return ret;
}

function buildTree(divId , formName , draggedField , initialState)
{
		/*if(document.getElementById("leftNav:drop1"))
		{
			treeObj = new JSDragDropTree('mainForm','__draggedFields__');
		treeObj.setTreeId('leftNav:drop1');
		treeObj.setMaximumDepth(7);treeObj.setMessageMaximumDepthReached('Maximum depth reached');treeObj.initTree();
		treeObj.expandAll();
		var initialState = eval("document.mainForm.__initialState__");
		initialState.value=treeObj.getNodeOrders();
		}*/
		if(document.getElementById(divId))
		{
			treeObj = new JSDragDropTree(formName,draggedField);
		treeObj.setTreeId(divId);
		treeObj.setMaximumDepth(7);treeObj.setMessageMaximumDepthReached('Maximum depth reached');treeObj.initTree();
		treeObj.expandAll();
		var initialState = eval("document."+formName+"."+initialState);
		initialState.value=treeObj.getNodeOrders();
		}

}

// Function used to create the clarkii image editor on the form.
function createClarkii(params1){
            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
            var swfVersionStr = "10.0.0";

            var flashvars = {};
            var params = {};
            var attributes = {};
            
            var defaultTextColor = params1.defaultTextColor;
            var defaultTextFont = params1.defaultTextFontFamily;
			
			//formatting default values into their acceptable input format	
            if(typeof(defaultTextColor)!='undefined')
            	defaultTextColor=defaultTextColor.replace(/#/g, '');
            else
            	defaultTextColor='000000';	//default value
            
            if(typeof(defaultTextFont)!='undefined'){
	           	defaultTextFont=defaultTextFont.replace(/_/g, '');
            	defaultTextFont='_'+defaultTextFont;
            }
            else
            	defaultTextFont='_Arial'; //dafault value	
            
            // Change these variables according to your needs
            flashvars.AdminID = "0";
	    	flashvars.ClarkiiID = params1.oieName;
            flashvars.FilePath = params1.oieCodebase;
            flashvars.StartupProject = "";
            
			//setting default text decoration parameters
			flashvars.DefaultTextFontFamily = defaultTextFont;
			flashvars.DefaultTextSize = params1.defaultTextSize;
			flashvars.DefaultTextColor = defaultTextColor;
			            
			flashvars.DefaultTAPTextFontFamily = defaultTextFont;
			flashvars.DefaultTAPTextSize = params1.defaultTextSize;
			flashvars.DefaultTAPTextColor = defaultTextColor;
			
       		//setting synchronous encoder property for faster encoding
       		flashvars.UseSynchronousEncoder="true";

            if(params1.oieImgName == "null")
            {
            	flashvars.StartupProject =  params1.cgipath + "Xcelerate/graphics/common/logo/spacer.gif";
            	isImageEmpty = true;
            }
            else
            {
            	flashvars.StartupProject = params1.cgipath + params1.imgurl + '%26test=test.jpg';
            }

            //Parameters
            params.quality = "high";
            params.bgcolor = "#ffffff";
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            attributes.id = params1.oieName;
            attributes.name = params1.oieName;
            attributes.align = "middle";

            swfobject.embedSWF(
                params1.oieCodebase+"clarkii4.swf", params1.divId,
                params1.oieWidth, params1.oieHeight,
                swfVersionStr, "",
                flashvars, params, attributes);
}

// Create the OIE Image Editor
function createClarkiiImageEditor(params)
{
	displayWait();	
	createClarkii(params);  
		document.getElementById(params.oieName + "_IMGPICKER_DIV").style.display= "block";
	    document.getElementById(params.oieName + "_EDITBUT_DIV").style.display= "none";
	    if (params.oieImgName != "null")
	    	document.forms[0].elements[MAIN_SUBVIEW+params.oieName+'_FILENAME'].value=params.oieImgName;
}

//load the image from clarkii4 image editor
function loadClarkiiImageEditor(fieldname, imageUrl) {
		var objEditor = swfobject.getObjectById(fieldname);
		objEditor.openImageFnc("../../"+imageUrl);
                var fileName=imageUrl.substring(imageUrl.lastIndexOf('=')+1);
                var inputName = fieldname.substring(8);
                document.forms[0].elements[MAIN_SUBVIEW+'clarkii_'+inputName+'_FILENAME'].value=fileName;
                document.forms[0].elements[MAIN_SUBVIEW+'clarkii_'+inputName+'_DELETE'].value='';
                isImageEmpty = false;
}

//insert the image from clarkii4 image editor
function insertClarkiiImageEditor(fieldname, imageUrl) {
	if ( isImageEmpty )
	{
		loadClarkiiImageEditor(fieldname, imageUrl);
	}
	else
	{
   		var objEditor = swfobject.getObjectById(fieldname);
					objEditor.insertImageFromInternet('../../'+imageUrl);   		
       		var inputName = fieldname.substring(8);
       		if(document.forms[0].elements[MAIN_SUBVIEW+'clarkii_'+inputName+'_FILENAME'].value=='')
       		{
			var fileName=imageUrl.substring(imageUrl.lastIndexOf('=')+1);
			document.forms[0].elements[MAIN_SUBVIEW+'clarkii_'+inputName+'_FILENAME'].value=fileName;
    		   	document.forms[0].elements[MAIN_SUBVIEW+'clarkii_'+inputName+'_DELETE'].value='';
		}
    	}
}

// Create the OIE Image Editor
function createOieImageEditor( params )
{
       var oieString = "<object type='application/x-oleobject' name='"+params.oieName +"' id='" + params.oieName + "' " +
         " classid='"+ CLSID_OIE +"'    " +
         " codebase='" + params.oieCodebase + "'    " +
         " width='" + params.oieWidth + "'          " +
         " height='" + params.oieHeight + "'>       " +
             "<param name='DefaultTextFontFamily' value='" + params.defaultTextFontFamily + "'>       " +
             "<param name='DefaultTextSize' value='" + params.defaultTextSize + "'>             " +
             "<param name='DefaultTextColor' value='" + params.defaultTextColor + "'>             " +
             "<param name='SnapshotPaneVisible' value='" + params.snapshotPanelVisible + "'>    " +
             "<param name='FitImage' value='" + params.fitImage + "'>   " +
             "<param name='LimitCropping' value='" + params.limitCropping + "'>                 " +
             "<param name='CropWidth' value='" + params.cropWidth + "'> " +
             "<param name='CropHeight' value='" + params.cropHeight + "'>                       " +
             "<param name='EnableOIEFormat' value='" + params.enableOIEFormat + "'>             " +
             "<param name='LimitSize' value='" + params.limitSize + "'> " +
             "<param name='MaxWidth' value='" + params.maxWidth + "'>   " +
             "<param name='MaxHeight' value='" + params.maxHeight + "'> " +
             "<param name='MinWidth' value='" + params.minWidth + "'>   " +
             "<param name='MinHeight' value='" + params.minHeight + "'> " +
             "<param name='AutoResample' value='" + params.autoResample + "'>                   " +
             "<param name='AutoResampleProportional' value='" + params.autoResampleProportional + "'>   " +
             "<param name='TagEdit' value='" + params.tagEdit + "'> " +
             "<param name='Base64JpegQuality' value='" + params.base64JpegQuality + "'> " +
             "<param name='AskToSaveLocally' value='" + params.askToSaveLocally + "'> " +
             "<param name='DefaultSavingType' value='" + params.defaultSavingType + "'> " +
             "<param name='EnableGIFSaving' value='" + params.enableGifSaving + "'> " +
             "<param name='EnableJPEGSaving' value='" + params.enableJpegSaving + "'> " +
             "<param name='EnablePNGSaving' value='" + params.enablePngSaving + "'> " +
             "<param name='EnableTIFFSaving' value='" + params.enableTiffSaving + "'> " +
             "<param name='EnableBMPSaving' value='" + params.enableBmpSaving + "'> " +
             "<param name='GridVisible' value='" + params.gridVisible + "'> " +
             "<param name='GridSnap' value='" + params.gridSnap + "'> " +
             "<param name='GridSpacingX' value='" + params.gridSpacingX + "'> " +
             "<param name='GridSpacingY' value='" + params.gridSpacingY + "'> " +
             "<param name='MaxThumbnailHeight' value='" + params.maxThumbnailHeight + "'> " +
             "<param name='MaxThumbnailWidth' value='" + params.maxThumbnailWidth + "'> " +
             "<param name='ThumbnailFormat' value='" + params.thumbnailFormat + "'> " +
         " </object>";
         // "<param name='langid' value='" + params.locale + " '>       " +     //this does not work with PPR


    //write innerhtml with oie object text..need to do this innerHTML business in div for PPR to work
    // also please be aware that this function needs to be in a .js file and not inlined or else it wont work with PPR..crazy???
    document.getElementById(params.divId).innerHTML= oieString;    

    var objEditor = document.getElementById( params.oieName );

    //set buttons status from two arrays one button name, the other value
   for( var i = 0; i < params.buttons.length; i++ )
   {
        objEditor.SetFeature(params.buttons[i], params.buttonValues[i]);
   }

    var fileData = document.forms[0].elements[MAIN_SUBVIEW + params.oieName + "_DATA"].value;
    if( fileData != null  && fileData != ''){
        objEditor.LoadFromBase64( fileData, params.oieImgName );
    }

    //show browse and insert  buttons and hide imagepicker button
    document.getElementById(params.oieName + "_IMGPICKER_DIV").style.display= "block";
    document.getElementById(params.oieName + "_EDITBUT_DIV").style.display= "none";

}

//load the image from OIE image editor
function loadOieImageEditor(fieldname, imageUrl) {
    var objEditor = document.getElementById(fieldname);
    objEditor.LoadFromURL(imageUrl);
}

//insert the image from OIE image editor
function insertOieImageEditor(fieldname, imageUrl) {
    var objEditor = document.getElementById(fieldname);
    objEditor.InsertImageFromURL(imageUrl);
}

function displayWait() {
	txtContent = waitMessage;    
    txtContent = "<table><tr align='center'><td><img src=\"../Xcelerate/graphics/common/icon/wait_ax.gif\" alt=\"\" /></td></tr><tr><td class='AFFieldText'>" + txtContent+"</td></tr></table>";
    var mydialog = dijit.byId('underlay');
    if (!mydialog) {
    	var div = document.createElement('div');
	div.innerHTML = txtContent;
    	mydialog = new dijit.Dialog({id:'underlay', title: ""}, div);
    	dojo.body().appendChild(mydialog.domNode);
    	mydialog.titleBar.style.display='none';
    	mydialog.startup();
    }
    mydialog.show();
}



/*
	A general function which is called whenever there is a partial submit.
	Changed the syntax to get the event inside the method. We need to identify
	the source inside the method which triggers the event.
 */
function submitCEVForm(butEvent)
{
	displayWait();
    count = 0;	
    imageObjects = [];
    oieObjects = [];
    
    /**
    	The check is necessary because in case of clarkii image we submit
    	the form manually by using _adfspu. So, when the form gets submitted
    	the submitCEVForm is again called which will cause the recurrsion.

    	The below check will break the recurrsion due to manual save in case
    	there are clarkii objects on the form.
     */
    if(isSubmited)
    {
	isSubmited = false;
	return true;
    }
    
    /* 
    	When the submit is called by the button
        we will find the id of the button which submits the form.
        The button id will be used to submit the form partilly when there
        are some clarkii objects.

        if the browser is IE then butEvent.srcElement will work else
        butEvent.target will solve our purpose.
     */
    var saveButton;	
	try 
	{	
		saveButton = butEvent && (butEvent.target || butEvent.srcElement);
	} 
	catch (err)
	{
		// Ignore this error. This is caused by cross iframe problems and we don't have to worry about it
		// because this matters only when clarkii is in the page and user clicked the save button
	}
      
    if(saveButton && saveButton.parentNode)
    	submitButtonId = saveButton.parentNode.id;
	
    // Image Editor
    var cevScreen = document.forms[0].elements[MAIN_SUBVIEW + 'ainspect_screen'];
    if( cevScreen == null){
        //not in cev screen so dont bother..TODO should make check for edit mode
        return true;
    }
    
    var objects = document.getElementsByTagName("object");


    // Populate the image objects array for clarkii images inside the 
    // clarkii image editor present on the form.
    for (i = 0; i < objects.length; i++) 
    {
        if (objects[i].id.indexOf("clarkii_") != -1) 
        {
		imageObjects.push(objects[i]);
		}
		else if (objects[i].id.indexOf("oie_") != -1)
		{
			oieObjects.push(objects[i]);
		}
    }
	
	
   // Populate the oieObjects array which contains the images opened in 
   // OIE image editor.
   if(oieObjects.length > 0)
   {
	saveOIEImages();
   }
	
   if(imageObjects.length > 0 && !isImageEmpty)
   { 
   	if(!isSubmited)
   	{
		saveImages();
		if (count < imageObjects.length)
		{
			// If there are clakii image editor on the form we will return false.
			// this is due to save is not shyncronous in case of clarkii and we have to 
			// handle the callback to populate the data
			return false;
		}
	}
   }
	
	return true;
}

/**
	The method is used to save the object which are there in the clarkii 
	image editor.
 */
function saveImages()
{
		if(count < imageObjects.length)
		{
			var objEditor=imageObjects[count];
			var objId=objEditor.id;
			var inputName = eval(objId + '_PARAM');
			var quality = inputName.base64JpegQuality;
			var fileName = document.forms[0].elements[MAIN_SUBVIEW+objId+'_FILENAME'].value;
			if(fileName.length>0)
			{
				var fileExt=fileName.substring(fileName.lastIndexOf('.')+1);
				if(fileExt.length==0)
					fileExt='jpg'
			}
			var isDeleted = (fileName == '' || fileName.substr(fileName.length - 7) == 'deleted');
			if (!isDeleted) 
			{
			    document.forms[0].elements[MAIN_SUBVIEW+objId+'_DELETE'].value = '';
			    document.forms[0].elements[MAIN_SUBVIEW+objId+'_FILENAME'].value = fileName;                
			    objEditor.getAsBase64(fileExt,quality);
			    window.status='Saving '+fileName;
			} else {
			    document.forms[0].elements[MAIN_SUBVIEW+objId+'_DELETE'].value = 'on';
			    document.forms[0].elements[MAIN_SUBVIEW+objId+'_FILENAME'].value = "";
			    document.forms[0].elements[MAIN_SUBVIEW+objId+'_DATA'].value="";
			   // obj.elements[inputName].value = "";
			    count=count+1;
			    saveImages();
			}
			
		}else 
		{
		    // Make sure the variable is set to true which will
		    // break the recursive calling of submitCEVForm.
		    isSubmited = true;
		    // Submit the form when the data for each clarkii image on the form is collected.
		    _adfspu('mainForm', 1, 0, submitButtonId);
		}
     	
     }
     
//callback method from clarkii
function onClarkiiSnapshot(dat)
{
	var objEditor=imageObjects[count];
	var objId=objEditor.id;
	document.forms[0].elements[MAIN_SUBVIEW+objId+'_DATA'].value = dat;
	count=count+1;
	saveImages();
}

/**
	Callback method of clarkii for setting the file name
	opened using the clakii user interface to browse images
	from local file system
*/

function onClarkiiImageOpen( data )
{
	var objId = data[1];
	document.forms[0].elements[MAIN_SUBVIEW+objId+'_FILENAME'].value = data[0];
	isImageEmpty = false;
}

/**
	The method is used to call the customization method generated for
	clarkii image editors. The method is called when the clarkii application
	is loading.
 */
 
 function onApplicationLoad(data){
 	customizeClarkii(data[0]);
 }

// A general function to customize the clarkii attribute editor. 
 function customizeClarkii(inputName)
 {
 	var param = eval(inputName + "_PARAM");
 	var objEditor=document.getElementById(inputName);
	 	
	var key='',value='';
	var position=-1;

	var keys=eval(inputName + "_PARAM" + "_BUTTONS");
	var values=eval(inputName + "_PARAM" + "_BUTTONVALS");
	for(i=0;i<keys.length;i++){	
		if(objEditor.setProperty){
			key=keys[i];
			value=values[i];
			if(value==0){
				position=buttons.toLowerCase().indexOf(key.toLowerCase());
				if(position>-1){
					key='id'+buttons.substring(position,buttons.indexOf(",",position))+'Btn';
				}else{
					position=sliders.toLowerCase().indexOf(key.toLowerCase());
					if(position>-1){
						key='id'+sliders.substring(position,sliders.indexOf(",",position))+'Sld';
					}else{
						position=checkboxes.toLowerCase().indexOf(key.toLowerCase());
						if(position>-1){
							key='id'+checkboxes.substring(position,checkboxes.indexOf(",",position))+'Chb';
						}else{
							position=others.toLowerCase().indexOf(key.toLowerCase());
							if(position>-1){
								key='id'+others.substring(position,others.indexOf(",",position));
							}else{
								key='';
							}	
						}
					}
				}
				if(key!=''){
					try{objEditor.setProperty(key, "visible","false");}catch(err){}
				}
			}
		}
	}
	//doing some default task
	objEditor.setProperty("idRightPanel", "visible","false");
	objEditor.setProperty("idRestartBtn", "visible","false");
	objEditor.setProperty("idSaveBtn", "visible","false");
	objEditor.setProperty("idSaveLocallyBtn", "visible","false");
	objEditor.setProperty("idWPSaveBtn", "visible","false");
	objEditor.setProperty("idFullScreen", "visible","false");
	objEditor.setProperty("idFullScreenBtn", "visible","true");
	if(param.fitImage == "true")
		try{objEditor.executeFunction(["zoomCanvasToFit"]);}catch(err){}
		
	try{objEditor.executeFunction(["snapToGrid",param.gridSnap]);}catch(err){}		
    //[DK] fix for #23923 -->with the following line enabled synchronous encoder does not work
	//try{objEditor.executeFunction(["thumbnailParameters","true", "true", "<%=maxThumbnailWidth%>","<%=maxThumbnailHeight%>"]);}catch(err){}	
	try{objEditor.executeFunction(["fileType",param.defaultSavingType]);}catch(err){}				    	
	try{objEditor.executeFunction(['toggleRightPanel']);}catch(err){}
	if(parent.dijit.byId('underlay'))
		{
			parent.dijit.byId('underlay').hide();
		}
 }

/**
	The method is used to save the images there on the OIE editor on the form
 */
function saveOIEImages()
{
    for (i = 0; i < oieObjects.length; i++) {
        if (oieObjects[i].classid == CLSID_OIE) {
            var objEditor = oieObjects[i];
            var objId = objEditor.id;
            var fileName = objEditor.OriginalFileName;

            var isDeleted = (document.forms[0].elements[MAIN_SUBVIEW + objId + '_DELETE'].value == 'on');
            if (!isDeleted) {
                document.forms[0].elements[MAIN_SUBVIEW + objId + "_FILENAME"].value = fileName;
                objEditor.GetAsBase64(fileName);
                document.forms[0].elements[MAIN_SUBVIEW + objId + "_DATA"].value=objEditor.EncodedData;
            }
            else {
                document.forms[0].elements[MAIN_SUBVIEW + objId + "_FILENAME"].value = "";
                document.forms[0].elements[MAIN_SUBVIEW + objId + "_DATA"].value = "";
            }
        }
    }
}

function pausecomp(millis)
{
var date = new Date();
var curDate = null;

do { curDate = new Date(); }
while(curDate-date < millis);
}


// Save the State of the Share All Check Box
var prevShareAllBox = false ;

function shareAllOrNone ( siteid )
{
    var checkboxes = document.getElementsByName(MAIN_SUBVIEW+"selectManyCheckbox1");
    // Slot 1 contains the Share All Checkbox state
    var crntShareAllBox = checkboxes[1].checked;

    // User Clicked on the Share All State Changed
    // Shared All Sites CheckBox
    if ( crntShareAllBox != prevShareAllBox )
    {
       for(var i = 1; i < checkboxes.length; i++)
       {
         var check = checkboxes[i];
         check.checked = crntShareAllBox ;
       }
       prevShareAllBox=crntShareAllBox  ;
    }
    else
    {
         // if user has uncheck in essence unshared any of the Enabled sites
         // and Shared All Sites is checked
         // then we need to uncheck the Share All Check Boxe
         if ( crntShareAllBox == true )
         {
              for(var i = 1; i < checkboxes.length; i++)
              {
                 var check = checkboxes[i];
                 // Has the user unchecked
                 if ( check.checked == false )
                 {
                     checkboxes[1].checked = false ;
                     prevShareAllBox=false ;
                 }
              }
         }
    }
}

/*****************************************************************************************
 * Determines what mode the Main Window Body to be displayed and
   automatically sets and places the cursor to the first Element of type
   Input and text.

*  Modified: Nov 04, 2009
   Reason:   PR# 21223 javascript error in IE7 "Can't move focus to the control"
             This only happens if you click edit when you are on A specific tab
             that contain no attributes of read/write input text elements.
             The problem is that if an user access the tab on the inspect screen,
             then clicks edit, the browser throws a javascript error about
             moving focus to the input text control element.

 * @param  none
 **************************************************************************************/
function setCEVFormCursor()
{
    var cevScreenA = document.getElementById(MAIN_SUBVIEW + 'lowerTabTabs');
    if( cevScreenA == null)
    {
      // No Asset Tabs Lets Do Asset Edit / New Pane with no tabs
      cevScreenA = document.getElementById(MAIN_SUBVIEW + 'assetPaneTable');
    }
    if( cevScreenA == null)
    {
       // At this point proceed without auto focus / blur Triple play
       return ;
    }
    // Get a List OF All Elemets input tags
    var inputTags = cevScreenA.getElementsByTagName("input");

    // Get very firat Input / Edit Text Field Attribute
    for (i = 0; i < inputTags.length; i++)
    {
         var objEditorI = inputTags[i];
         var objId = objEditorI.id;
         var objtype = objEditorI.type;
         var readOnly= objEditorI.readOnly ;
         var value = objEditorI.value ;

         if (objtype == "text" && !readOnly &&
             !(objId.search(/searchText/) > 0))
         {
             // focus / blur Triple play
             objEditorI.blur() ;
             objEditorI.focus();
             objEditorI.focus();
             break ;

         }
     }
     return ;
}




/*************************************************************************************
 * Sets the cursor to the new and next generated input field for a
   MultiValued Attribute

   Created: Dec 10, 2008 
 * @param  The next new input field element
 **************************************************************************************/
function PPR_MoveCursorToNewMVField ( nextMvField )
{
    var mvNext = document.getElementById(MAIN_SUBVIEW + nextMvField );

    if ( mvNext != null )
    {
      mvNext.focus() ;
      mvNext.focus() ;
    }
}

// Hides the Asset Right Click Asset Menu
function PPR_HideRClkAssetMenu()
{
    var theDiv=document.getElementById(ASSET_TREE_PULLDOWN_MENU);
    if(theDiv)
    {
       theDiv.style.display = 'none';
    }
}


/*
flashInfo.id; //used for flashDiv and flashId
flashInfo.classid
flashInfo.codebase
flashInfo.width
flashInfo.height
flashInfo.align
flashInfo.flashVars
flashInfo.allowScriptAccess
flashInfo.movie
flashInfo.quality
flashInfo.bgcolor
flashInfo.scale
*/

/**
 * Need to create scroller through JScript so that the user does not have to click once to activate. This javascript
 * cannot be called from a portion of the page which is being refreshed through PPR or else it will fail.
 *
 * @param flashInfo
 */
function createFlash( flashInfo )
{    
	
    var flashDiv = document.getElementById(flashInfo.id + "_DIV");
    var flashCode = " <OBJECT classid='"+ flashInfo.classid + "' ";
    flashCode = flashCode + " codebase='" + flashInfo.codebase + "' "
    flashCode = flashCode + " WIDTH='" + flashInfo.width +"' HEIGHT='"+ flashInfo.height +"' ";
    flashCode = flashCode + " id='" + flashInfo.id + "' ";
    flashCode = flashCode + " align='" + flashInfo.align + "'> ";

    flashCode = flashCode + " <PARAM NAME='FlashVars' VALUE='"+ flashInfo.flashVars +"'> ";
    flashCode = flashCode + " <PARAM NAME='allowScriptAccess' VALUE='" + flashInfo.allowScriptAccess+"'> ";
    flashCode = flashCode + " <PARAM NAME='movie' VALUE='"+ flashInfo.movie+"'> ";
    flashCode = flashCode + " <PARAM NAME='quality' VALUE='"+ flashInfo.quality+"'> ";
    flashCode = flashCode + " <PARAM NAME='bgcolor' VALUE='"+ flashInfo.bgcolor+"'> ";
    flashCode = flashCode + " <PARAM NAME='scale' VALUE='"+ flashInfo.scale+"'> ";

    flashCode = flashCode + " <EMBED src='"+ flashInfo.movie +"' quality='"+ flashInfo.quality+"' bgcolor='" + flashInfo.bgcolor+"' ";
    flashCode = flashCode + " FlashVars='"+ flashInfo.flashVars+ "' ";
    flashCode = flashCode + " WIDTH='"+ flashInfo.width+ "' HEIGHT='"+ flashInfo.height+"' NAME='"+ flashInfo.id+"' ";
    flashCode = flashCode + " scale='"+ flashInfo.scale+"' align='"+ flashInfo.align+"' ";
    flashCode = flashCode + " allowScriptAccess='"+ flashInfo.allowScriptAccess+"' ";
    flashCode = flashCode + " TYPE='application/x-shockwave-flash' ";
    flashCode = flashCode + " PLUGINSPAGE='http://www.macromedia.com/go/getflashplayer'> ";

    flashCode = flashCode + " </EMBED> ";
    flashCode = flashCode + " </OBJECT> ";    
    flashDiv.innerHTML = flashCode;
}

/**
 *  Test whether str is a valid int
 * @param str - variable to test if Int
 * @return true if it is a valid integer
 */
function isInt (str)
{
	/* strip leading zeroes to prevent false negative! */
	while (str.charAt(0) == '0' && str.length > 1) str = str.substr(1);
	var i = parseInt(str);
	if (isNaN(i))
		return false;
	i = i.toString();
	if (i != str)
		return false;
	return true;
}

/** [2007-09-1x KG]
 * Test if there are any characters in str that might
 * trip up and parse as HTML or JS.  Used for XSS fixes,
 * adapted from 6.3 fixes.
 * @param str - variable to test for invalid characters
 * @param morechars - optional string of additional characters to check against
 * @param replace - optional boolean, if true, morechars *replaces* invchars (instead of append)
 * @return - true if str contains NO invalid characters.
 */
//Previously ^ \ and / have been removed.
function isCleanString(str, morechars, replace) {
  //[2008-02-21 KGF] checking for \ only at end of string.
  //since this is a special case, it can't simply be thrown into
  //the array with the rest... so we'll always check for it here.
  if (str.substr(str.length - 1) == '\\')
    return false;
  
  var invchars;
  if (replace) invchars = new Array(); //morechars replaces defaults
  else invchars = new Array("'", '"', ';', ':', '?', "\<", "\>", '%');
  if (morechars) { //add additional characters present in morechars
    for (var i = 0; i < morechars.length; i++) {
      invchars.push(morechars.charAt(i));
    }
  }
  //6.3 used iterative loop + charAt.
  //indexOf is MUCH faster especially in FF.
  for (var i = 0; i < invchars.length; i++) {
    if (str.indexOf(invchars[i]) >= 0) return false;
  }
  return true;
}

// Hijack the ADF method call for select all and select none links in the search resutls table.
function changeSelectAll()
{
    var tableDiv = document.getElementById('contentPaneSearchSubView:searchResultsTable');
    var links = tableDiv.getElementsByTagName("a");
    for(var i = 0; i < links.length; i++) {
     var link = links[i];


      if(link.onclick.toString().indexOf("_uixt_contentPaneSearchSubView_searchResultsTable.multiSelect(true)") > 0) 
      {
          if(document.dispatchEvent) {
              link.onclick = selectAll;
          } else
          {
              link.onclick = "selectAll()";
          }
      } else if(link.onclick.toString().indexOf("_uixt_contentPaneSearchSubView_searchResultsTable.multiSelect(false)") > 0) 
      {
          if(document.dispatchEvent) {
              link.onclick = selectNone;
          } else
          {
              link.onclick = "selectNone()";
          }
      }
    }
}


function refreshTags(removeFromLabel)
{
    var checkboxes = document.getElementsByName("contentPaneSearchSubView:searchResultsTable:_s");
    assetIds = new Array();    
    bulkOperation = true;
    for(var i = 0; i < checkboxes.length; i++) {
        var check = checkboxes[i];
        if(check.checked)
        {    
            check.onclick();
        }    
    }    
    bulkOperation = false;
    bulkAddTags(assetIds , removeFromLabel);
}

function addTags(checkbox , assetid , assettype , removeLabel)
{
    var checkboxes = document.getElementsByName("contentPaneSearchSubView:searchResultsTable:_s");    
    assetIds = new Array();    
    bulkOperation = true;
    // get the assetid of the selected assets and add it to the array.
    for(var i = 0; i < checkboxes.length; i++) {
        var check = checkboxes[i];
        if(check.checked)
         {
             check.onclick();
         }
     }
    // Do the add/remove operation
    bulkAddTags(assetIds , removeFromLabel);
    bulkOperation = false;
}

// Used By FckEditor Plug In 
var assetIds = new Array();
var removeFromLabel;
var bulkOperation = false;

function addRemoveTags(checkbox , assetid , assettype , removeLabel) 
{
 //If this is bulk operation just populate the array
  if(bulkOperation) 
  {
      var asset = assettype+":"+assetid;
      assetIds[assetIds.length] = asset;
      removeFromLabel = removeLabel;      
  } else
  {
      addTags(checkbox , assetid , assettype , removeLabel);
  }
}

// check all the checkboxes and invoke addTags method for adding the tags is any.
function selectAll()
{
    var checkboxes = document.getElementsByName("contentPaneSearchSubView:searchResultsTable:_s");
    assetIds = new Array();    
    bulkOperation = true;
    for(var i = 0; i < checkboxes.length; i++) {
        var check = checkboxes[i];
        check.checked=true;
        check.onclick();
    }    
    bulkAddTags(assetIds , removeFromLabel);
    bulkOperation = false;
}

// uncheck all the checkboxes and invoke the addTags method to remove the added tags
function selectNone()
{
    var checkboxes = document.getElementsByName("contentPaneSearchSubView:searchResultsTable:_s");
    var tagselect = document.getElementById('contentPaneSearchSubView:selectTags');
 
    for(var i = 0; i < checkboxes.length; i++) {
        var check = checkboxes[i];
        check.checked=false;       
    }
	var reqURL=window.document.URL ;
        
    var pos = reqURL.lastIndexOf('/')  ;
    if ( pos != -1 )
    {
        reqURL=reqURL.substring(0,pos+1)   ;
        var URL= reqURL+"userTag.jspx";
    }
    
    new Ajax.Request(URL, {
        asynchronous:true,
        onSuccess: function(transport)
        {    	
            selectNoneProc( transport.responseText);
        }
    });
}

function selectNoneProc(myTags)
{
    var tags = myTags.split("||");
  
    var tagselect = document.getElementById('contentPaneSearchSubView:selectTags');    
    var tags = myTags.split("||");
    var tagsArray = new Array();
    // populate the tags array from the hidden value.
    for (x=0 ; x< tags.length ; x ++)
    {
      var tagsAndTagItems = tags[x];
      var tagsAndTagItemsArray = tagsAndTagItems.split("=");
      var tagDetails = tagsAndTagItemsArray[0].split("_");
      var tag = new Object();
      tag.tagName = tagDetails[0];
      tag.tagId = tagDetails[1];
      tag.tagItems = tagsAndTagItemsArray[1];
      tagsArray[x] = tag;
    }
    
    tagselect.options.length = 1;
    var options = tagselect.options;
    var tagCounter = 1;
    for(var m = 0; m < tagsArray.length; m++) {
        var tag = tagsArray[m];
        var tagName = tag.tagName;
        var tagId = tag.tagId;
        options[tagCounter] = new Option("- "+tagName, "add``"+tagId);
        tagCounter++;
    }
        
}

//Add/remove the tags based on the passed assetids
//1). Populate the list of tags in the javascript array.
//2). Get the list of tags to be removed
//3). Populate the the list of tags for removing from "add to" list.
//4). Repopulate the tags dropdown
function bulkAddTags(assetids , removeLabel)
{
	var reqURL=window.document.URL ;

    var pos = reqURL.lastIndexOf('/')  ;
    if ( pos != -1 )
    {
        reqURL=reqURL.substring(0,pos+1)   ;
        var URL= reqURL+"userTag.jspx";
    }
    
    new Ajax.Request(URL, {
        asynchronous:true,
        method:'get',
        onSuccess: function(transport)
        {
    		bulkAddTagsProc(assetids, removeLabel, transport.responseText);
        }
    });
}

function bulkAddTagsProc(assetids , removeLabel, myTags)
{
      var tagselect = document.getElementById('contentPaneSearchSubView:selectTags');
      var tags = myTags.split("||");
      var tagsArray = new Array();
      // populate the tags array from the hidden value.
      for (x=0 ; x< tags.length ; x ++)
      {
        var tagsAndTagItems = tags[x];
        var tagsAndTagItemsArray = tagsAndTagItems.split("=");
        var tagDetails = tagsAndTagItemsArray[0].split("_");
        var tag = new Object();
        tag.tagName = tagDetails[0];
        tag.tagId = tagDetails[1];
        tag.tagItems = tagsAndTagItemsArray[1];
        tagsArray[x] = tag;
      }
      if(!tagselect)
      {
          return;
      }   
      // If the assetIds are not passed , add all the tags to "Add To" list.
      if(assetids.length < 1)
      {
          
          tagselect.options.length = 1;
          var options = tagselect.options;
          var tagCounter = 1;
          for(var m = 0; m < tagsArray.length; m++) {
              var tag = tagsArray[m];
              var tagName = tag.tagName;
              var tagId = tag.tagId;
              options[tagCounter] = new Option("- "+tagName, "add``"+tagId);
              tagCounter++;
          }
         return;
      }      
      var newOptions = new Array();
      var counter = 0;
      var newOptionsString ="" // used for duplicate Check;
      // If the tag has the assetid in its tagitem add it to the remove from tag list.
      for (i=0 ; i< assetids.length ; i ++)
      {
          var asset = assetids[i];
          for (j=0 ; j< tagsArray.length ; j ++)
          {
            var tag = tagsArray [j]
            var tagName = tag.tagName;
            var tagId = tag.tagId
            var tagItems = tag.tagItems;
            if(tagItems && tagItems.indexOf(asset) > -1)
            {
                if(newOptionsString.indexOf(tagId) == -1)
                {
                   var objOption = new Option("- "+tagName, "remove``"+tagId);
                   newOptions[counter] =  objOption;
                   counter++;
                   newOptionsString = newOptionsString +"__" + tagId;
                }    
            }
          }
      }   
      // check whether "Remove from" is to be added or not.
      var removeOption = new Option(removeLabel , "Remove From...");
      var addRemoveFrom = false;
      if(newOptions.length > 0)
      {
          addRemoveFrom = true;         
      }
      // Get the list of the tags to be removed from "Add To" list.
      var removeFromAdd = new Array();
      var removeFromAddCounter = 0;
      for(var k = 0; k < tagsArray.length; k++) {
         var localTag = tagsArray[k];
         var tagItem = localTag.tagItems;
         var addToList = true;
          for(var l = 0; l < assetIds.length; l++) {
              var assetId = assetIds[l];
              if(tagItem.indexOf(assetId) < 0)
              {
                  addToList = false;
               } 
          }
              if(addToList)
              {
                  removeFromAdd[removeFromAddCounter] = localTag;
                  removeFromAddCounter ++;
              }
      }
      var tagselect = document.getElementById('contentPaneSearchSubView:selectTags');
      tagselect.options.length = 1;
      var options = tagselect.options;
      var tagCounter = 1;
      for(var m = 0; m < tagsArray.length; m++) {
          var tag = tagsArray[m];
          var addTag = true;
          var tagName = tag.tagName;
          var tagId = tag.tagId;
              for(var n = 0; n < removeFromAdd.length; n++) {
                  var notToAddTag = removeFromAdd[n];                  
                  if( tagId== notToAddTag.tagId &&  tagName== notToAddTag.tagName)
                  {
                      addTag = false;
                  }
              }
           if(addTag)
           {
               options[tagCounter] = new Option("- "+tagName, "add``"+tagId);
               tagCounter++;
           }
      }
      if(addRemoveFrom)
      {
          options[tagCounter] = removeOption;
          tagCounter++;
          for(var n = 0; n < newOptions.length; n++) {              
              options[tagCounter] = newOptions[n];
              tagCounter ++;
          }
      }
}

var xOffset=-60 //x offset of the tooltip
var yOffset=10 //y offset of the tooltip

// Utility to  display and shpw a Div Container, Menu, etc
function showTip(e, divName)
{
	var theDiv = document.getElementById(divName);
	if(theDiv)
	{
		var currentX=(e.pageX)?e.pageX : event.clientX;
		var currentY=(e.pageY)?e.pageY : event.clientY;
		//workaround for css relative-absolute-positioning
		var elements = dojo.body().childNodes;
		for (var i = 0; i < elements.length; i++)
		{
			if (elements[i].id == divName)
			{
				dojo.body().removeChild(elements[i]);
				break;
			}
		}
   		dojo.body().appendChild(theDiv);
		//set the x position and y position with respect to the mouse position
		theDiv.style.left=currentX+xOffset+"px";
		theDiv.style.top=currentY+yOffset+"px";
		theDiv.style.display="block";
				
	}
}


// Must Use the Request Object Advanced knows nothing about Faces
// The URL is  crafted using the scheme and ServerName + port + INSITEurl

// Preview the Asset Triggered via Asset Pulldown Menu
//   1). Parse and retrieve asset type and Asset ID
function openMenuPreview( divName, baseURL )
{
    hideTip( divName ) ;

    // Add the right-click selected asset to preview 
        var assetType = document.getElementById("leftNav:assetType").value ;
        var assetId =   document.getElementById("leftNav:assetId").value ;
    var assetURL  =  baseURL+"&AssetType="+assetType+"&id="+assetId ;
        openPreview(assetURL) ;
    }


function findPos(obj)
{
    var curleft = curtop = 0;

    if (obj.offsetParent) {
    do {
                       curleft += obj.offsetLeft;
                       curtop += obj.offsetTop;
               } while (obj = obj.offsetParent);
    }
    return [curleft,curtop];
}

//  On Success, process the reply string received from
//  the server containg the asset permission
//  Reply Message Payload has been formated as follows
//  10100:startItemName(0)@StartIemId(0):startItemName(1)@StartIemId(1):....startItemName(n)@StartIemId(n)
//   Where n = Number of StartMenu Items found for this Asset..
function populateAssetMenu(req,divName,currentX,currentY,maxItems)
{
    var tout = req.responseText ;
    var theDiv = document.getElementById(divName);
    var nItems = 0 ;
    var startItemList ;
    var itemLink ;
    var disCmd   ;

    // Reply contains no Menu generated command. Ignore and Continue.. 
    if ( tout == NO_MENU_DISPLAY_REPLY )
    {
       return ;
    }
    
    var theMenuTable = document.getElementById(LEFT_NAV_SUBVIEW+"menuColumnTable") ;
    theMenuTable.style.background="#d7d7d7" ;
    theMenuTable.style.border="0px solid #425a89";
    
    assetNEW = tout.split(":")  ;
    for(var m = 0; m < 6; m++)
    {
    	
        itemLink = document.getElementById(LEFT_NAV_SUBVIEW+"popupmenuItemLink_"+m) ;
        itemLink.style.color = "000" ;
        itemLink.style.background="#eee" ;
        itemLink.style.textDecoration="none" ;
    }
    // The Preview GO Link
    itemLink = document.getElementById(LEFT_NAV_SUBVIEW+"menuItemGoLink_6") ;
    itemLink.style.color = "000" ;
    itemLink.style.textDecoration="none" ;
    itemLink.style.background="#eee" ;
        
    for(var m = 0; m < 7; m++)
    {
    	   
        // The Icon and associated menu item Cell
        var rcicon = document.getElementById(LEFT_NAV_SUBVIEW+"rcicon_"+m) ;
        var rcitem = document.getElementById(LEFT_NAV_SUBVIEW+"rcitem_"+m) ;
        var rccas = document.getElementById(LEFT_NAV_SUBVIEW+"rccas_"+m) ;

        rccas.style.background="#eee"  ;
        rcicon.style.background="#d7d7d7" ;

        if ( m == 0 )
        {
           rcicon.style.paddingTop="6px" ;
        }
        else
        {
           if ( m == 5 )
           {
              rcicon.style.paddingBottom="6px" ;
           }
        }

        var itemEnabled =  document.getElementById(LEFT_NAV_SUBVIEW+"menuRowE_"+m) ;
        var itemDisAabled =  document.getElementById(LEFT_NAV_SUBVIEW+"menuRowD_"+m) ;
        var itemCascaded =   document.getElementById(LEFT_NAV_SUBVIEW+"CasE_"+m) ;
        itemDisAabled.style.display="none" ;
        itemEnabled.style.display="none" ;
        itemCascaded.style.display="none" ;

        itemEnabled.style.background="#eee" ;
        if ( tout.charAt(m) == '1')
        {
           itemEnabled.style.display="block" ;
        }
        else
        {
           var mitem=  document.getElementById(LEFT_NAV_SUBVIEW+"popupmenuOutputText_"+m) ;
           mitem.style.color = "#888" ;
           itemDisAabled.style.display="block" ;
        }
    }

    //  If we have more than one Startmenu Items for this Asset
    //  Then Must Populate Cascaded with startmenu item
    if ( assetNEW.length > 2 )
    {
        var copyCascaded = document.getElementById(LEFT_NAV_SUBVIEW+"CasE_0") ;
        copyCascaded.style.display="block" ;

        var newCascaded = document.getElementById(LEFT_NAV_SUBVIEW+"CasE_4") ;
        newCascaded.style.display="block" ;

        // Task B Insure we have not max out our slots Truncate
        if ( assetNEW.length >= maxItems  )
        {
          nItems=maxItems ;
        }
        else
        {
          nItems=assetNEW.length ;
        }

        // Task C  Enabled and Set the menu item names....
        for ( var nx=1 ; nx < nItems ; ++nx )
        {
          var itemPair = assetNEW[nx].split("@")  ;
          startItemList = startItemList+":"+itemPair[1] ;
        }
        var citemLink=document.getElementById(LEFT_NAV_SUBVIEW+"popupmenuItemLink_4") ;
        citemLink.innerHTML="New";         
        document.getElementById("leftNav:startMenuId").value = startItemList;
    }

    // safe plus single new start menu item
     if ( assetNEW.length == 2 )
     {
          var citemLink=document.getElementById(LEFT_NAV_SUBVIEW+"popupmenuItemLink_4") ;
          var itemPair = assetNEW[1].split("@")  ;
          citemLink.innerHTML=itemPair[0];
          disCmd =  itemPair[0];
          document.getElementById("leftNav:startMenuId").value = "******:"+itemPair[1];
     }
     
     var elements = dojo.body().childNodes;
	 for (var i = 0; i < elements.length; i++)
	 {
		if (elements[i].id == divName)
		{
				dojo.body().removeChild(elements[i]);
				break;
		}
	}

       // Lets Locate and get the dimensions of the Left Nav Pane

       var leftNav = document.getElementById("ContentPaneLeftNavLayoutContainer");

       // Menu
       var containerY = parseInt(leftNav.style.top);
       var containerHeight = parseInt(leftNav.style.height);

       var divXpos =  leftNav.offsetLeft ;
       var divYpos =  leftNav.offsetTop ;
       var divHeight = leftNav.offsetHeight;
       var divWidth = leftNav.offsetWidth;

       // The Asset Menu
       var menuHeight = theDiv.offsetHeight;
       var menuWidth = theDiv.offsetWidth;

       var menuYpos = parseInt(currentY+yOffset) ;


       if ( ( menuYpos + 120 ) > containerHeight )
       {
          // Make sure you offset the menu so the cursor is outside the menu div
          // if not the default on context popup menu will be displayed 
          menuYpos = menuYpos-150 ;
          theDiv.style.top=menuYpos-yOffset+"px";

       }
       else
       {
           theDiv.style.top=currentY+yOffset+"px";
       }

    // added to dojo to convince it.
	dojo.body().appendChild(theDiv);

    //set the x position and y position with respect to the mouse position
    theDiv.style.left=currentX+xOffset+"px";

    theDiv.style.background = "#d7d7d7" ;
    theDiv.style.border="1px solid #878586" ;
    theDiv.style.padding="0";
    theDiv.style.display="block";

}



//  Has the click occured on the right mouse button
//  If yes: Perform following
//      1). Set the Asset type and Asset Id
//      2). Use Click X, Y pivot to Determine the Upper Left Location to place the menu
//      3). Add the Menu to body, display menu
//      4). ?  // List of Which Menu Items is Enable or Disabled
//      id="leftNav:menuRowE_0"
function showAssetPullDownMenu(e, divName, assetType, assetId , rootNodeName)
{
    var clickType=e.button;
    // On Right Click Button,  Display the Asset Context Menu
    if (clickType == 2 )
    {
        document.getElementById("leftNav:assetType").value = assetType;
        document.getElementById("leftNav:assetId").value = assetId;
        document.getElementById("leftNav:rootNodeName").value = rootNodeName;
        var  maxItems = document.getElementById("leftNav:maxNewItems").value ;

        // Utilizing the context request Build the URL with Asset Type
        var reqURL=window.document.URL ;
        var pos = reqURL.lastIndexOf('/')  ;
        if ( pos != -1 )
        {
            reqURL=reqURL.substring(0,pos+1)   ;
            var URL= reqURL+"AssetSAFE.jspx?AssetType="+assetType+"="+assetId ;
                 //GABBY
            alert(URL)    ;
        }

        // Since async, Save the event info, since it will be overrided
        // by the time we received the reply from the server
        var currentX=(e.pageX)?e.pageX : e.clientX;
        var currentY=(e.pageY)?e.pageY : e.clientY;
        // Send The request, process reply message to build the menu
        new Ajax.Request(URL, {
                asynchronous:true, 
                onSuccess: function(transport) {populateAssetMenu(transport,divName,currentX,currentY,maxItems);}
        });
    }
}


//  Call send the Request for
//  the CS Element that will craft the
//  Insite Preview Asset URL
//
function requestInsiteAssetURL ( ajaxURL )
{
   new Ajax.Request(ajaxURL, {
                asynchronous:true,
                onSuccess: function(transport)
                {
                    focusInsiteWindow(transport.responseText);
                }
   });
}


// Display a muenu Items cascaded menu of selections
function showCascadedPullDownMenu(divName, currentX, currentY)
{
    if ( !notMapped )
    {
        notMapped = 1 ;
        var theDiv = document.getElementById(divName);

        // What is the max items
        var maxItems = document.getElementById("leftNav:maxNewItems").value ;
        // Hide all cascaded menu items
        for(var m = 0; m < maxItems; m++)
        {
        	
           var itemEnabled =  document.getElementById(LEFT_NAV_SUBVIEW+"Cascaded_"+m) ;
          itemEnabled.style.display="none" ;
        }
        // Populate cascade with associated start New  Parent Asset Def
        for(var i = 1; i < assetNEW.length; i++)
        {
			
           var citem = document.getElementById(LEFT_NAV_SUBVIEW+"Cascaded_"+i) ;
           var citemLink = document.getElementById(LEFT_NAV_SUBVIEW+"casLink_"+i) ;
           var rccasItem = document.getElementById(LEFT_NAV_SUBVIEW+"rccasItem_"+i) ;
           var itemPair = assetNEW[i].split("@")  ;
           citemLink.innerHTML=itemPair[0];

           // Pad and offset the cell equal to the Menu item's parent Cell
           if ( i == 1 ) 
           {
              rccasItem.style.paddingTop =  casTop ;
              rccasItem.style.paddingBottom = casBottom ;
           }
           else
           {
              rccasItem.style.paddingTop = "3px" ;
              rccasItem.style.paddingBottom = "3px" ;
           }
           citem.style.display="block" ;
        }

        //added to dojo to convince it.
        dojo.body().appendChild(theDiv);

        theDiv.style.left=currentX+"px";
        theDiv.style.top=currentY+"px";
        theDiv.style.border="1px solid #878586" ;
        theDiv.style.padding="0";
  
        theDiv.style.display="block";
        notMapped = 1 ;
   }
}


// HighLights the Menu Item Selection
function highLightOn( name, rowId, menuId )
{
    // For good measure turn off  Cascaded Menu items..
    // ???
    highLightOff("menuRowE_0","popupmenuItemLink_0") ;
    highLightOff("menuRowE_4","popupmenuItemLink_4") ;
    var rowItem = document.getElementById(LEFT_NAV_SUBVIEW+rowId) ;
    rowItem.style.background = "#d7d7d7" ;
    var item = document.getElementById(LEFT_NAV_SUBVIEW+menuId) ;
    item.style.color = "#000"  ;
    item.style.background = "#d7d7d7";
    item.style.textDecoration="none" ;
    crntMenuOn = name ;
    var menuItem = document.getElementById(LEFT_NAV_SUBVIEW+"menuItemName") ;
    menuItem.value = name ;

    // Determine the Cascaded Cell
    var cas = rowId.split("_");
    
   
    var rccas = document.getElementById(LEFT_NAV_SUBVIEW+"rccas_"+cas[1]) ;
    rccas.style.background = "#d7d7d7";

    // If the menu item has a cascaded Menu.. Do we need to hide
    if ( menuId.search("popupmenuItemLink_0") == -1  &&  menuId.search("popupmenuItemLink_4") == -1 )
    {
       var theCasDiv=document.getElementById(ASSET_TREE_CASCADED_PULLDOWN_MENU) ;
       notMapped=0 ;
       theCasDiv.style.display = 'none';
    }
    var itemCas = document.getElementById(LEFT_NAV_SUBVIEW+"CasE_"+cas[1]) ;
    // Does this Parent menu item have a Cascaded Button ?
    if ( itemCas.style.display == "block" &&  notMapped == 0 )
    {
        // Get the Asset Menu Dimensions
        var theDiv=document.getElementById(ASSET_TREE_PULLDOWN_MENU);

        var divXpos =  theDiv.offsetLeft ;
        var divYpos =  theDiv.offsetTop ;
        var divHeight = theDiv.offsetHeight;
        var divWidth = theDiv.offsetWidth;

        // Determine the menu item's absolute xy origin position
        var  xyorg = findPos(rowItem) ;

        // Get the menu item's  BOUNDING BOX Dimensions
        var rowHeight = rowItem.offsetHeight;
        var rowWidth = rowItem.offsetWidth;
        var rowXpos = rowItem.offsetLeft;
        var rowYpos = rowItem.offsetTop;

        // Set the Absolute Position of the Cascaded Menu
        var xOrg = xyorg[0]+divWidth ;
        var yOrg = xyorg[1] ;

        // Cascaded menu items are not fixed IN OFFSETS depending on position
        // Save the Offsets used for highlighting the cascaded menu items
        if ( name == "Copy" )
        {
            casBottom ="6px" ;
            casTop = "4px"   ;
        }
        else
        {
             casBottom ="3px" ;
             casTop = "2px"   ;            
        }
        // Show the Cascaded Menu
        showCascadedPullDownMenu(ASSET_TREE_CASCADED_PULLDOWN_MENU,xOrg,yOrg) ;
    }
}



// HighLights the Menu / Menu Child Item Selection
function highLightOff( rowId, menuId )
{
    var rowItem = document.getElementById(LEFT_NAV_SUBVIEW+rowId) ;
    rowItem.style.background = "#eee" ;
    var item =  document.getElementById(LEFT_NAV_SUBVIEW+menuId) ;
    item.style.color = "#000"  ;
    item.style.background = "#eee" ;
    item.style.textDecoration="none" ;
    // Determine the Cascaded Cell is Mapped
    var cas = rowId.split("_");
   
    var rccas = document.getElementById(LEFT_NAV_SUBVIEW+"rccas_"+cas[1]) ;
    rccas.style.background = "#eee";
}
// HighLights as selected the Cascaded Menu Item Selection
function highLightCascadeOn(  name, rowId, menuId )
{
    var item = document.getElementById(LEFT_NAV_SUBVIEW+menuId) ;
    item.style.color = "#000"  ;
    var rowItem = document.getElementById(LEFT_NAV_SUBVIEW+rowId) ;
    rowItem.style.background = "#d7d7d7" ;
    item.style.background = "#d7d7d7" ;
    

    if ( crntMenuOn != null )
    {
      if ( crntMenuOn.search("Copy") != -1 )
      {
           highLightOn("Copy","menuRowE_0","popupmenuItemLink_0") ;
      }
      else
      {
        if ( crntMenuOn.search("New") != -1 )
        {

             highLightOn("New","menuRowE_4","popupmenuItemLink_4") ;
        }
      }
    }
}


// HighLights the Menu / Menu Child Item Selection
function highLightCascadeOff( rowId, menuId )
{
    var rowItem = document.getElementById(LEFT_NAV_SUBVIEW+rowId) ;
    rowItem.style.background = "#eee" ;
    var item =  document.getElementById(LEFT_NAV_SUBVIEW+menuId) ;
    item.style.color = "#000"  ;
    item.style.background = "#eee" ;
    item.style.textDecoration="none" ;

    // Determine the Cascaded Cell
    var cas = rowId.split("_");
    var rccas = document.getElementById(LEFT_NAV_SUBVIEW+"rccas_"+cas[1]) ;  
    rccas.style.background = "#eee";
}

// Hides any Div  and Associated Selected Pulldown Menu child item
//onmouseup="hidePullDownMenu('CascadedMenu','#{citem.id}', '#{citem.rowId}')"
function hidePullDownMenu(divName,rowId, menuId)
{
    /*var theDiv=document.getElementById(ASSET_TREE_PULLDOWN_MENU);
    var theCasDiv=document.getElementById(ASSET_TREE_CASCADED_PULLDOWN_MENU) ;
    
    if(theDiv)
	{
       // Don't forget to Hight light OFF the selection
       // This will insure that WHEN USER displays Menu Previous Item is not highlighted 
       highLightOff(rowId, menuId) ;
       theDiv.style.display = 'none';
       theCasDiv.style.display = 'none';
       notMapped = 0 ; 
    }
    if(isIE)
    setCEVFormCursor() ;    */           

              
}

//  Context Click Outside Asset Tree Hide the Right-Click Asset Tree Menu / Cascade
function checkAssetMenu()
{
  var theDiv=document.getElementById(ASSET_TREE_PULLDOWN_MENU) ;
  var theCasDiv=document.getElementById(ASSET_TREE_CASCADED_PULLDOWN_MENU) ;
  if(theDiv)
  {
    theDiv.style.display = 'none';
    theCasDiv.style.display = 'none';
    notMapped = 0 ; 
  }
}


function resizeSearchPopupIfAvailable()
{
	var div = document.getElementById("searchpopupdiv");
	if(div && div.style.display == 'block')
	{
		var table = div.childNodes[0];
		if(table)
		{
			var tableHeight = table.offsetHeight;   		
   		    var tableWidth = table.offsetWidth;
   		    var popupOffsetHeight = 400;
   		    if(isIE)
				{
					popupOffsetHeight = 386;
				}
		   	if(tableHeight > popupOffsetHeight)
			{
				div.style.height="400px";	
				if(isIE)
				{
					tableWidth = tableWidth + 15;
				}			
				div.style.width= tableWidth + 16+"px";	
				div.style.overflow ="auto";				
					
			} else
			{
				if(isIE)
				{
					tableHeight = tableHeight + 14;
					tableWidth = tableWidth + 12;
				}
			    div.style.height= tableHeight +"px";			    
				div.style.width= tableWidth +"px";
				div.style.overflow ="hidden";
				
			}  
			var theiFrame= document.getElementById('myiFrame');
			theiFrame.style.height = div.offsetHeight + 2;
		    theiFrame.style.width = div.offsetWidth + 2;
		    	     
		}
	}
}


var showsearchpopup ="false";

// Utility to  display and shpw a Div Container, Menu, etc
// Create an iframe and place it behind the div.
// If we did not do this,div will not show the dropdown elements from the main page when dragged over.
// This problem is just in IE.
function showSearchPopUp(divName , searchLink ,searchField)
{
	var theDiv = document.getElementById(divName);
    popupSearchField = searchField;
	popupSearchLink = searchLink;

    //window.top.console.start("showSearchPopUp() div Name :"+divName)  ;

    if(theDiv)
	{
        var linkButton = document.getElementById(linkAssetButtonId);
        // Lets set the PopUp Selected Asset to Correct Source image
        if ( null != linkButton )
        {
          linkButton.src = linkImageSrc ;
        }
        showsearchpopup = true;
		//set the x position and y position with respect to the mouse position			
										         
   		var container = document.getElementById("ContentPaneMainBody");
   		var topContainer = document.getElementById("ContentPaneSearch");
   		var leftContainer = document.getElementById("ContentPaneLeftNavLayoutContainer");   		
   		var containerY = parseInt(container.style.top);   		
   		var containerHeight = parseInt(container.style.height);


        theDiv.style.display="block";
   		var divHeight = theDiv.offsetHeight;
   		var divWidth = theDiv.offsetWidth;
   		var leftNavWidth = leftContainer.offsetWidth;
   		var containerWidth = container.offsetWidth;
   		
   		var div = document.getElementById("searchpopupdiv");

        if(!div)
   		{
            div = document.createElement("div");
			// do not change the id.The id is used in java.
   			div.id = "searchpopupdiv";
   			div.style.position="absolute";
   			div.style.border="1px solid #425a89"; 
			div.style.padding="5px"; 
			div.style.paddingTop="5px";
			div.style.paddingBottom="7px"; 
			div.style.backgroundColor="#fff";
			div.style.overflow="auto";
			div.style.display="none";   
			div.style.zIndex = "0";
			div.style.zIndex=998; // Make sure the index is greater than the index of Iframe
		    
		    theiFrame=document.createElement('iFrame');
			theiFrame.id='myiFrame';
			theiFrame.style.filter='alpha(opacity=0)';
			theiFrame.style.position='absolute';
			theiFrame.style.zIndex=997; 
			document.body.appendChild(theiFrame);
			theiFrame.style.border="none";
			var formElement = 	document.getElementById("mainForm");
   			formElement.appendChild(div);   
   		}
   		
   		theiFrame= document.getElementById('myiFrame');
   		theiFrame.style.display='block';
        
        div.style.display="block";
   		div.innerHTML = theDiv.innerHTML;  
   		theDiv.innerHTML = "";

        // Resize and wrap the popup div arround the search
        // results table. 
        resizeSearchPopup() ;

        var divX = sourceX + 20;
   		if(containerWidth > divWidth )
   		{
   			if(!containerWidth >= divX + divWidth)
   			{
   				divX =  leftNavWidth + (containerWidth - divWidth) - 20; 
   			}   			
   		} else
   		{
   			divX = leftNavWidth - (divWidth - containerWidth) -20;
   		}
   		var topContainerHeight = 0;
   		if(topContainer)
   		{
   			topContainerHeight = topContainer.offsetHeight;   			
   		}
   		
   		var spaceAbove = sourceY - containerY + topContainerHeight;   		
   		var spaceBelow = containerHeight - sourceY;
   		theDiv.style.display="none";
   		var divY = 	sourceY + 15
   		if(spaceBelow > divHeight)
   		{
   			divY = sourceY + 30;
   		}	
   		else if(spaceAbove > divHeight)
   		{
   			
   			divY = sourceY - divHeight - 75;
   		}

        // Is it Search Assets Popup or Find Replace Dialog Control
        if (divName.search(/RegExpr/) > 0)
        {
           divX = ( divX - divWidth )-125 ;
           divY = ( divY - divHeight/2 )  ;   
        }

        div.style.left= divX;
   		div.style.top= divY;

        theiFrame.style.height = div.offsetHeight + 2;
		theiFrame.style.width = div.offsetWidth + 2;
		theiFrame.style.top= divY - 1; //one less than the value given for theDiv
		theiFrame.style.left= divX - 1; //one less than the value given for theDiv   
   		
	}
}

function refreshSearchPopUp()
{
	var div = document.getElementById("searchpopupdiv"); 
    if(div && showsearchpopup=="false")
	{
		div.style.display="none";
        var iframe = document.getElementById("myiFrame");	
		iframe.style.display="none";
    }
	showsearchpopup = "false";
}

function setShowSearchPopup(popupdiv)
{
	showsearchpopup ="true";
	var sourcediv = document.getElementById(popupdiv);	
	var div = document.getElementById("searchpopupdiv");	
	div.innerHTML = sourcediv.innerHTML;
	sourcediv.innerHTML = "";
}

function setPositionForDragAndDrop(e)
{
	sourceX=(e.pageX)?e.pageX : event.clientX;
	sourceY=(e.pageY)?e.pageY : event.clientY;
	sourceX=sourceX;
	sourceY=sourceY;

    //window.top.console.start("setPositionForDragAndDrop()  sourceX:"+sourceX+", sourceY:"+sourceY) ;
    
    //alert("<in> cs.js::setPositionForDragAndDrop() sourceX:"+sourceX+", sourceY:"+sourceY) ;
    
}

function drag(e , divId)
{
	if(isIE)
	{
		sourceNode = event.srcElement.nodeName;
	} else
	{
		sourceNode = e.target.nodeName;
	}
	if(sourceNode.toLowerCase() != "td")
		return;
 	theDiv = document.getElementById(divId); 
	theDiv = document.getElementById(divId);  
	offsetx=isIE ? event.clientX : e.clientX;
	offsety=isIE ? event.clientY : e.clientY;	
	nowX=parseInt(theDiv.style.left);
	nowY=parseInt(theDiv.style.top);
	dragEnabled =true;
	
	 if(isNaN(nowX)) nowX= offsetx;
	 if(isNaN(nowY)) nowY= offsety;  
	 document.onmousemove=dragdrop;

}

function dragdrop(e)
{
  if (!dragEnabled) return;
  theDiv.style.left=isIE ? nowX+event.clientX-offsetx : nowX+e.clientX-offsetx;   
  theDiv.style.top=isIE ? nowY+event.clientY-offsety : nowY+e.clientY-offsety;  
  theiFrame= document.getElementById('myiFrame');
  theiFrame.style.left=isIE ? nowX+event.clientX-offsetx-1 : nowX+e.clientX-offsetx-1;   
  theiFrame.style.top=isIE ? nowY+event.clientY-offsety-1 : nowY+e.clientY-offsety-1; 
  return false;  
}

       
document.onmouseup=Function("dragEnabled=false");


// The following helper functions  are used for Staged Delete for References
// Controls and Select / De-Select checkbox the associated Link Boxes
//
function stageRefAsset ( checkbox, name, parent, offset )
{
    var ckParts = checkbox.id.split(":") ;
    var ckend = parseInt(parent)+parseInt(offset) ;
    var ckstart = parseInt(parent) ;	 
    // Select or Deselect the associated Refernce Asset Children check boxes
    for ( ckbox = ckstart ;  ckbox < ckend ;  ++ckbox)
    {
        var sck=ckParts[0]+":"+ckParts[1]+":"+ckbox ;
        var cksib=document.getElementById(sck) ;
        cksib.checked = checkbox.checked ;
    }
}


// Set the Dimensions of the Screen..
function findScreenDim ()
{
   var screenDim = document.getElementById("screenDim") ;
   // Get the Width and Height of the Default Display Screen
   if ( screenDim != null )
   {
       screenDim.value = screen.availWidth+":"+screen.availHeight   ;
   }
}

// Creates image beacon to invoke analytics audit integration sensor path
function populateAnalytics(assetName,siteName,UobjType,id,TsessionID,optype,csuserid,statisticSensorPath) {
    var jsParam = '';
	if(self.StatPixelParamFromPage) {jsParam=StatPixelParamFromPage;};
	var color = 'n/a';
	var size = 'n/a';
	var puri = '';
	var java = navigator.javaEnabled();
	var nav = navigator.appName;
	var agent = navigator.userAgent;
	var objValue = '';
    var ref = document.referrer;
    var write = (navigator.appName=='Netscape' && (navigator.appVersion.charAt(0)=='2' || navigator.appVersion.charAt(0)=='3')) ? false:true;
    if (write==true) {
        size = screen.width + "x" + screen.height;
		color = (nav!='Netscape') ? screen.colorDepth:screen.pixelDepth;
		objValue= encodeURIComponent(assetName);
        puri = '?siteName='+siteName+'&objType='+UobjType+'&objID='+id+'&objName='+objValue+'&sessionID='+ TsessionID+'&Referer='+ref.replace(/&/g, "[amp]")+'&nav='+nav+'&agent='+agent+'&size='+size+'&color='+color+'&java='+java+'&js=true'+jsParam+'&optype='+ optype+'&csuserid='+csuserid;
        pixel = new Image();
        pixel.src = statisticSensorPath + puri;
    }
}

function setClientBaseUrl()
{
	

	if (isClientUrlSet == false)
	{
		var reqURL=window.document.URL ;
	
		var pos = reqURL.lastIndexOf('/')  ;
		if ( pos != -1 )
		{
	    	reqURL=reqURL.substring(0,pos+1)   ;
		}
		var URL= reqURL+"clientBaseUrl.jspx?clientBaseUrl="+location.protocol + "//" + location.host;
	
		new Ajax.Request(URL, {
			asynchronous:true
		});
		isClientUrlSet = true;
	}
}
/*
 * Add onLoad listener to the Hidden pprIframe to monitor the session.
 * If the session timesout pprIframe is redirected to CAS login page
 */
function addOnLoadListener()
{
	var iFrame = document.getElementById('_pprIFrame');
	// Internet explorer does not work if we assign the function.so use attachevent
	if(iFrame)
	{
		if(iFrame.attachEvent)
		{
			iFrame.attachEvent('onload',checkSession);
		}	
		else 
		{
			iFrame.onload=checkSession;									
		}				
	}
	
}
/*
 *	URL of pprIframe is same as dash.jspx.
 *	If session is timedout CAS filter redirects the page to cas login page.
 *	URL of the pprIframe is changed from dash.jspx to cas login page.
 *	If the URL is different refresh the parent page.
 *	Use window.top so that even if dash is in WEM framework it will maintain its context
 *	If cas is installed in a different domain accessing location object will throw error.
 *	On error reload the top page which will redirect to the login page						
*/
function checkSession() {						
	var iFrame = window.frames['_pprIFrame'];
	try 
	{
		var IframeUrl = iFrame.location.href;
		if(IframeUrl.indexOf("?service=") > 0)
		{
			window.top.location.href = window.top.location.href;
		}	
	}
	 catch(err)
	{
		window.top.location.href = window.top.location.href;
	}
	
}		


/*****************************************************************************************
 * Determines what mode the Main Window Body to be displayed and
   automatically sets and places the cursor to the first Element of type
   Input and text.

 * @param  none 
 Modified:  Jan 26, 2010  
 Reason:     PR# 21223   Fixed javascript error in  Asset edit mode when the edit pane contains 
                   no input field attributes and in essence "Can't move focus to the input control" field 
				    
	        PR#21398 For assetmaker assets and multi valued attributes, 
	        fixed the  following case 
	        for  Multi valued attributes : checkbox will show as checkbox and the 
	        checkbox attribute editor should show up as checkbox. In the past 
	         we displayed Single Value radio buttons.


 **************************************************************************************/
function setCEVFormCursor()
{
    var cevScreenA = document.getElementById(MAIN_SUBVIEW + 'lowerTabTabs');
    if( cevScreenA == null)
    {
       return ;
    }
    // Get a List OF All Elemets input tags
    var inputTags = cevScreenA.getElementsByTagName("input");
    for (i = 0; i < inputTags.length; i++)
    {
         var objEditorI = inputTags[i];
         var objId = objEditorI.id;
         var objtype = objEditorI.type;
         var readOnly= objEditorI.readOnly ;
         var value = objEditorI.value ;
	     if (objtype == "text" && !readOnly &&
             !(objId.search(/searchText/) > 0))
         {
            // For what ever strange reason, focus must be called twice off the input element
            // to force or insure the cursor will be position in the input text field
			objEditorI.blur() ;
            objEditorI.focus();
            objEditorI.focus();

            break ;
         }
     }
     return ;
}


//delete table row from dnd list
function deleteRow(delRowId, dragListId)
{
	var elem = dojo.byId(delRowId);
	elem.parentNode.removeChild(elem);
		
	var draggedListVals = dojo.query("table#" + dragListId + " > tr");	
	var orderedAssetIds = "";
	var tdElem = "";

    for(var x = 0; x < draggedListVals.length; x++)
    {
    	if (x==0)
    	{
    		tdElem = draggedListVals[x].getElementsByTagName("td");
    		orderedAssetIds = tdElem[0].id + "!" + tdElem[0].innerHTML;
    	}
    	else
    	{
    		tdElem = draggedListVals[x].getElementsByTagName("td");
    	 	orderedAssetIds = orderedAssetIds + "," + tdElem[0].id + "!" + tdElem[0].innerHTML;;
   		}
    }
    dojo.byId("hiddenField_" + dragListId).value = orderedAssetIds;
}

// Creates image beacon to invoke analytics audit integration sensor path
function populateAnalytics(assetName,siteName,UobjType,id,TsessionID,optype,csuserid,statisticSensorPath) {
    var jsParam = '';
	if(self.StatPixelParamFromPage) {jsParam=StatPixelParamFromPage;};
	var color = 'n/a';
	var size = 'n/a';
	var puri = '';
	var java = navigator.javaEnabled();
	var nav = navigator.appName;
	var agent = navigator.userAgent;
	var objValue = '';
    var ref = document.referrer;
    var write = (navigator.appName=='Netscape' && (navigator.appVersion.charAt(0)=='2' || navigator.appVersion.charAt(0)=='3')) ? false:true;
    if (write==true) {
        size = screen.width + "x" + screen.height;
		color = (nav!='Netscape') ? screen.colorDepth:screen.pixelDepth;
		objValue= encodeURIComponent(assetName);
        puri = '?siteName='+siteName+'&objType='+UobjType+'&objID='+id+'&objName='+objValue+'&sessionID='+ TsessionID+'&Referer='+ref.replace(/&/g, "[amp]")+'&nav='+nav+'&agent='+agent+'&size='+size+'&color='+color+'&java='+java+'&js=true'+jsParam+'&optype='+ optype+'&csuserid='+csuserid;
        pixel = new Image();
        pixel.src = statisticSensorPath + puri;
        //alert(pixel.src);
    }
}




function displayLookAheadPulldown(divName) {
        var theDiv = document.getElementById(divName);

        //alert("displayLookAheadPulldown() div name:"+divName+", id:"+theDiv)  ;

        if(theDiv)
        {
             //alert("showTip() div name:"+divName+", id:"+theDiv.id+",display:"+theDiv.style.display)  ;

               if ( theDiv.style.display == "block"  )
              {
                theDiv.style.display = 'none';
              }
            else
                {
                theDiv.style.display = 'block';
              }
       }
}


// New Filter Request Narrow the List Baby 
function handleFilterKeyUp ( ctlInputName ) {
  var theInput = document.getElementById(MAIN_SUBVIEW+ctlInputName);

  //alert("handleFilterKeyUp() input name:"+theInput+", id:"+theInput )  ;

  if(theInput )
           {
                  //alert("handleFilterKeyUp() div name:"+ctlInputName+", id:"+theInput.id+",value:"+theInput.value)  ;
           }
}

//set client request base url
function setClientBaseUrl()
{
	if (isClientUrlSet == false)
	{
		var reqURL=window.document.URL ;
	
		var pos = reqURL.lastIndexOf('/')  ;
		if ( pos != -1 )
		{
	    	reqURL=reqURL.substring(0,pos+1)   ;
		}
		var URL= reqURL+"clientBaseUrl.jspx?clientBaseUrl="+location.protocol + "//" + location.host;
	
		new Ajax.Request(URL, {
			asynchronous:true
		});
		isClientUrlSet = true;
	}
}



/**********************************************************************************************************************
 *     Following are the helper and utility functions to
 *     implement DND across Iframe Applications
 *     Utilizes : The Asset Data Model Object Passing defined in fwdnd.js
 *
 *     Updated: August 05, 2009
 *      1) Pass the Drag AssetID Object Reference
 *      2) Avatar: Override Avatar is kind of ghost image of the object List of Assets being dragged.
           This ghost image follows the mouse cursor, till the time it is dropped to a proper Source/Target
           placeholder. In Dojo this functionality is available to every drag and drop operation by default.
           The 'Dojo.dnd.Avatar' class does all this for you and displays a nice tooltop with text 'moving' or 'copying',
           indicating the type of operation being performed.

 *********************************************************************************************************************/

// UC1 Load The Asset Tree
function populateAssetTree(req)
{
    var tout = req.responseText ;
    // Parse and extract the Assets we are going to drag
    var assetTypeLst = tout.split("$") ;

    //FIXME: embed in params
    var srcContainer = dojo.byId('dragListUC1Assets');

    // Transverse the asset types
    for ( var n=0  ; n < assetTypeLst.length ; ++n )
    {
      var  assetLst = assetTypeLst[n].split("+") ;

      // Transverse the assets of this specific type
      for(var m = 0; m < assetLst.length-1 ;  m++)
      {
        // Parse and extract the Assets we are going to drag
        var asset = assetLst[m].split("#")  ;
         // Create the DND Row
        var trow1 = document.createElement("tr");
        trow1.setAttribute("id", asset[0]);
        trow1.setAttribute("name", asset[0]);
        trow1.setAttribute("className", "dojoDndItem");
        trow1.setAttribute("dndType", "uc1assetid");

        // Create the Asset Name TD CELL 0
        var td1 = document.createElement("td");
        td1.setAttribute("id", asset[1]);
        td1.setAttribute("name", asset[1]);

        // The Asset Name
        td1.innerHTML=asset[2] ;

        // Create the Asset Name TD CELL 0
        var td2 = document.createElement("td");

        // The Asset Image Cell
        var img0 = document.createElement("img");
        // Parse and craft the source inage file
        var imgSrcLst = asset[1].split("!") ;
        var imgSrc = imgSrcLst[1].split(":") ;

        //if ( m == 0 )
          //console.log(' f$(populateAssetTree) Loading.. Asset Type:'+imgSrc[0]+", Total Assets:"+(assetLst.length-1) );

        img0.setAttribute("src", "/cs/Xcelerate/OMTree/TreeImages/DashAssetTypes/"+imgSrc[0]+".gif" );
        td2.appendChild(img0) ;

        trow1.appendChild(td1) ;
        trow1.appendChild(td2) ;

        var tbody = srcContainer.getElementsByTagName('tbody')[0];
        tbody.appendChild(trow1)  ;

       }  // End for   Asset
    } // End for  Asset Type

    initDND() ;
}





// Query CS Database Associated Asset Type Tables to Generate a List of Assets
function loadAssetTree()
{
      // Utilizing the context request Build the URL with Asset Type
      var reqURL=window.document.URL ;
      var pos = reqURL.lastIndexOf('/')  ;
      if ( pos != -1 )
      {
          reqURL=reqURL.substring(0,pos+1)   ;
          //var URL= "http://jgranato-330:8080/cs/ContentServer?pagename=OpenMarket/Xcelerate/UIFramework/GetListOfAssets" ;
          var URL= "http://jgranato-330:8080/cs/ContentServer?pagename=OpenMarket/Xcelerate/UIFramework/UC1" ;
      }
      // Send The request, process reply message to build the menu
       new Ajax.Request(URL, {
               asynchronous:false,
               onSuccess: function(transport) {populateAssetTree(transport);}
       });

}

 // Research DOJO Drag and Drop
 // This function will initilize, populate and subscribe to events
function initDND()
{
      var c1,target1;

       // Uses New DND extended Source Object
      c1 = new SuperDndSource("dragListUC1Assets",{accept: ["uc1assetid"],copyOnly:false});

      var handlerNode = dojo.byId("dragListUC1Assets");
      var nodes =  dojo.query(".dndContainer")  ;

      //console.log('$f(initDND) OnLoad Completed') ;
}


// Do the transfer of Assets to all participating sources
// Construct the Drag Box from the List of Asset Objects
// Input:  Asset Drag Object fron Source Application
/* function  _transferSource( dndAsset )
{
    //cosole.log(' $fDash(_transferSource) ') ;

    if (div && div.parentNode)
       div.parentNode.removeChild(div); //cleanup

    // Dragable Container
    div = document.createElement('div');

    div.style['display'] = 'none';

    // alert("DNDAssetId$_Ref name:"+dndAsset.getName()+",id:"+dndAsset.getUniqueId()+",src:"+dndAsset.getSrc() )   ;

    // Create the Source Top Level Asset List Table
    var table = document.createElement('table');
    dojo.addClass(table, 'UC1dndContainer');
    table.setAttribute("name","dragListAssets")

    // Create the Single Cell Row
    var tritem = document.createElement('tr');
    dojo.addClass(tritem, 'dojoDndItem');

    // Set element dnd attributes from generic Asset Drag Type
    tritem.setAttribute("id",dndAsset.getUniqueId());
    tritem.setAttribute("name",dndAsset.getUniqueId());
    // Set the DND type
    tritem.setAttribute("dndType", dndAsset._dndType) ;

    // Create Asset Name Cell To Hold Asset Name
    var tditem0 = document.createElement('td');
    tditem0.setAttribute("id", dndAsset.getUniqueTdNode())   ;
    tditem0.setAttribute("name", dndAsset.getUniqueTdNode())   ;
    tditem0.innerHTML = dndAsset.getName() ;

    // Create AssetType Image Cell To Hold Asset type image
    var tditem1= document.createElement('td');
    var clickNode = document.createElement('a');

    var imgType =  document.createElement('img') ;
    imgType.src = dndAsset.getSrc();
    imgType.setAttribute("align","middle" )   ;

    clickNode.appendChild(imgType);
    tditem1.appendChild(clickNode);

    tritem.appendChild(tditem0);
    tritem.appendChild(tditem1);

    table.appendChild(tritem);
    div.appendChild(table);

    dojo.body().appendChild(div);

    // Set the Source with the Type
    var s = new dojo.dnd.Source(table, {accept: [dndAsset._dndType]});
    var m = dojo.dnd.manager();
    // Start Dragging Avatar in action Managed by dnd manager..
    m.startDrag(s, [tritem], false);
}

*/




function hideTip(divName)
{
    var theDiv = document.getElementById(divName);
    if(theDiv)
	{
         //window.top.console.log("cs.js::hideTip() divName:"+divName) ;
        
         theDiv.style.display = 'none';
	}
}

/**********************************************************************************************************************
 *     Following is  the Action Call Back on The Find and replace Apply
       Callback. This will set the Apply Hidden field to true
       The field is used to determine if the find is  replaced with text
       at Bulk Asset Save time.

       Updated: Nov 16, 2009

 *********************************************************************************************************************/

function applyBulkFindReplace ( divName, applyField )
{
    var theDiv = document.getElementById(divName);
    var applyUIComp = document.getElementById(applyField);

    //window.top.console.start("cs.js::applyBulkFindReplace() ") ;
    //window.top.console.log("   divName      : "+divName) ;
    //window.top.console.log("   applyField id: "+applyField) ;

    if ( applyUIComp )
    {
        //window.top.console.log("   applyField value: true") ;
        applyUIComp.value = "true"    ;
    }

    hideTip(divName)   ;
}


/*************************************************
       Global Variables
**************************************************/

var  prevUserSeletedTab = "none" ;

/*************************************************************************************************
                           discloseDOJOTabSelection
 Reason:  User has Clicked on DOJO Scrollable Tab  Toolbar Action Command Button:
          Disclose the Tab Faces Disclosure Radio Dial Button ?
 Modified: Dec 16, 2009

 Modified: Dec 21, 2009
 Reason:   When Initial Selected Tab is Disclosed Display Node the Radio Dial And Lets see if
           we can fire the trigger.

           This is the id of the Radio Dial Container Parent containing all
           of the radio dials

           ID -> contentPaneMainBodySubView:lowerTabTabs_sor_tbl

           Lets Make This dynamic  Crafted of the Tab Name

****************************************************************************************************/

var  __scInner   ;

function discloseDOJOTabSelection( tabName)
{
   //window.top.console.start("setHello.onclick().Tabid@");
   var  triggerButton = MAIN_SUBVIEW+tabName ;
   // alert("<in> cs.js::discloseDOJOTabSelection()@"+tabName) ;

   var radioNode = 'contentPaneMainBodySubView:'+tabName ; // __DYN_TABfatwire_Alloy_Tabs_Metadatar' ;
   var triggerParams =  {event:'show',source:radioNode} ;



  //  alert(scrollBarDiv.innerHTML)   ;
    
  //  alert("")
   if ( tabName != '__DYN_TABfatwire_Alloy_Tabs_Content')
      _submitPartialChange('mainForm',1, triggerParams );
   else
   {
       var scrollBarDiv = dojo.byId('ScrollingParent')  ;

       __scInner = scrollBarDiv.innerHTML   ;

      // alert(scrollBarDiv.innerHTML) ;
      // scrollBarDiv.innerHTML =  __scInner ;

   }
    
    /*  if ( tabName == '__DYN_TABfatwire_Alloy_Tabs_Content')
      {
          _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Content'});
          return true;


      }
      if ( tabName == '__DYN_TABfatwire_Alloy_Tabs_Metadata')
      {

          var node = 'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Metadatar' ;

         // var metaParams =  {event:'show',source:'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Metadata'} ;
           var metaParams =  {event:'show',source:node} ;

           alert('_Metadata')    ;
           _submitPartialChange('mainForm',1, metaParams);
          return true;

      }
       if ( tabName == '__DYN_TABfatwire_Alloy_Tabs_Relations' )
       {
           // alert('Relations')    ;
           _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Relations'});
           return true;

       }
        if ( tabName == 'versionsDI' )
        {
            _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:versionsDI'});
            // alert('versionsDI')    ;
            return true;

        }
         if ( tabName == 'publishingDI' )
         {
             _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:publishingDI'});
            // alert('publishingDI')    ;
          return true;
         }
         */



}


function hideRadioTable ()
{
   var radioTable = document.getElementById("contentPaneMainBodySubView:lowerTabTabs_sor_tbl") ;

   if ( radioTable != null )
   {
           //alert("<in> cs.js::hideRadioTable()@RadioTable@"+radioTable.id) ;
           radioTable.style.display="none" ;

           var scrollBarDiv = dojo.byId('ScrollingParent')  ;

           if ( scrollBarDiv != null )
           {
               //alert(" hideRadioTable()"+scrollBarDiv.innerHTML)  ;
           }
           //dojo.parser.parse('ScrollingParent') ;
   }
}
  

// Utility to  display and show the Triggering Cells Container, Menu, etc
// Create an iframe and place it behind the div.
// If we did not do this,div will not show the dropdown elements from the main page when dragged over.
// This problem is just in IE.
/* function discloseTabScrollerTab(tabName, tabRootCore )
{
      alert("<in> discloseTabScrollerTab DiscloseTab@"+tabName)    ;
      var tabRootCoreCell = document.getElementById(MAIN_SUBVIEW+tabRootCore) ;
      // What Tab Container to Disclose
     // alert("<in> discloseTabScrollerTab  disclose Tab@"+tabName+"@"+tabRootCore)    ;
      //  itemEnabled.style.display="none" ;
      // itemCascaded.style.display="none" ;               
      if ( tabRootCoreCell != null )
      {
          // Conceal the Previous Selected and Disclosed tab dude

            var prevTabCoreCell =  document.getElementById(MAIN_SUBVIEW+prevUserSeletedTab) ;

            if ( prevTabCoreCell != null )
            {
               prevTabCoreCell.style.display=  "none" ;
            }
            //alert("<in> discloseTabScrollerTab Node Tab DISPLAY@"+tabRootCoreCell.id)    ;
            // Disclosed the Cell
            tabRootCoreCell.style.display=  "block" ;

            prevUserSeletedTab=tabRootCore ;

            // Lets Hide the ToolBar Table

            var tabToolBarTable=document.getElementById(MAIN_SUBVIEW+"headerTabsPanel_TabsTriggerDiscloseTable") ;
            if ( tabToolBarTable != null )
            {
               alert("<in> discloseTabScrollerTab Table Found@"+tabToolBarTable.id) ;

               //_submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Metadata'});return true;
               //tabToolBarTable.style.display="none" ;
            }

      }
      else
      {
          alert ("<in> discloseTabScrollerTab ERROR NoRootCell")   ;
      }
}

*/
/*
    The Following are the  TabName and Associated Partial Triggers

    MetaData   _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Metadata'});return true;




    Relations  _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Relations'});return true;


    Versions   _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:versionsDI'});return true;

    Marketing  _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:__DYN_TABfatwire_Alloy_Tabs_Marketing'});return true;

    Workflow  _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:workflowDI'});return true;

    Publishing _submitPartialChange('mainForm',1, {event:'show',source:'contentPaneMainBodySubView:publishingDI'});return true;

*/




/**********************************************************************************
 *
 *   The support transfer  Drag and Drop Asset Node
     Source from external Iframe application
     where both Source DND Tree as the Node source.
     
     Passed in is the Asset Object to be droped
     across APPLICATION Iframe(s)

 *   Modified              Reason
 *  ----------             ------
 *
 *
 *   March 04, 2010
***************************************************************************************/

// Accepts the Asset Dragged from external Iframe App hence UC1
function  _transferSource( dndAsset )
{
    console.log(' <TR> $cs.js:Dash(_transferSource) assetID:'+dndAsset.getUniqueId()+",name:"+dndAsset.getName() ) ;

    if (div && div.parentNode)
       div.parentNode.removeChild(div); //cleanup

    // Dragable Container
    div = document.createElement('div');

    div.style['display'] = 'none';

    // alert("DNDAssetId$_Ref name:"+dndAsset.getName()+",id:"+dndAsset.getUniqueId()+",src:"+dndAsset.getSrc() )   ;

    // Create the Source Top Level Asset List Table
    var table = document.createElement('table');
    dojo.addClass(table, 'UC1dndContainer');
    table.setAttribute("name","dragListAssets")

    // Create the Single Cell Row
    var tritem = document.createElement('tr');
    dojo.addClass(tritem, 'dojoDndItem');

    // Set element dnd attributes from generic Asset Drag Type
    tritem.setAttribute("id",dndAsset.getUniqueId());
    tritem.setAttribute("name",dndAsset.getUniqueId());
    // Set the DND type
    tritem.setAttribute("dndType", dndAsset._dndType) ;

    // Create Asset Name Cell To Hold Asset Name
    var tditem0 = document.createElement('td');
    tditem0.setAttribute("id", dndAsset.getUniqueTdNode())   ;
    tditem0.setAttribute("name", dndAsset.getUniqueTdNode())   ;
    tditem0.innerHTML = dndAsset.getName() ;

    // Create AssetType Image Cell To Hold Asset type image
    var tditem1= document.createElement('td');
    var clickNode = document.createElement('a');

    //var imgType =  document.createElement('img') ;
    //imgType.src = dndAsset.getSrc();
    //imgType.setAttribute("align","middle" )   ;

    //clickNode.appendChild(imgType);
    //tditem1.appendChild(clickNode);

    tritem.appendChild(tditem0);
    tritem.appendChild(tditem1);

    table.appendChild(tritem);
    div.appendChild(table);

    dojo.body().appendChild(div);

    // Set the Source with the Type
    var s = new dojo.dnd.Source(table, {accept: [dndAsset._dndType]});
    var m = dojo.dnd.manager();
    // Start Dragging Avatar in action Managed by dnd manager..
    m.startDrag(s, [tritem], false);
}



/**********************************************************************

        Add Embedded Include Asset for Advanced UI
          Type = Included
*************************************************************************/

// Called to perform Asset Include  into Advanced Select From Tree
  function addIncludedAssetTCS  ( assetname,assetId,AssetType, fieldname, fielddesc, oFCKeditor )
   {
       // Select from Asset
       var EncodedString = window.top.frames["XcelWorkFrames"].frames["XcelTree"].document.TreeApplet.exportSelections()+'';
       var allowedassettypes = oFCKeditor.config.allowedassettypes;

       console.log ( "<in> fwincludeasset.plugin::CS.JS.addIncludedAssetT  assetID:"+assetId)  ;
       console.log ( "allowedassettypes:"+allowedassettypes )  ;
       console.log ( "EncodedString :"+EncodedString )  ;

   }



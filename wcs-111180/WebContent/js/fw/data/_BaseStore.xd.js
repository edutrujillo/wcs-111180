/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data._BaseStore"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data._BaseStore"]){_4._hasResource["fw.data._BaseStore"]=true;_4.provide("fw.data._BaseStore");_4.declare("fw.data._BaseStore",null,{url:"",translator:null,transport:null,transportOptions:null,receiver:null,constructor:function(_7){_4.mixin(this,{url:_7.url,translator:_7.translator,transport:_7.transport,transportOptions:_7.transportOptions,receiver:_7.receiver});},_checkProperties:function(_8){if(!this.url){console.error("[fw.data] No URL endpoint defined for store!");return false;}else{if(!this.transport){console.error("[fw.data] Cannot fetch data without a transport!");return false;}else{if(!this.receiver){console.error("[fw.data] Cannot absorb store data without a receiver!");return false;}}}if(_8){if(!this.translator){console.error("[fw.data] Cannot properly send server requests without a translator!");return false;}}return true;},onReceiveData:function(_9){}});}}};});
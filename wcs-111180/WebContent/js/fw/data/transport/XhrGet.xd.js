/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data.transport.XhrGet"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data.transport.XhrGet"]){_4._hasResource["fw.data.transport.XhrGet"]=true;_4.provide("fw.data.transport.XhrGet");fw.data.transport.XhrGet={sendFetchRequest:function(_7,_8,_9,_a,_b){var _c={url:_7,content:_8,handleAs:"json-comment-optional"};if(_9&&_4.isObject(_9)){_4.mixin(_c,_9);}if(_a){_c.load=_a;}if(_b){_c.error=_b;}return _4.xhrGet(_c);}};}}};});
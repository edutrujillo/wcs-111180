/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data.util"]){dojo._hasResource["fw.data.util"]=true;dojo.provide("fw.data.util");dojo.require("dojo.data.util.sorter");fw.data.util={caseInsensitiveComparator:function(a,b){if(dojo.isString(a)&&dojo.isString(b)){return dojo.data.util.sorter.basicComparator(a.toLowerCase(),b.toLowerCase());}else{return dojo.data.util.sorter.basicComparator(a,b);}},serializeStoreItem:function(_1,_2,_3){var _4={},i,_5,_6;if(!_1){return _4;}if(!_3||!_3.length){_3=_1.getAttributes(_2);}_5=_3.length;for(i=_5;i--;){_6=_1.getValues(_2,_3[i]);if(_6.length==1){_4[_3[i]]=_6[0];}else{if(_6.length>1){_4[_3[i]]=_6;}}}return _4;}};}
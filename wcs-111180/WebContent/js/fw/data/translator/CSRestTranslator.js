/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data.translator.CSRestTranslator"]){dojo._hasResource["fw.data.translator.CSRestTranslator"]=true;dojo.provide("fw.data.translator.CSRestTranslator");fw.data.translator.CSRestTranslator={translate:function(_1){var _2={};if(!_1){_1={};}if(_1.start){_2.startindex=_1.start;}if(_1.count&&isFinite(_1.count)){_2.count=_1.count;}if(_1.sort){for(var i=0;i<_1.sort.length;i++){_2["sortfield:"+_1.sort[i].attribute+(_1.sort[i].descending?":des":"")]="";}}if(_1.query){for(var k in _1.query){if(!_1.query.hasOwnProperty(k)){continue;}if(k=="q"){_2.q=_1.query[k];}else{if(_1.query[k]){_2["field:"+k]=_1.query[k];}}}}return _2;}};}
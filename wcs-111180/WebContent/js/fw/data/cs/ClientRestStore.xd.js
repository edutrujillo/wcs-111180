/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data.cs.ClientRestStore"],["require","fw.data.ClientStore"],["require","fw.data.cs._Resources"],["require","fw.data.util"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data.cs.ClientRestStore"]){_4._hasResource["fw.data.cs.ClientRestStore"]=true;_4.provide("fw.data.cs.ClientRestStore");_4.require("fw.data.ClientStore");_4.require("fw.data.cs._Resources");_4.require("fw.data.util");_4.declare("fw.data.cs.ClientRestStore",fw.data.ClientStore,{resource:"",params:null,constructor:function(_7){var _8=fw.data.cs._Resources.resolveStoreArgs(_7);_4.mixin(this,_8);if(!this.transportOptions){this.transportOptions={};}this.transportOptions.sync=true;},onReceiveData:function(_9){this.inherited(arguments);this.comparatorMap={};if(_9.idAttribute){this.comparatorMap[_9.idAttribute]=fw.data.util.caseInsensitiveComparator;}if(_9.labelAttribute){this.comparatorMap[_9.labelAttribute]=fw.data.util.caseInsensitiveComparator;}}});}}};});
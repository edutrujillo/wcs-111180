/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data.cs.UsersServerRestStore"]){dojo._hasResource["fw.data.cs.UsersServerRestStore"]=true;dojo.provide("fw.data.cs.UsersServerRestStore");dojo.require("fw.data.cs.ServerRestStore");dojo.declare("fw.data.cs.UsersServerRestStore",fw.data.cs.ServerRestStore,{_renderDate:function(_1){var _2={url:dojo.config.fw_csPath+"ContentServer",content:{"date":_1,"wemUsers":true,pagename:"OpenMarket/Xcelerate/Util/ConvertDateTZByXhr"},handleAs:"json",sync:true};return dojo.xhrGet(_2).then(function(_3){return _3.formattedDates;});},_receiveData:function(_4,_5){var _6=[];dojo.forEach(_4.users,function(_7,i){if(_7.lastloggedin){_6.push(_7.lastloggedin);}});if(_6.length!=0){var _8;dojo.when(this._renderDate(_6),function(_9){_8=_9;});var _a=0;dojo.forEach(_4.users,function(_b,i){if(_b.lastloggedin){_b.lastloggedin=_8[_a];_a++;}});}this.inherited(arguments);}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.controller.MultiDevicePreviewController"]){dojo._hasResource["fw.ui.controller.MultiDevicePreviewController"]=true;dojo.provide("fw.ui.controller.MultiDevicePreviewController");dojo.require("fw.ui.controller.PreviewController");dojo.declare("fw.ui.controller.MultiDevicePreviewController",fw.ui.controller.PreviewController,{changeDeviceGroup:function(_1){var _2=this.view;_2.setDeviceGroupInfo(_1.params);_2.slideOutDeviceImages();if(_2.loadingScreen){dojo.style(_2.loadingScreen,"display","");}_2.show();},navigate:function(_3){}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.insite.SystemLocaleString"]){dojo._hasResource["fw.ui.insite.SystemLocaleString"]=true;dojo.provide("fw.ui.insite.SystemLocaleString");dojo.require("fw.util");fw.util.fetchSLS(["UI/UC1/JS/OpenInANewTab","dvin/Common/Edit","UI/UC1/JS/ChangeContentLayout","UI/UC1/JS/ClearSlot","dvin/Common/Delete","UI/UC1/JS/SlotProperties","UI/UC1/JS/Manage","UI/UC1/JS/Upload"]);}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dojox.layout.GridContainer"]){dojo._hasResource["fw.ui.dojox.layout.GridContainer"]=true;dojo.provide("fw.ui.dojox.layout.GridContainer");dojo.require("fw.ui.dojox.layout.GridContainerLite");dojo.require("dojox.layout.GridContainer");dojo.declare("fw.ui.dojox.layout.GridContainer",fw.ui.dojox.layout.GridContainerLite,{});}
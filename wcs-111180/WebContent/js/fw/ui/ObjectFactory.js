/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.ObjectFactory"]){dojo._hasResource["fw.ui.ObjectFactory"]=true;dojo.provide("fw.ui.ObjectFactory");(function(){var _1={managers:{},views:{},services:{}};function _2(_3){if(_3){var _4=dojo.getObject(_3);if(typeof _4=="function"){return new _4();}}return null;};function _5(_6,_7){var c=_1[_7][_6];if(c){if(dojo.isString(c)){c=_1[_7][_6]=_2(c);}return c;}console.error("[ObjectFactory] - no entry with key \""+_6+"\" found in section \""+_7+"\"");return null;};fw.ui.ObjectFactory={init:function(a){_1=a;},createManagerObject:function(_8){return _5(_8,"managers");},createViewObject:function(_9){return _5(_9,"views");},createServiceObject:function(_a){return _5(_a,"services");},getController:function(_b){return _5(_b,"controllers");},getManager:function(_c){return _5(_c,"managers");},getService:function(_d){return _5(_d,"services");}};})();}
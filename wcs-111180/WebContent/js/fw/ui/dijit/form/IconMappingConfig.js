/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.form.IconMappingConfig"]){dojo._hasResource["fw.ui.dijit.form.IconMappingConfig"]=true;dojo.provide("fw.ui.dijit.form.IconMappingConfig");(function(){var _1="excel",_2="pdf",_3="powerpoint",_4="word";fw.ui.dijit.form.DEFAULT_ICON="default";fw.ui.dijit.form.iconMapping={"xls":_1,"xlsx":_1,"pdf":_2,"ppt":_3,"txt":_4,"doc":_4};})();}
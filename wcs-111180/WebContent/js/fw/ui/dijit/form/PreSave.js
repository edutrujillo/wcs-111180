/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.form.PreSave"]){dojo._hasResource["fw.ui.dijit.form.PreSave"]=true;dojo.provide("fw.ui.dijit.form.PreSave");dojo.declare("fw.ui.dijit.form.PreSave",null,{execute:function(_1){var _2="extensions.sites."+_1.PubName+".assettypes."+_1.AssetType+".PreSave";try{dojo["require"](_2);}catch(e){return;}dojo.ready(function(){var _3=dojo.getObject(_2);var _4=new _3();_4.execute(_1);});}});fw.ui.dijit.form._preSave=null;fw.ui.dijit.form.preSave=function(){if(!fw.ui.dijit.form._preSave){fw.ui.dijit.form._preSave=new fw.ui.dijit.form.PreSave();}return fw.ui.dijit.form._preSave;};}
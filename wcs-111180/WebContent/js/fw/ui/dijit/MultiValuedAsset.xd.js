/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.MultiValuedAsset"],["require","fw.ui.dijit.MultiValuedAttributes"],["require","fw.ui.dnd.util"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.MultiValuedAsset"]){_4._hasResource["fw.ui.dijit.MultiValuedAsset"]=true;_4.provide("fw.ui.dijit.MultiValuedAsset");_4.require("fw.ui.dijit.MultiValuedAttributes");_4.require("fw.ui.dnd.util");_4.declare("fw.ui.dijit.MultiValuedAsset",fw.ui.dijit.MultiValuedAttributes,{title:fw.util.getString("UI/UC1/JS/ManageSlotContent"),dndItems:[],postCreate:function(){this.inherited(arguments);this.dndItems=[];},startup:function(){this.inherited(arguments);var _7=this;_4.safeMixin(this._dndContainer,{onDndStart:function(_8,_9,_a){this.inherited(arguments);if(this.accept&&this.checkAcceptance(_8,_9)){this._changeState("Target","Enabled");}},onDropExternal:function(_b,_c,_d){var _e={},_f=_7._getPosition(this.current,this.parent,this.before);if(fw.ui.dnd.util.isTreeSource(_b)){_e.id=_b.tree.selectedNode.item.i.id;_e.name=_b.tree.selectedNode.item.i.name;_e.type=_b.tree.selectedNode.item.i.type;if(_7.checkDuplicate(_e.id,_e.type)){_7._addField(_e,_f);}}else{if(fw.ui.dnd.util.isGridSource(_b)){var _10=_4.map(_c,function(_11){return _b.getItem(_11.id).data;});var _12=_b.getItem(_c[0].id);var _13=_10[0];for(var a=1;a<_10.length;++a){_13=_13.concat(_10[a]);}var _14=_12.dndPlugin.grid.store;var _15=_14._items[_13];_e=fw.data.util.serializeStoreItem(_14,_15,["id","name","asset"]);_e.id=_e.asset.id;_e.type=_e.asset.type;if(_7.checkDuplicate(_e.id,_e.type)){_7._addField(_e,_f);}}}},checkAcceptance:function(_16,_17){if(this.accept&&this.accept["*:*"]){return true;}var _18=_4.map(_17,function(_19){return _16.getItem(_19.id).data;});if(fw.ui.dnd.util.isTreeSource(_16)){_1a=fw.ui.dnd.util.getNormalizedData(_16,_17[0]);}else{if(fw.ui.dnd.util.isGridSource(_16)){var _1b=_16.getItem(_17[0].id),_1c=_1b.dndPlugin.grid,_1d=_7._joinToArray(_18),_1e=fw.ui.dnd.util.gridSourceCreator(_1c,_1d),_1a=_1e.data;}else{return true;}}if(_1a){var _1f=_1a.type;var _20=_1a.subtype;var _21=false;var _22=_1f+":"+_20;var _23=_1f+":*";if(_22 in this.accept||_23 in this.accept){_21=true;}if(!_21){return false;}}return true;}});},_joinToArray:function(_24){var a=_24[0];for(var i=1;i<_24.length;++i){a=a.concat(_24[i]);}return a;},buildItem:function(_25,_26){_4.create("div",{"class":"listItemField",innerHTML:_26.name},_25);_4.attr(_25,"dnddata",_4.toJson(_26));this.dndItems.push(_26);},displayAddButton:function(){},_onSave:function(){var _27=[];_4.query(".listItem",this.domNode).forEach(function(_28){_27.push(_4.fromJson(_4.attr(_28,"dnddata")));});this.onSave(_27);_5.getEnclosingWidget(this.domNode.parentNode).hide();},_getPosition:function(_29,_2a,_2b){var _2c=_2a.childNodes.length;for(var i=0;i<_2c;i++){if(_2a.childNodes[i]===_29){return _2b?i:i+1;}}return _2c;},checkDuplicate:function(id,_2d){var _2e=this.dndItems.length;for(var i=0;i<_2e;i++){if(Number(this.dndItems[i].id)===Number(id)&&this.dndItems[i].type===_2d){return false;}}return true;},_addNewField:function(){},_removeField:function(id,_2f){var _30=_4.fromJson(_4.attr(_2f,"dnddata")),_31=this;_4.forEach(_31.dndItems,function(_32,i){if(_32&&Number(_32.id)===Number(_30.id)&&_32.type===_30.type){_31.dndItems.splice(i,1);}});}});}}};});
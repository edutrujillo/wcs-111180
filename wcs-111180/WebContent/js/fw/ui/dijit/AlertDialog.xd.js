/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.AlertDialog"],["require","fw.ui.dijit.FlexibleDialog"],["require","fw.ui.dijit.Button"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.AlertDialog"]){_4._hasResource["fw.ui.dijit.AlertDialog"]=true;_4.provide("fw.ui.dijit.AlertDialog");_4.require("fw.ui.dijit.FlexibleDialog");_4.require("fw.ui.dijit.Button");_4.declare("fw.ui.dijit.AlertDialog",fw.ui.dijit.FlexibleDialog,{templateString:_4.cache("fw.ui.dijit","templates/AlertDialog.html","<div class=\"alertDialog\">\r\n\t<span dojoAttachPoint=\"closeButtonNode\"></span>\r\n\t<div class=\"iconNode\"></div>\r\n\t<div class=\"titleNode\" dojoAttachPoint=\"titleNode\"></div>\r\n\t<div class=\"messageNode\" dojoAttachPoint=\"messageNode\"></div>\r\n\t<div class=\"buttonNode\">\r\n\t\t<div dojoType=\"fw.ui.dijit.Button\" buttonStyle=\"greySmall\" dojoAttachEvent=\"onClick:closeDialog\">OK</div>\r\n\t</div>\r\n</div>\r\n"),widgetsInTemplate:true,showCloseButton:false,message:"",title:"",_setMessageAttr:function(_7){this.messageNode.innerHTML=_7;this.message=_7;},_setTitleAttr:function(_8){this.titleNode.innerHTML=_8;this.title=_8;},closeDialog:function(){this.hide();this.destroyRecursive();}});}}};});
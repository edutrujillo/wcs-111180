/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.insite.InsiteMultiWidgetMixin"],["require","fw.ui.ObjectFactory"],["require","fw.ui.dijit.insite.InsiteWidgetMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.insite.InsiteMultiWidgetMixin"]){_4._hasResource["fw.ui.dijit.insite.InsiteMultiWidgetMixin"]=true;_4.provide("fw.ui.dijit.insite.InsiteMultiWidgetMixin");_4.require("fw.ui.ObjectFactory");_4.require("fw.ui.dijit.insite.InsiteWidgetMixin");_4.declare("fw.ui.dijit.insite.InsiteMultiWidgetMixin",fw.ui.dijit.insite.InsiteWidgetMixin,{init:function(_7){var _8=this.widgetArgs,_9=this.get("value");this._asset=_7.asset;this.assetManager=fw.ui.ObjectFactory.createManagerObject("asset");this._asset.set(_8.fieldName,_9||[]);this.set("disabled",true);}});}}};});
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.CommonButtonMixin"]){dojo._hasResource["fw.ui.dijit.CommonButtonMixin"]=true;dojo.provide("fw.ui.dijit.CommonButtonMixin");dojo.declare("fw.ui.dijit.CommonButtonMixin",[],{createButton:function(_1){var _2=dojo.create("div",{"class":"inline-left"});var _3=dojo.create("div",{"class":"button-left"},_2,"last");var _4=dojo.create("div",{"class":"button-middle"},_2,"last");var _5=dojo.create("div",{"class":"button-text",innerHTML:_1},_4,"last");var _6=dojo.create("div",{"class":"button-right"},_2,"last");return _2;}});}
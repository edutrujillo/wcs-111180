/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.view.BaseView"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.view.BaseView"]){_4._hasResource["fw.ui.view.BaseView"]=true;_4.provide("fw.ui.view.BaseView");(function(){var _7=0,_8=function(_9){_7+=1;return _9+"-"+_7;};_4.declare("fw.ui.view.BaseView",_4.Stateful,{dirty:false,initialized:false,editable:false,constructor:function(){this.id=_8("view");},init:function(){this.inherited(arguments);},show:function(){},destroy:function(){},info:function(_a){this.message(_a,"info");},error:function(_b){this.message(_b,"error");},warn:function(_c){this.message(_c,"warn");},message:function(_d,_e){if(this.appContext){this.appContext.message(_d,_e);}else{}},feedback:function(_f,_10){var app=this.appContext;if(app){app.feedback(_f,_10);}},clearMessage:function(){var app=this.appContext;if(app){app.clearMessage();}},prompt:function(_11){return this.appContext.prompt(_11);},prompt:function(_12,_13){return this.appContext.prompt(_12,_13);},feedback:function(_14,_15){},clearFeedback:function(){},isEditable:function(){return this.editable;},isEditMode:function(){var _16=this.get("mode");return (_16&&_16==="edit");}});}());}}};});
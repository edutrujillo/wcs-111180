/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.view.advanced.FinishAssignmentView"],["require","fw.ui.view.AdvancedView"],["require","fw.ui.view.TabbedViewMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.view.advanced.FinishAssignmentView"]){_4._hasResource["fw.ui.view.advanced.FinishAssignmentView"]=true;_4.provide("fw.ui.view.advanced.FinishAssignmentView");_4.require("fw.ui.view.AdvancedView");_4.require("fw.ui.view.TabbedViewMixin");_4.declare("fw.ui.view.advanced.FinishAssignmentView",[fw.ui.controller.BaseDocController,fw.ui.view.AdvancedView,fw.ui.view.TabbedViewMixin],{getAdvancedURLParams:function(){var _7=this.model.get("asset");if(_7){return {ThisPage:"SetStatusFront",AssetType:_7.type,id:_7.id,PostPage:"SetStatusPost",contentfunctions:"true"};}else{throw new Error(fw.util.getString("UI/UC1/JS/CannotRenderView"));}},show:function(){this.set("title",this.model.get("name"));return this.inherited(arguments);}});}}};});
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.document.DocumentId"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.document.DocumentId"]){_4._hasResource["fw.ui.document.DocumentId"]=true;_4.provide("fw.ui.document.DocumentId");_4.declare("fw.ui.document.DocumentId",null,{constructor:function(_7,id,_8,_9,_a){this.getId=function(){return id;};this.getType=function(){return _7;};this.getAssetType=function(){return _8;};this.isFlex=function(){return _9;};this.isComplex=function(){return _a;};},equals:function(_b){if(_b instanceof fw.ui.document.DocumentId){var _c=this.getId()==_b.getId()&&this.getType()==_b.getType();var _d=(typeof (this.getAssetType())!=="undefined"&&typeof (_b.getAssetType())!=="undefined");var _e=(typeof (this.isFlex())!=="undefined"&&typeof (_b.isFlex())!=="undefined");var _f=(typeof (this.isComplex())!=="undefined"&&typeof (_b.isComplex())!=="undefined");if(_d===true){_c=_c&&this.getAssetType()==_b.getAssetType();}if(_e===true){_c=_c&&this.isFlex()==_b.isFlex();}if(_f===true){_c=_c&&this.isComplex()==_b.isComplex();}return _c;}}});}}};});
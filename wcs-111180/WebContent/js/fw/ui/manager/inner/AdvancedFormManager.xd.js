/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.manager.inner.AdvancedFormManager"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.manager.inner.AdvancedFormManager"]){_4._hasResource["fw.ui.manager.inner.AdvancedFormManager"]=true;_4.provide("fw.ui.manager.inner.AdvancedFormManager");_4.declare("fw.ui.manager.inner.AdvancedFormManager",null,{});}}};});
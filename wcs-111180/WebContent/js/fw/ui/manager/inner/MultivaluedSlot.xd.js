/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.manager.inner.MultivaluedSlot"],["require","fw.ui.manager.inner.Slot"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.manager.inner.MultivaluedSlot"]){_4._hasResource["fw.ui.manager.inner.MultivaluedSlot"]=true;_4.provide("fw.ui.manager.inner.MultivaluedSlot");_4.require("fw.ui.manager.inner.Slot");_4.declare("fw.ui.manager.inner.MultivaluedSlot",fw.ui.manager.inner.Slot,{set:function(_7){},isEditable:function(){return false;},isContentEditable:function(){var _8=this.getParentAsset();if(_8){if(!_8.isEditable()){this.reason="You do not have the required asset permissions";return false;}return true;}return true;},clear:function(){},getButtonList:function(){if(this.getParentAsset()){return ["edit","reorderSlot"];}else{return ["edit"];}}});}}};});
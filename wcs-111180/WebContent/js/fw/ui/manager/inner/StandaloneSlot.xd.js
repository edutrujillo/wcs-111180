/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.manager.inner.StandaloneSlot"],["require","fw.ui.manager.inner.Slot"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.manager.inner.StandaloneSlot"]){_4._hasResource["fw.ui.manager.inner.StandaloneSlot"]=true;_4.provide("fw.ui.manager.inner.StandaloneSlot");_4.require("fw.ui.manager.inner.Slot");_4.declare("fw.ui.manager.inner.StandaloneSlot",fw.ui.manager.inner.Slot,{set:function(_7){this.inherited(arguments);this.setStatus("ED");},isContentEditable:function(){var _8=this.isEditable();if(!_8){return false;}if(!this.isEmpty()&&this.isPreset("c")&&this.isPreset("cid")){this.reason=fw.util.getString("UI/UC1/JS/SlotContentNotEditable");return false;}return true;},clear:function(){if(this.assetType==="CSElement"||this.assetType==="SiteEntry"){this.voidSlotAsset();}else{if(!this.isPreset("c")){delete this.assetType;}if(!this.isPreset("cid")){delete this.assetId;delete this.assetName;}if(!this.tname||!this.isLayoutEditable()||this.tname===this.defaultTName){this.voidSlotAsset();}else{this.setStatus("ED");}}}});}}};});
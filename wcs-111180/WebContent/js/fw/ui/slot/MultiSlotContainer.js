/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.slot.MultiSlotContainer"]){dojo._hasResource["fw.ui.slot.MultiSlotContainer"]=true;dojo.provide("fw.ui.slot.MultiSlotContainer");dojo.require("dijit._Widget");dojo.require("dijit._CssStateMixin");dojo.require("fw.ui.slot.ActivatorMixin");dojo.declare("fw.ui.slot.MultiSlotContainer",[dijit._Widget,dijit._CssStateMixin,fw.ui.slot.ActivatorMixin],{slotArgs:null,overlayTitle:"",baseClass:"fwMultiSlot",buttons:[],constructor:function(_1){this.slotArgs={};},postCreate:function(){this.inherited(arguments);this._addActivator();}});}
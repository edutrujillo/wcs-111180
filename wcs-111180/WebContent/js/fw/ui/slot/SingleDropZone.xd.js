/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.slot.SingleDropZone"],["require","fw.ui.slot.BaseSlotSource"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.slot.SingleDropZone"]){_4._hasResource["fw.ui.slot.SingleDropZone"]=true;_4.provide("fw.ui.slot.SingleDropZone");_4.require("fw.ui.slot.BaseSlotSource");_4.declare("fw.ui.slot.SingleDropZone",fw.ui.slot.BaseSlotSource,{_pubEvent:"/fw/dnd/newSlotItemDrop",_noswapPrompt:"The current contents of the target slot cannot be swapped "+"to the originating slot.  Proceed anyway?",_yesClickHandler:function(_7,_8,_9){this.onDropExternal(_7,_8,_9,true);_7._publishEvent();},markupFactory:function(_a,_b){_a._skipStartup=true;return new fw.ui.slot.SingleDropZone(_b,_a);},onDropExternal:function(_c,_d,_e,_f){if(!_f&&(_c instanceof fw.ui.slot.SingleSlotSource||_c instanceof fw.ui.slot.SingleDropZone)){if(_c.checkAcceptance(this,this.getAllNodes())){var _10=this.getAllNodes();this.inherited(arguments);_c.onDropExternal(this,_10,false,true);this._publishEvent();}else{this._askConfirmation(this._noswapPrompt,_4.hitch(this,"_yesClickHandler",_c,_d,_e));}}else{var arr=this.getAllNodes(),i;this.inherited(arguments);this._publishEvent();}}});}}};});
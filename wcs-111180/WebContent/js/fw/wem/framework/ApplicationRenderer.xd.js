/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.wem.framework.ApplicationRenderer"],["require","fw.wem.framework.LayoutRenderer"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.wem.framework.ApplicationRenderer"]){_4._hasResource["fw.wem.framework.ApplicationRenderer"]=true;_4.provide("fw.wem.framework.ApplicationRenderer");_4.require("fw.wem.framework.LayoutRenderer");_4.declare("fw.wem.framework.ApplicationRenderer",null,{constructor:function(_7,_8){this.application=_7;this.applicationbody=_8;},render:function(){var _9=this.application.layouttype;var _a=eval(_9);if(_a){var _b=new _a(this.application.id);_b.render(this.applicationbody);}}});}}};});
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UIDropDownImage"]){dojo._hasResource["fw.dijit.UIDropDownImage"]=true;dojo.provide("fw.dijit.UIDropDownImage");dojo.require("fw.dijit.UIActionImage");dojo.require("dijit._HasDropDown");dojo.declare("fw.dijit.UIDropDownImage",[fw.dijit.UIActionImage,dijit._HasDropDown],{focus:function(){}});}
/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UILoadingIndicator"]){dojo._hasResource["fw.dijit.UILoadingIndicator"]=true;dojo.provide("fw.dijit.UILoadingIndicator");dojo.require("dijit._Widget");dojo.declare("fw.dijit.UILoadingIndicator",dijit._Widget,{loadingMessage:"Loading...",attributeMap:dojo.delegate(dijit._Widget.prototype.attributeMap,{loadingMessage:{node:"domNode",type:"innerHTML"}}),_subscriptions:null,_overlayElement:null,constructor:function(_1){this._subscriptions=[dojo.subscribe(fw.dijit.UILoadingIndicator.startLoadEvent,this,"show"),dojo.subscribe(fw.dijit.UILoadingIndicator.stopLoadEvent,this,"hide")];},postCreate:function(){this.inherited(arguments);dojo.addClass(this.domNode,"UILoadingIndicator");this.hide();},uninitialize:function(){for(var i=0;i<this._subscriptions.length;i++){dojo.unsubscribe(this._subscriptions[i]);}this._subscriptions=null;this.inherited(arguments);},show:function(){dojo.style(this.domNode,"display","block");},hide:function(){dojo.style(this.domNode,"display","none");}});fw.dijit.UILoadingIndicator.startLoadEvent="fw/wem/StartLoad";fw.dijit.UILoadingIndicator.stopLoadEvent="fw/wem/StopLoad";}
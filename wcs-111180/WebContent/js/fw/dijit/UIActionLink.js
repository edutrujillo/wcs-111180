/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UIActionLink"]){dojo._hasResource["fw.dijit.UIActionLink"]=true;dojo.provide("fw.dijit.UIActionLink");dojo.require("dijit._Widget");dojo.require("dijit._Templated");dojo.declare("fw.dijit.UIActionLink",[dijit._Widget,dijit._Templated],{disabled:false,templateString:"<a href=\"javascript:void(0)\" class=\"UIActionLink\" dojoAttachPoint=\"containerNode\"></a>",attributeMap:dojo.delegate(dijit._Widget.prototype.attributeMap,{label:{node:"containerNode",type:"innerHTML"}}),postCreate:function(){this.inherited(arguments);this.connect(this.domNode,"onclick","_onClick");},_setDisabledAttr:function(_1){if(_1){dojo.addClass(this.domNode,"UIActionDisabled");}else{dojo.removeClass(this.domNode,"UIActionDisabled");}this.disabled=_1;},_onClick:function(_2){if(!this.get("disabled")){this.onClick();}},onClick:function(_3){}});}
dojo._xdResourceLoaded(function(dojo, dijit, dojox){
return {depends: [["provide", "fw.nls.fw_wem_framework_ROOT"],
["provide", "dijit.nls.loading"],
["provide", "dijit.nls.loading.ROOT"],
["provide", "dijit.nls.common"],
["provide", "dijit.nls.common.ROOT"],
["provide", "dijit.form.nls.validate"],
["provide", "dijit.form.nls.validate.ROOT"],
["provide", "dijit.form.nls.ComboBox"],
["provide", "dijit.form.nls.ComboBox.ROOT"],
["provide", "dojo.cldr.nls.number"],
["provide", "dojo.cldr.nls.number.ROOT"]],
defineResource: function(dojo, dijit, dojox){dojo.provide("fw.nls.fw_wem_framework_ROOT");dojo.provide("dijit.nls.loading");dijit.nls.loading._built=true;dojo.provide("dijit.nls.loading.ROOT");dijit.nls.loading.ROOT={"loadingState":"Loading...","errorState":"Sorry, an error occurred"};dojo.provide("dijit.nls.common");dijit.nls.common._built=true;dojo.provide("dijit.nls.common.ROOT");dijit.nls.common.ROOT={"buttonOk":"OK","buttonCancel":"Cancel","buttonSave":"Save","itemClose":"Close"};dojo.provide("dijit.form.nls.validate");dijit.form.nls.validate._built=true;dojo.provide("dijit.form.nls.validate.ROOT");dijit.form.nls.validate.ROOT={"rangeMessage":"This value is out of range.","invalidMessage":"The value entered is not valid.","missingMessage":"This value is required."};dojo.provide("dijit.form.nls.ComboBox");dijit.form.nls.ComboBox._built=true;dojo.provide("dijit.form.nls.ComboBox.ROOT");dijit.form.nls.ComboBox.ROOT={"previousMessage":"Previous choices","nextMessage":"More choices"};dojo.provide("dojo.cldr.nls.number");dojo.cldr.nls.number._built=true;dojo.provide("dojo.cldr.nls.number.ROOT");dojo.cldr.nls.number.ROOT={"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencyFormat":"¤ #,##0.00","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","perMille":"‰","group":",","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":".","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"};

}};});
dojo._xdResourceLoaded(function(dojo, dijit, dojox){
return {depends: [["provide", "dojo.nls.fw_wem_login_zh"],
["provide", "dijit.form.nls.validate"],
["provide", "dijit.form.nls.validate.zh"]],
defineResource: function(dojo, dijit, dojox){dojo.provide("dojo.nls.fw_wem_login_zh");dojo.provide("dijit.form.nls.validate");dijit.form.nls.validate._built=true;dojo.provide("dijit.form.nls.validate.zh");dijit.form.nls.validate.zh={"rangeMessage":"此值超出范围。","invalidMessage":"输入的值无效。","missingMessage":"此值是必需值。"};

}};});
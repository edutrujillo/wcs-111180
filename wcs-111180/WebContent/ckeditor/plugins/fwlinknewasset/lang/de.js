/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwlinknewasset', 'de',
{
	fwlinknewasset :
	{
	  assetCreatorIncludeDlgTitle : 'Asset erstellt von',
	  assetCreatorIncludeTitleDesc : 'Neues Asset erstellen und einbeziehen',
	  assetCreatorLinkTitleDesc : 'Neues Asset erstellen und verknüpfen',
	  PleaseSelectLinkTextFrom	: 'Der Text zur Verknüpfung mit dem Asset muss zuerst gewählt werden' 
	
	
	  	    
	}
});


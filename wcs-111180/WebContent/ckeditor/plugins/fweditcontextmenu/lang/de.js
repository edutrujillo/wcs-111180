/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'de',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  'Einbezogenes Asset ändern',
         EditProperties :       'Eigenschaften bearbeiten',
         RemoveIncludedAsset :  'Einbezogenes Asset entfernen',
         OpenLink :             'Link öffnen',
         ChangeLinkedAsset :    'Verknüpftes Asset ändern',
         RemoveLinkedAsset :    'Verknüpftes Asset entfernen',
         PleaseSelectAssetFromSearchResults : 'Wählen Sie ein Asset aus den Suchergebnissen',
         CannotLinkOrIncludeThisAsset :       'Dieses Asset kann nicht verknüpft oder einbezogen werden.\nWählen Sie Assets des folgenden Typs:\n',

         CutIncludedAsset :            'Einbezogenes Asset ausschneiden',
         CopyIncludedAsset :           'Einbezogenes Asset kopieren',
         CutLinkedAsset :              'Verknüpftes Asset ausschneiden',
         CopyLinkedAsset :             'Verknüpftes Asset kopieren',
         PasteIncludedAsset :          'Einbezogenes Asset einfügen',
         PasteLinkedAsset :            'Verknüpftes Asset einfügen' 		

	}
});





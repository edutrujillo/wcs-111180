/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'ja',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  '含まれるアセットの変更',
         EditProperties :       'プロパティの編集',
         RemoveIncludedAsset :  '含まれるアセットの除去',
         OpenLink :             'リンクを開く',
         ChangeLinkedAsset :    'リンク・アセットの変更',
         RemoveLinkedAsset :    'リンク・アセットの除去',
         PleaseSelectAssetFromSearchResults : '検索結果からアセットの選択',
         CannotLinkOrIncludeThisAsset :       'このアセットをリンクまたは含めることはできません。\n次のタイプのアセットを選択してください: \n',

         CutIncludedAsset :            '含まれるアセットの切取り',
         CopyIncludedAsset :           '含まれるアセットのコピー',
         CutLinkedAsset :              'リンク・アセットの切取り',
         CopyLinkedAsset :             'リンク・アセットのコピー',
         PasteIncludedAsset :          '含まれるアセットの貼付け',
         PasteLinkedAsset :            'リンク・アセットの貼付け' 		

	}
});





/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'it',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  'Modifica asset incluso',
         EditProperties :       'Modifica proprietà',
         RemoveIncludedAsset :  'Rimuovi asset incluso',
         OpenLink :             'Apri collegamento',
         ChangeLinkedAsset :    'Modifica asset collegato',
         RemoveLinkedAsset :    'Rimuovi asset collegato',
         PleaseSelectAssetFromSearchResults : 'Selezionare un asset dai risultati della ricerca',
         CannotLinkOrIncludeThisAsset :       'Non è consentito effettuare il collegamento a questo asset o includerlo.\nSelezionare un asset dei tipi seguenti:\n',

         CutIncludedAsset :            'Taglia asset incluso',
         CopyIncludedAsset :           'Copia asset incluso',
         CutLinkedAsset :              'Taglia asset collegato',
         CopyLinkedAsset :             'Copia asset collegato',
         PasteIncludedAsset :          'Incolla asset incluso',
         PasteLinkedAsset :            'Incolla asset collegato' 		

	}
});





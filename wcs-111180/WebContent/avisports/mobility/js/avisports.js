function toggleNavigationBar () {
	var navBar = document.getElementById("nav-container");
	var navButton = document.getElementById("nav-icon");
	if (navBar.style.display == "block")
	{
		navBar.style.display = "none";
		navButton.className = "close";
	}
	else
	{
		navBar.style.display = "block";
		navButton.className = "open";
	}
}
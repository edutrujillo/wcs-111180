CKEDITOR.editorConfig = function( config )
{
    config.enterMode=CKEDITOR.ENTER_DIV;
    config.fullPage = false;
	config.contentsCss = 'avisports/ckeditor/ckeditor.css';
	config.extraPlugins = 'fwlinknewasset,fwlinkasset,fwincludenewasset,fwincludeasset,fwimagepicker,fwnoneditable,fweditcontextmenu';
	config.stylesSet = [
	 { name : 'Body', element : 'p', attributes: {'class': 'ckbody'} },
	 { name : 'Teaser Title', element : 'h2', attributes: {'class': 'cktitle'} },
	 { name : 'Teaser Text', element : 'div', attributes: {'class': 'cktext'} },
	 { name : 'Home Title', element : 'h2', attributes: {'class': 'ckheadline'} },
	 { name : 'Home Text', element : 'p', attributes: {'class': 'ckhometeaser'} }
	];
	
	config.toolbar_Home =
	[
		['Styles', 'fwlinkasset','fwincludeasset','fweditcontextmenu']
	];

	config.toolbar_Article =
	[
		['Styles', 'fwlinkasset','fwincludeasset','fweditcontextmenu', 'Source']
	];
	
	config.urlFCKEditorRenderer= CKEDITOR.basePath + "../ContentServer?pagename=OpenMarket%2FXcelerate%2FActions%2FFCKEditorRenderer";
};
